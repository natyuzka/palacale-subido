var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HTTP } from '@ionic-native/http';
import { MenuEvaluarPunto } from '../menu-evaluar-punto/menu-evaluar-punto';
import { Storage } from '@ionic/storage';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var MapaPage = (function () {
    function MapaPage(navCtrl, navParams, geolocation, http, storage, loadingCtrl, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.http = http;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.database = database;
        this.punto_offline = "";
    }
    MapaPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('usuario')
            .then(function (val) {
            _this.usuario = val;
            _this.loadMap();
        });
    };
    MapaPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.database.probarConexion()
            .then(function () {
            _this.internet = true;
        })
            .catch(function () {
            _this.internet = false;
        });
    };
    MapaPage.prototype.internet_loadMap = function (position) {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/', { puntos_all: 1, usuario: this.usuario.id_u }, {})
            .then(function (data) {
            _this.colocarPuntos(JSON.parse(data.data), "http://palacalle.abbottdata.com/imagenes/", position);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
            console.log(error.headers);
        });
    };
    MapaPage.prototype.no_internet_loadMap = function () {
        var _this = this;
        this.database.loadMap(this.usuario.id_u)
            .then(function (data) {
            _this.colocarListas(data);
        })
            .catch(function (error) { return console.log("error no_internet_loadMap: " + error); });
    };
    MapaPage.prototype.colocarListas = function (data) {
        this.puntos_offline = data;
    };
    MapaPage.prototype.colocarPuntos = function (data, url, position) {
        var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var mapOptions = {
            center: latLng,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        var loading = this.loadingCtrl.create();
        loading.present();
        var content_sin;
        var puntos = data;
        var icono = "";
        var estado = "";
        var marcadores = [];
        var i = 0;
        for (var punto in puntos) {
            // console.log("id_p: " + puntos[punto].id_p);
            if (puntos[punto].estado == 0) {
                icono = url + "marcador-blue.png";
                estado = "<p class='color-rojo-2'>No esta habilitado para visita</p>";
            }
            else if (puntos[punto].estado == 1) {
                icono = url + "marcador-green.png";
                estado = "<p class='color-verde'>Habilitado para visita</p>";
            }
            else {
                icono = url + "marcador-red.png";
                estado = "<p class='color-rojo'>Ya ha sido visitado</p>";
            }
            latLng = new google.maps.LatLng(puntos[punto].latitud, puntos[punto].longitud);
            content_sin = "<table class=\"mapa-punto\">\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t<img src=\"" + url + "tipospunto/" + puntos[punto].foto + "\" alt=\"Im\u00E1gen no disponible\" onerror=\"this.src='assets/sin-foto.jpg'\">\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t<h6>" + puntos[punto].nombre + "</h6>\n\t\t\t\t\t\t\t\t\t\t" + estado + "\n\t\t\t\t\t\t\t\t\t\t<p class=\"azul\">" + puntos[punto].tipo + "</p>\n\t\t\t\t\t\t\t\t\t\t<button class=\"btn-seleccionar btn-azul btn-rounded\" id=\"punto-" + puntos[punto].id_p + "\" data-punto=\"" + puntos[punto].id_p + "\" data-estado=\"" + puntos[punto].estado + "\">Seleccionar</button>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</table>";
            marcadores[i] = new google.maps.Marker({
                id: puntos[punto].id_p,
                position: latLng,
                map: this.map,
                icon: icono,
                estado: puntos[punto].estado
            });
            this.addInfoWindow(marcadores[i], content_sin);
            i++;
        }
        // Seguir mi posicion
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            latLng = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
            marker_yo.setPosition(latLng);
        });
        var marker_yo = new google.maps.Marker({
            id: "yo",
            position: latLng,
            map: this.map,
            icon: url + "marcador-yo.png"
        });
        loading.dismiss();
    };
    MapaPage.prototype.loadMap = function () {
        var _this = this;
        if (this.internet === true) {
            this.geolocation.getCurrentPosition().then(function (position) {
                _this.internet_loadMap(position);
            }).catch(function (error) {
                console.log("error mapa: " + JSON.stringify(error));
                alert('¡Activa el GPS para poder ver el mapa!');
            });
        }
        else {
            this.no_internet_loadMap();
        }
    };
    MapaPage.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content,
            maxWidth: 225
        });
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(_this.map, marker);
            google.maps.event.addDomListener(document.getElementById('punto-' + marker.id), 'click', function () {
                var punto = document.getElementById('punto');
                punto.dataset.punto = this.dataset.punto;
                punto.dataset.estado = this.dataset.estado;
                google.maps.event.trigger(marker, 'dblclick');
            });
        });
        google.maps.event.addListener(marker, 'dblclick', function () {
            var punto = document.getElementById('punto');
            punto.dataset.punto = marker.id;
            punto.dataset.estado = marker.estado;
            _this.irMenu("");
        });
    };
    MapaPage.prototype.irMenu = function (datos_punto) {
        var id_punto = "";
        var estado;
        var datos;
        if (datos_punto != "") {
            id_punto = datos_punto.id_p;
            estado = datos_punto.estado;
            datos = {
                id_punto: id_punto,
                estado: estado,
                id_u: this.usuario.id_u,
                nombre: datos_punto.nombre,
                foto: datos_punto.foto,
                canal: datos_punto.tipo_nombre,
                tipo: datos_punto.tipo_nombre,
                tipo_id: datos_punto.tipo_id,
                internet: false
            };
        }
        else {
            var punto = document.getElementById('punto');
            id_punto = punto.dataset.punto;
            estado = punto.dataset.estado;
            datos = {
                id_punto: id_punto,
                estado: estado,
                id_u: this.usuario.id_u,
                internet: true
            };
        }
        if (id_punto != "") {
            this.navCtrl.push(MenuEvaluarPunto, datos);
        }
        else
            alert("¡Seleccione un punto!");
    };
    return MapaPage;
}());
__decorate([
    ViewChild('map'),
    __metadata("design:type", ElementRef)
], MapaPage.prototype, "mapElement", void 0);
MapaPage = __decorate([
    Component({
        selector: 'page-mapa',
        templateUrl: 'mapa.html',
        providers: [HTTP]
    }),
    __metadata("design:paramtypes", [NavController, NavParams, Geolocation, HTTP, Storage, LoadingController, DatabaseServiceProvider])
], MapaPage);
export { MapaPage };
//# sourceMappingURL=mapa.js.map