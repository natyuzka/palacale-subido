import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { MenuEvaluarPunto } from '../menu-evaluar-punto/menu-evaluar-punto';
import { Storage } from '@ionic/storage';

import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

@Component({
	selector: 'page-mapa',
	templateUrl: 'mapa.html',
	providers: [HTTP]
})

export class MapaPage {

	usuario;
	internet;
	puntos = [{
		id_p: "",
		nombre: "SIN INFORMACIÓN",
		foto: "",
		tipo_nombre: "",
		tipo_id: "",
		usuario: "",
		direccion: "",
		estado: "0"
	}];
	punto = {
		id_p: "",
		nombre: "SIN INFORMACIÓN",
		foto: "",
		tipo_nombre: "",
		tipo_id: "",
		usuario: "",
		direccion: "",
		estado: "0"
	};
	puntos_no_visitados = [{
		id_p: "",
		nombre: "SIN INFORMACIÓN",
		foto: "",
		tipo_nombre: "",
		tipo_id: "",
		usuario: "",
		direccion: "",
		estado: "0"
	}];
	punto2 = {
		id_p: "",
		nombre: "SIN INFORMACIÓN",
		foto: "",
		tipo_nombre: "",
		tipo_id: "",
		usuario: "",
		direccion: "",
		estado: "0"
	};
	puntos_buscar_ver = [{
		id_p: "",
		nombre: "SIN INFORMACIÓN",
		foto: "",
		tipo_nombre: "",
		tipo_id: "",
		usuario: "",
		direccion: "",
		estado: "0"
	}];
	punto_buscar_ver = {
		id_p : "",
	    nombre :  "SIN INFORMACIÓN",
	    foto :  "",
	    tipo_nombre :  "",
	    tipo_id :  "",
	    usuario :  "",
	    direccion :  "",
	    estado :  "0"
	};
	ciudades = [{
		id_c: "",
		nombre: "SIN INFORMACIÓN"
	}];
	ciudad = {
		id_c: "",
		nombre: "SIN INFORMACIÓN"
	};
	cadenas = [{
		id_c: "",
		nombre: "SIN INFORMACIÓN"
	}];
	cadena = {
		id_c: "",
		nombre: "SIN INFORMACIÓN"
	};

	puntos_traer = "1";
	mispuntos = true;
	punto_buscar_select = 0;
	punto_buscar = "";
	mispuntosnovisitadosver = false;

	constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, private storage: Storage, public loadingCtrl: LoadingController, private database: DatabaseServiceProvider){}

	ionViewDidEnter(){

		this.database.probarConexion()
	    .then(() => {
	    	this.internet = true;
	    	this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				this.mostrarPuntos(this.ciudad, this.cadena);
				
			});
	    })
	    .catch(() => {
	    	this.internet = false;
	    	this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				this.mostrarPuntos(this.ciudad, this.cadena);
			});
	    });
		
		this.puntos_no_visitados = [{
				id_p: "",
				nombre: "SIN INFORMACIÓN",
				foto: "",
				tipo_nombre: "",
				tipo_id: "",
				usuario: "",
				direccion: "",
				estado: "0"
			}];
			this.puntos_buscar_ver = [{
					id_p: "",
					nombre: "SIN INFORMACIÓN",
					foto: "",
					tipo_nombre: "",
					tipo_id: "",
					usuario: "",
					direccion: "",
					estado: "0"
				}];
			this.puntos = [{
					id_p: "",
					nombre: "SIN INFORMACIÓN",
					foto: "",
					tipo_nombre: "",
					tipo_id: "",
					usuario: "",
					direccion: "",
					estado: "0"
				}];

			this.punto_buscar_ver = this.puntos_buscar_ver[0];
			this.punto = this.puntos[0];
			this.punto2 = this.puntos_no_visitados[0];
	}

	ionViewDidLoad() {
		this.puntos_traer = "1";
		this.mispuntos = true;
		this.punto_buscar_select = 0;
		this.punto_buscar = "";
		this.mispuntosnovisitadosver = false;

		this.database.probarConexion()
	    .then(() => {
	    	this.internet = true;
	    	this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				this.limpiarListas()
				.then(()=>{
					this.internet_loadCiudades()
					.catch((error)=>{
						alert("¡Error al cargar las ciudades, ingrese a Trade de nuevo!");
						console.log("error internet_loadCiudades 2: " + JSON.stringify(error));
					});
				});
				
			});
	    })
	    .catch(() => {
	    	this.internet = false;
	    	this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				this.limpiarListas()
				.then(()=>{
					this.no_internet_loadCiudades()
					.catch((error)=>{
						alert("¡Error al cargar las ciudades, ingrese a Trade de nuevo!");
						console.log("error no_internet_loadCiudades 2: " + JSON.stringify(error));
					});
				});
			});
	    });
	}

	limpiarListas(){
		return new Promise((resolve, reject) => {
			this.puntos_no_visitados = [{
				id_p: "",
				nombre: "SIN INFORMACIÓN",
				foto: "",
				tipo_nombre: "",
				tipo_id: "",
				usuario: "",
				direccion: "",
				estado: "0"
			}];
			this.puntos_buscar_ver = [{
					id_p: "",
					nombre: "SIN INFORMACIÓN",
					foto: "",
					tipo_nombre: "",
					tipo_id: "",
					usuario: "",
					direccion: "",
					estado: "0"
				}];
			this.puntos = [{
					id_p: "",
					nombre: "SIN INFORMACIÓN",
					foto: "",
					tipo_nombre: "",
					tipo_id: "",
					usuario: "",
					direccion: "",
					estado: "0"
				}];
			this.ciudades = [{
					id_c: "",
					nombre: "SIN INFORMACIÓN"
				}];
			this.cadenas = [{
					id_c: "",
					nombre: "SIN INFORMACIÓN"
				}];

			this.punto_buscar_ver = this.puntos_buscar_ver[0];
			this.ciudad = this.ciudades[0];
			this.cadena;
			this.punto = this.puntos[0];
			this.punto2 = this.puntos_no_visitados[0];
			resolve(true);
		});
	}

	mostrarpuntosnovisitados(){
		this.mispuntosnovisitadosver = true;
		if(this.internet === true){
			this.internet_loadPuntos_noVisitados();
		}else
			this.no_internet_loadPuntos_noVisitados();
	}

	mostrarCiudades(){
		this.limpiarListas();
		if(this.internet === true){
			this.internet_loadCiudades();
		}else
			this.no_internet_loadCiudades();
	}

	mostrarCadenas(ciudad){
		this.puntos = [{
				id_p: "",
				nombre: "SIN INFORMACIÓN",
				foto: "",
				tipo_nombre: "",
				tipo_id: "",
				usuario: "",
				direccion: "",
				estado: "0"
			}];
		this.cadenas = [{
				id_c: "",
				nombre: "SIN INFORMACIÓN"
			}];
		if(this.internet === true){
			this.internet_loadCadenas(ciudad);
		}else
			this.no_internet_loadCadenas(ciudad);
	}

	mostrarPuntos(ciudad, cadena){
		this.puntos = [{
				id_p: "",
				nombre: "SIN INFORMACIÓN",
				foto: "",
				tipo_nombre: "",
				tipo_id: "",
				usuario: "",
				direccion: "",
				estado: "0"
			}];
		if(this.internet === true){
			this.internet_loadPuntos(ciudad, cadena);
		}else
			this.no_internet_loadPuntos(ciudad, cadena);
	}

	internet_loadCiudades(){
		return new Promise((resolve, reject) => {
			if(this.mispuntos === true)
				this.puntos_traer = '1';
			else	
				this.puntos_traer = '0';
			this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/',{ ciudades: 1, usuario : this.usuario.id_u, puntos_traer: this.puntos_traer},{})
		    .then(data => {
		    	this.ciudades = JSON.parse(data.data);
		    	resolve(true);
			})
		    .catch(error => {
		    	if(error.status >= 500){
		            alert("¡Error, el servidor no responde!");
		        }else
		        if(error.status >= 400){
		            alert("¡Error, no se encuentra el servidor!");
		        }else
		         if(error.status == 0){
		            alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
		        }
			    console.log("error internet_loadCiudades: " + JSON.stringify(error));
			    reject(true);
		 	});
		});
	}

	internet_loadCadenas(ciudad){
		if(this.mispuntos === true)
			this.puntos_traer = '1';
		else	
			this.puntos_traer = '0';
		this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/',{ cadenas: 1, usuario : this.usuario.id_u, ciudad: ciudad, puntos_traer: this.puntos_traer},{})
	    .then(data => {
	    	this.cadenas = JSON.parse(data.data);
		})
	    .catch(error => {
	    	if(error.status >= 500){
	            alert("¡Error, el servidor no responde!");
	        }else
	        if(error.status >= 400){
	            alert("¡Error, no se encuentra el servidor!");
	        }else
	         if(error.status == 0){
	            alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
	        }
		    console.log("error internet_loadCadenas: " + JSON.stringify(error));
	 	});
	}

	internet_loadPuntos(ciudad, cadena){
		if(this.mispuntos === true)
			this.puntos_traer = '1';
		else	
			this.puntos_traer = '0';
		// console.log("usuario : " + this.usuario.id_u + ", ciudad: " + ciudad +  ", cadena: " + cadena +  ", puntos_traer:"  + this.puntos_traer);
		this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/',{ puntos_all: 1, usuario : this.usuario.id_u, ciudad: ciudad, cadena: cadena, puntos_traer: this.puntos_traer},{})
	    .then(data => {
	    	this.puntos = JSON.parse(data.data);
		})
	    .catch(error => {
	    	if(error.status >= 500){
	            alert("¡Error, el servidor no responde!");
	        }else
	        if(error.status >= 400){
	            alert("¡Error, no se encuentra el servidor!");
	        }else
	         if(error.status == 0){
	            alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
	        }
		    console.log("error internet_loadPuntos: " + JSON.stringify(error));
	 	});
	}

	internet_loadPuntos_noVisitados(){
		let loading = this.loadingCtrl.create();
		loading.present();
		this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/',{ puntos_all_no_visitados: 1, usuario : this.usuario.id_u },{})
	    .then(data => {		
	    	this.puntos_no_visitados = JSON.parse(data.data); 
	    	loading.dismiss();
		})
	    .catch(error => {
	    	if(error.status >= 500){
	            alert("¡Error, el servidor no responde!");
	        }else
	        if(error.status >= 400){
	            alert("¡Error, no se encuentra el servidor!");
	        }else
	         if(error.status == 0){
	            alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
	        }
		    console.log("error internet_loadPuntos_noVisitados: " + JSON.stringify(error));
		    loading.dismiss();
	 	});
	}

	internet_buscarPunto(punto){
		let loading = this.loadingCtrl.create();
		loading.present();
		this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/',{ buscarPunto: 1, punto_nombre : punto, usuario : this.usuario.id_u },{})
	    .then(data => {
	    	// console.log("internet_buscarPunto: " + data.data);
	    	this.puntos_buscar_ver = JSON.parse(data.data);
	    	loading.dismiss();
		})
	    .catch(error => {
	    	if(error.status >= 500){
	            alert("¡Error, el servidor no responde!");
	        }else
	        if(error.status >= 400){
	            alert("¡Error, no se encuentra el servidor!");
	        }else
	         if(error.status == 0){
	            alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
	        }
		    console.log("error internet_buscarPunto: " + JSON.stringify(error));
		    loading.dismiss();
	 	});
	}
	
	no_internet_loadCiudades(){
		return new Promise((resolve, reject) => {
			if(this.mispuntos === true)
				this.puntos_traer = '1';
			else	
				this.puntos_traer = '0';
			this.database.loadCiudades(this.usuario.id_u, this.puntos_traer)
	        .then((data:any) => {
	            this.ciudades = data;
	            resolve(true);
	        })
	        .catch(error => {
	        	console.log("error no_internet_loadCiudades: " + JSON.stringify(error));
	        	reject(true);
	        });
	    });
	}

	no_internet_loadCadenas(ciudad){
		if(this.mispuntos === true)
			this.puntos_traer = '1';
		else	
			this.puntos_traer = '0';
		this.database.loadCadenas(this.usuario.id_u, ciudad, this.puntos_traer)
        .then((data:any) => {
            this.cadenas = data;
        })
        .catch(error => console.log("error no_internet_loadCadenas: " + JSON.stringify(error)));
	}

	no_internet_loadPuntos(ciudad, cadena){
		if(this.mispuntos === true)
			this.puntos_traer = '1';
		else	
			this.puntos_traer = '0';
		this.database.loadPuntos(this.usuario.id_u, ciudad, cadena, this.puntos_traer)
        .then((data:any) => {
           	this.puntos = data;
        })
        .catch(error => console.log("error no_internet_loadPuntos: " + JSON.stringify(error)));
	}

	no_internet_loadPuntos_noVisitados(){
		let loading = this.loadingCtrl.create();
		loading.present();
		this.database.loadPuntosMios_noVisitados(this.usuario.id_u)
        .then((data:any) => {
        	// console.log("no_internet_loadPuntos_noVisitados : " + JSON.stringify(data));
           	this.puntos_no_visitados = data;
           	loading.dismiss();
        })
        .catch(error => {
        	console.log("error no_internet_loadPuntos_noVisitados: " + JSON.stringify(error));
        	loading.dismiss();
        });
	}

	no_internet_buscarPunto(punto){
		let loading = this.loadingCtrl.create();
		loading.present();
		this.database.buscarPunto(punto, this.usuario.id_u)
        .then((data:any) => {
        	// console.log("no_internet_buscarPunto : " + JSON.stringify(data));
           	this.puntos_buscar_ver = data;
           	loading.dismiss();
        })
        .catch(error => 
        	{console.log("error no_internet_buscarPunto: " + JSON.stringify(error));
        	loading.dismiss();
        });
	}

	buscarPunto(punto){
		if( punto.length > 1){
			this.punto_buscar_select = 1;
			this.puntos_buscar_ver = [{
				id_p: "",
				nombre: "SIN INFORMACIÓN",
				foto: "",
				tipo_nombre: "",
				tipo_id: "",
				usuario: "",
				direccion: "",
				estado: "0"
			}];
			this.punto_buscar_ver;
			if(this.internet === true){
				this.internet_buscarPunto(punto);
			}else
				this.no_internet_buscarPunto(punto);
		}else	
			alert("¡Ingresa el valor a buscar!");		
	}

	irMenu(datos_punto, buscar = 0){
		let id_punto = "";
		let datos;

		/*else
		if(  this.punto2.id_p.toString().length > 0 ){
			id_punto = this.punto2.id_p;
			datos = {
				id_punto: this.punto2.id_p,
				estado: this.punto2.estado,
				id_u: this.usuario.id_u,
				nombre: this.punto2.nombre,
				foto: this.punto2.foto,
				canal: this.punto2.tipo_nombre,
				tipo: this.punto2.tipo_nombre,
				tipo_id : this.punto2.tipo_id,
				internet: this.internet
			};
		}*/

		if(  (buscar == 1)  && (datos_punto.id_p.toString().length > 1) ){
			id_punto = datos_punto.id_p;
			datos = {
				id_punto: id_punto,
				estado: datos_punto.estado,
				id_u: this.usuario.id_u,
				nombre: datos_punto.nombre,
				foto: datos_punto.foto,
				canal: datos_punto.tipo_nombre,
				tipo: datos_punto.tipo_nombre,
				tipo_id : datos_punto.tipo_id,
				internet: this.internet
			};
		}else{
			id_punto = datos_punto.id_p;
			datos = {
				id_punto: id_punto,
				estado: datos_punto.estado,
				id_u: this.usuario.id_u,
				nombre: datos_punto.nombre,
				foto: datos_punto.foto,
				canal: datos_punto.tipo_nombre,
				tipo: datos_punto.tipo_nombre,
				tipo_id : datos_punto.tipo_id,
				internet: this.internet
			};
		}

		if(id_punto.toString().length > 0){
			this.navCtrl.push(MenuEvaluarPunto, datos);
		}else	
			alert("¡Seleccione un punto!");
	}

}