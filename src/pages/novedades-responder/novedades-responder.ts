import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { HTTP } from '@ionic-native/http';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

@Component({
  selector: 'page-novedades-responder',
  templateUrl: 'novedades-responder.html',
})

export class NovedadesResponderPage {
	publicacion = {
		id_p: "",
		punto_nombre: "",
		foto: "",
		comentario: "",
		respuesta: ""
	};
	ruta_publicaciones;
	respuesta;
	btn_guardar = "Responder Novedad";
	btn_continuar = "Continuar";

	constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer, private http: HTTP, private database: DatabaseServiceProvider) {
		this.ruta_publicaciones = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
	}

	ionViewDidLoad() {
		this.publicacion = this.navParams.get('publicacion');
	}

	getImagen(imagen){
		return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_publicaciones + imagen);	
	}

	guardar(no = ""){
        if( (this.btn_guardar != "Procesando...") ){
        	if(no == "no"){
        			this.btn_continuar = "Procesando...";
					let tamaño = this.navCtrl.length();
		            this.navCtrl.remove((tamaño-1), 1);
			}else{
					if(   this.respuesta != "" ){
		                this.btn_guardar = "Procesando...";
		        			let datos = {
		                            publicacion: this.publicacion.id_p, 
		                            respuesta: this.respuesta
		                        };
		                    this.http.post('http://palacalle.abbottdata.com/backend/v1/novedades/', {responder:1, datos: JSON.stringify(datos)}, {})
		                    .then(data => {
		                        let respuesta = data.data;
		                        if(respuesta == "1"){
		                        	this.database.enviarAvisoUsuario(this.publicacion);
		                            alert("¡Respuesta guardada!");
		                            let tamaño = this.navCtrl.length();
		                            this.navCtrl.remove((tamaño-1), 1);
		                        }else    
		                            alert("¡Ha ocurrido un error al almacenar la información!");
		                    })
		                    .catch(error => {
		                        alert("¡Ha ocurrido un error al enviar la información para ser almacenada!");
		                        console.log(error.status);
		                        console.log(error.error);
		                        this.btn_guardar = "Responder Novedad";
		                    });
		            }else
		                alert("¡Completa todos los campos!");
			}
        }
    }

}
