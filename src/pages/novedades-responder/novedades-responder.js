var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { HTTP } from '@ionic-native/http';
var NovedadesResponderPage = (function () {
    function NovedadesResponderPage(navCtrl, navParams, sanitizer, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sanitizer = sanitizer;
        this.http = http;
        this.publicacion = {
            id_p: "",
            punto_nombre: "",
            foto: "",
            comentario: "",
            respuesta: ""
        };
        this.btn_guardar = "Responder Novedad";
        this.btn_continuar = "Continuar";
        this.ruta_publicaciones = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
    }
    NovedadesResponderPage.prototype.ionViewDidLoad = function () {
        this.publicacion = this.navParams.get('publicacion');
    };
    NovedadesResponderPage.prototype.getImagen = function (imagen) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_publicaciones + imagen);
    };
    NovedadesResponderPage.prototype.guardar = function (no) {
        var _this = this;
        if (no === void 0) { no = ""; }
        if ((this.btn_guardar != "Procesando...")) {
            if (no == "no") {
                this.btn_continuar = "Procesando...";
                var tamaño = this.navCtrl.length();
                this.navCtrl.remove((tamaño - 1), 1);
            }
            else {
                if (this.respuesta != "") {
                    this.btn_guardar = "Procesando...";
                    var datos = {
                        publicacion: this.publicacion.id_p,
                        respuesta: this.respuesta
                    };
                    this.http.post('http://palacalle.abbottdata.com/backend/v1/novedades/', { responder: 1, datos: JSON.stringify(datos) }, {})
                        .then(function (data) {
                        var respuesta = data.data;
                        if (respuesta == "1") {
                            alert("¡Respuesta guardada!");
                            var tamaño = _this.navCtrl.length();
                            _this.navCtrl.remove((tamaño - 1), 1);
                        }
                        else
                            alert("¡Ha ocurrido un error al almacenar la información!");
                    })
                        .catch(function (error) {
                        alert("¡Ha ocurrido un error al enviar la información para ser almacenada!");
                        console.log(error.status);
                        console.log(error.error);
                        _this.btn_guardar = "Responder Novedad";
                    });
                }
                else
                    alert("¡Completa todos los campos!");
            }
        }
    };
    return NovedadesResponderPage;
}());
NovedadesResponderPage = __decorate([
    Component({
        selector: 'page-novedades-responder',
        templateUrl: 'novedades-responder.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, DomSanitizer, HTTP])
], NovedadesResponderPage);
export { NovedadesResponderPage };
//# sourceMappingURL=novedades-responder.js.map