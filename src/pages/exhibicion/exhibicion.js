var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, LoadingController, Platform } from 'ionic-angular';
import { AgotadosPage } from '../agotados/agotados';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { CamaraProvider } from '../../providers/camara/camara';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var ExhibicionPage = (function () {
    function ExhibicionPage(navCtrl, navParams, http, sanitizer, cameraProvider, actionSheetCtrl, loadingCtrl, platform, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.sanitizer = sanitizer;
        this.cameraProvider = cameraProvider;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.database = database;
        this.punto = {
            id_p: "",
            usuario: "",
            nombre: "",
            foto: "",
            canal: "",
            tipo: "",
            tipo_id: ""
        };
        this.comentario = "";
        this.foto = false;
        this.mostrarExhibicion = 0;
        this.btn_guardar = "Guardar Valoración";
        this.btn_continuar = "Continuar";
        if (platform.is('ios')) {
            this.mobile = "library-nosync";
        }
        else if (platform.is('android')) {
            this.mobile = "files";
        }
    }
    ExhibicionPage.prototype.ionViewDidLoad = function () {
        this.ruta_publicaciones = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
        this.punto = {
            id_p: this.navParams.get('id_punto'),
            usuario: this.navParams.get('usuario'),
            nombre: this.navParams.get('nombre'),
            foto: this.navParams.get('foto'),
            canal: this.navParams.get('canal'),
            tipo: this.navParams.get('tipo'),
            tipo_id: this.navParams.get('tipo_id')
        };
        if (this.navParams.get('internet') == true) {
            this.internet = true;
            this.ruta = "http://palacalle.abbottdata.com/imagenes/tipospunto/";
            if (this.navParams.get('publicacion')) {
                this.cargarExhibicion(this.navParams.get('publicacion'));
            }
        }
        else {
            this.internet = false;
            this.ruta = "cdvfile://localhost/" + this.mobile + "/tipospunto/";
        }
    };
    ExhibicionPage.prototype.subirFoto = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Subir Imágen',
            buttons: [
                {
                    text: 'Camara',
                    icon: 'camera',
                    handler: function () {
                        _this.chosenPicture = "assets/loader.gif";
                        _this.cameraProvider.getPictureFromCamera().then(function (picture) {
                            if (picture) {
                                _this.chosenPicture = picture[0];
                                _this.imagenUpload = picture[1];
                                _this.foto = true;
                            }
                        }, function (error) {
                            alert("Error, Vuelva a subir la imágen");
                            _this.foto = false;
                            _this.chosenPicture = "";
                        });
                    }
                }, {
                    text: 'Cancelar',
                    icon: 'close',
                    role: 'destructive',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ExhibicionPage.prototype.irAgotados = function (mostrar) {
        if (mostrar === void 0) { mostrar = ""; }
        if ((this.punto.id_p != "") || (this.punto.usuario != "") || (this.punto.canal != "")) {
            if ((this.btn_guardar != "Procesando...") || (this.btn_continuar != "Procesando...")) {
                if (mostrar == "") {
                    if ((this.comentario.length > 0) && (this.uno != "") && (this.dos != "") && (this.tres != "") && (this.cuatro != "") && (this.cinco != "") && (this.foto == true)) {
                        this.btn_guardar = "Procesando...";
                        var datos = {
                            comentario: this.comentario,
                            uno: this.uno,
                            dos: this.dos,
                            tres: this.tres,
                            cuatro: this.cuatro,
                            cinco: this.cinco,
                            foto: this.imagenUpload
                        };
                        this.navCtrl.push(AgotadosPage, {
                            id_punto: this.punto.id_p,
                            usuario: this.punto.usuario,
                            nombre: this.punto.nombre,
                            canal: this.punto.canal,
                            tipo: this.punto.tipo,
                            tipo_id: this.punto.tipo_id,
                            exhibicion: datos,
                            internet: this.internet
                        });
                    }
                    else
                        alert("¡Completa todos los campos!");
                }
                else {
                    this.btn_continuar = "Procesando...";
                    var datos_1 = {
                        publicacion: this.navParams.get('publicacion'),
                        id_punto: this.punto.id_p,
                        usuario: this.punto.usuario,
                        nombre: this.punto.nombre,
                        canal: this.punto.canal,
                        tipo: this.punto.tipo,
                        tipo_id: this.punto.tipo_id,
                        exhibicion: {}
                    };
                    this.navCtrl.push(AgotadosPage, datos_1);
                }
            }
        }
        else
            alert("¡No se ha cargado bien la información del punto!");
    };
    ExhibicionPage.prototype.getImagen = function (imagen, ruta) {
        if (ruta === void 0) { ruta = ""; }
        if (ruta == "publicacion") {
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_publicaciones + imagen);
        }
        else {
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta + imagen);
        }
    };
    ExhibicionPage.prototype.cargarExhibicion = function (id_publicacion) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://palacalle.abbottdata.com/backend/v1/exhibiciones/', { cargarExhibicion: 1, publicacion: id_publicacion }, {})
            .then(function (data) {
            _this.mostrarExhibicion = 1;
            _this.datosExhibicion = JSON.parse(data.data);
            _this.punto = {
                id_p: _this.datosExhibicion.id_p,
                usuario: _this.datosExhibicion.usuario,
                nombre: _this.datosExhibicion.nombre,
                foto: _this.datosExhibicion.foto,
                canal: _this.datosExhibicion.canal,
                tipo: _this.datosExhibicion.tipo,
                tipo_id: _this.datosExhibicion.tipo_id
            };
            _this.comentario = _this.datosExhibicion.comentario;
            _this.uno = String(_this.datosExhibicion.r1);
            _this.dos = String(_this.datosExhibicion.r2);
            _this.tres = String(_this.datosExhibicion.r3);
            _this.cuatro = String(_this.datosExhibicion.r4);
            _this.cinco = String(_this.datosExhibicion.r5);
            _this.foto_exhibicion = id_publicacion + ".png";
            loading.dismiss();
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
            loading.dismiss();
        });
    };
    return ExhibicionPage;
}());
ExhibicionPage = __decorate([
    Component({
        selector: 'page-exhibicion',
        templateUrl: 'exhibicion.html',
        providers: [HTTP]
    }),
    __metadata("design:paramtypes", [NavController, NavParams, HTTP, DomSanitizer, CamaraProvider, ActionSheetController, LoadingController, Platform, DatabaseServiceProvider])
], ExhibicionPage);
export { ExhibicionPage };
//# sourceMappingURL=exhibicion.js.map