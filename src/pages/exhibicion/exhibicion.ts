import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, ViewController, LoadingController, Platform } from 'ionic-angular';
import { ExhibicionAdicionalPage } from '../exhibicion-adicional/exhibicion-adicional';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { CamaraProvider } from '../../providers/camara/camara';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
import { Storage } from '@ionic/storage';
import { MenuEvaluarPunto } from '../menu-evaluar-punto/menu-evaluar-punto';

@Component({
    selector: 'page-exhibicion',
    templateUrl: 'exhibicion.html',
    providers: [HTTP]
})
export class ExhibicionPage {
    id_punto; 
    ruta; 
    tipo;
    ruta_publicaciones;
    punto = {
        id_p: "",
        usuario : "",
        nombre: "", 
        foto: "", 
        canal: "", 
        tipo: "",
        tipo_id: ""
    };
    punto_final;
    comentario = "";
    id_califica:string;
    chosenPicture: any;
    imagenUpload = "";
    foto:boolean = false;
    datosExhibicion;
    mostrarExhibicion = 0;
    foto_exhibicion;
    btn_guardar = "Guardar Valoración";
    btn_continuar = "Continuar";
    internet;
    mobile;

    /***** FINAL */
    exhibicionAdicional = [];
    usuario;
    esSupervisor = false;

    constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, private sanitizer: DomSanitizer, public cameraProvider: CamaraProvider, public actionSheetCtrl: ActionSheetController, public loadingCtrl: LoadingController, public viewCtrl: ViewController, platform: Platform, private database: DatabaseServiceProvider, private storage: Storage, public modalCtrl: ModalController) {
        if (platform.is('ios')) {
            this.mobile = "library-nosync";
        }else
        if (platform.is('android')) {
            this.mobile = "files";
        }
        this.storage.get('usuario')
        .then((val) => {
            this.usuario = val;
            if( this.usuario.tipo == "s"){
                this.esSupervisor = true;
            }
        }); 
    }

    ionViewDidLoad() {
        this.ruta_publicaciones = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
        this.punto = {
            id_p : this.navParams.get('id_punto'),
            usuario : this.navParams.get('usuario'),
            nombre: this.navParams.get('nombre'), 
            foto : this.navParams.get('foto'),
            canal: this.navParams.get('canal'), 
            tipo: this.navParams.get('tipo'),
            tipo_id: this.navParams.get('tipo_id')
        };
        this.punto_final = {
                  exhibicion: this.navParams.get('exhibicion'),
                  agotados: this.navParams.get('agotados'),
                  precio: this.navParams.get('precio'),
                  exhibicionesAdicionales: this.navParams.get('exhibicionesAdicionales'),
                  secuenciaExhibicionAdicional : this.navParams.get('secuenciaExhibicionAdicional'),
                  exhibicionAdicionalDatos : this.navParams.get('exhibicionAdicionalDatos')
              };
        if(this.navParams.get('internet') == true){
            this.internet = true;
            this.ruta = "http://palacalle.abbottdata.com/imagenes/tipospunto/";
            if( this.navParams.get('publicacion') ){
                this.cargarExhibicion(this.navParams.get('publicacion'));
            }
        }else{
            this.internet = false;
            this.ruta = "cdvfile://localhost/" + this.mobile + "/tipospunto/";
        }  
    }

    getImagen(imagen, ruta = ""){
        if(ruta == "publicacion"){
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_publicaciones + imagen);
        }else{
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta + imagen);
        }    
    }

    tomarFoto() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Tomar Foto',
            buttons: [
            {
                text: 'Cámara',
                icon: 'camera',
                handler: () => {
                    this.chosenPicture = "assets/loader.gif";
                    this.cameraProvider.getPictureFromCamera().then(picture => {
                      if (picture) {
                        this.chosenPicture = picture[0];
                        this.imagenUpload = picture[1];
                        this.foto = true;
                        
                      }
                    }, error => {
                      alert("Error, Vuelva a tomar la foto");
                      this.foto = false;
                      this.chosenPicture = "";
                    });
                }
            },{
                text: 'Cancelar',
                icon: 'close',
                role: 'destructive',
                handler: () => {
                }
            }
            ]
        });
        actionSheet.present();
    }

    cargarExhibicion(id_publicacion){
        let loading = this.loadingCtrl.create();
        loading.present();
        console.log();
        this.http.get('http://palacalle.abbottdata.com/backend/v1/exhibiciones/', {cargarExhibicion: 1, publicacion: id_publicacion}, {})
        .then(data => {
            this.mostrarExhibicion = 1;
            this.datosExhibicion = JSON.parse(data.data);
            this.punto = {
                id_p : this.datosExhibicion.id_p,
                usuario : this.datosExhibicion.usuario,
                nombre: this.datosExhibicion.nombre,
                foto : this.datosExhibicion.foto,
                canal: this.datosExhibicion.canal,
                tipo: this.datosExhibicion.tipo,
                tipo_id: this.datosExhibicion.tipo_id
            };
            this.comentario = this.datosExhibicion.comentario;
            this.foto_exhibicion = id_publicacion + ".png";
            loading.dismiss();
        })
        .catch(error => {
            this.mostrarExhibicion = 1;
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("cargarExhibicion error : " + JSON.stringify(error));
            loading.dismiss();
        });
    }


    continuar(mostrar = ""){
        if( (this.punto.id_p != "") ||  (this.punto.usuario != "") ||  (this.punto.canal != "") ){
            if( (this.btn_guardar != "Procesando...")  || (this.btn_continuar != "Procesando...")){
                if(mostrar == ""){
                    if( this.imagenUpload.length > 10 ){
                        if(confirm('¿Está seguro de continuar?')){  
                            this.btn_guardar = "Procesando...";
                            let button = document.getElementById("guardar");
                            button.remove();
                            /* Exhibicion */
                            var datos_exhibicion = this.navParams.get('exhibicion');
                            datos_exhibicion.comentario = this.comentario;
                            datos_exhibicion.foto = this.imagenUpload;
                            if(this.punto_final.exhibicionesAdicionales.length > 0){
                                this.navCtrl.push(ExhibicionAdicionalPage, {
                                        id_punto: this.punto.id_p, 
                                        usuario: this.punto.usuario, 
                                        nombre: this.punto.nombre, 
                                        foto: this.punto.foto,
                                        canal: this.punto.canal, 
                                        tipo: this.punto.tipo,
                                        tipo_id: this.punto.tipo_id,
                                        exhibicion: this.punto_final.exhibicion,
                                        agotados: this.punto_final.agotados,
                                        precio: this.punto_final.precio,
                                        exhibicionesAdicionales: this.punto_final.exhibicionesAdicionales,
                                        secuenciaExhibicionAdicional: 1,
                                        exhibicionAdicionalDatos: [],
                                        internet: this.internet
                                    });
                            }else{
                                let datos = {
                                        id_punto: this.punto.id_p, 
                                        usuario: this.punto.usuario, 
                                        nombre: this.punto.nombre, 
                                        foto: this.punto.foto,
                                        canal: this.punto.canal, 
                                        tipo: this.punto.tipo,
                                        tipo_id: this.punto.tipo_id,
                                        exhibicion: this.punto_final.exhibicion,
                                        agotados: this.punto_final.agotados,
                                        precio: this.punto_final.precio,
                                        internet: this.internet
                                    };
                                if(this.internet === true){
                                    this.guardarPublicacion_Internet(datos);
                                }
                                else{
                                    this.guardarPublicacion_noInternet(datos);
                                }
                            } // end else
                        } // end else confirmar
                    }else
                        alert("¡Toma una foto!");
                }else{
                    this.btn_continuar = "Procesando...";
                    if(this.punto_final.exhibicionesAdicionales.length > 0){
                        let datos = {
                                cargarInfo: true,
                                publicacion: this.navParams.get('publicacion'),
                                id_punto: this.punto.id_p, 
                                usuario: this.punto.usuario, 
                                nombre: this.punto.nombre, 
                                foto: this.punto.foto,
                                canal: this.punto.canal, 
                                tipo: this.punto.tipo,
                                tipo_id: this.punto.tipo_id,
                                exhibicion: this.punto_final.exhibicion,
                                agotados: this.punto_final.agotados,
                                precio: this.punto_final.precio,
                                exhibicionesAdicionales: this.punto_final.exhibicionesAdicionales,
                                secuenciaExhibicionAdicional: 1,
                                exhibicionAdicionalDatos: [],
                                internet: true
                            };
                        this.navCtrl.push(ExhibicionAdicionalPage, datos); 
                    }else{
                        if(this.esSupervisor == true)
                            this.presentCerrarCasoModal();
                        else{
                            let tamaño = this.navCtrl.length();
                            this.navCtrl.remove((tamaño-4), 4);
                        }                    
                    }
                } 
            }
        }else
            alert("¡No se ha cargado bien la información del punto!");
    }

    // **************************************** 
    // ****** ENVIAR O GUARDAR
    // ****************************************

    regresarVista(internet){
        let args;
        if(internet === true){
            args = {
                    id_punto: this.punto.id_p,
                    estado: 2,
                    id_u: this.punto.usuario,
                    internet : true
                };
            let tamaño = this.navCtrl.length();
            this.navCtrl.remove((tamaño-5), 5, args)
            .then(()=>{
                console.log(args);
                
                this.navCtrl.push(MenuEvaluarPunto, args);
            })
            .catch((error)=>{
                console.log("error this.navCtrl.remove((tamaño-5), 5, args) - 1 " + JSON.stringify(error));
            });
        }else{
            let foto;
            let tamaño = this.navCtrl.length();
            this.database.fotoTipoPunto(this.punto.tipo_id)
            .then((data)=>{
                foto = data;
                args = {
                        id_punto: this.punto.id_p,
                        estado: 2,
                        id_u: this.punto.usuario,
                        tipo: this.punto.canal + "-" + this.punto.tipo,
                        nombre: this.punto.nombre,
                        foto: this.punto.tipo_id + "." + foto,
                        tipo_id: this.punto.tipo_id,
                        internet: false,
                        califico: "p"
                    };
                this.navCtrl.remove((tamaño-5), 5, args)
                .then(()=>{
                    this.navCtrl.push(MenuEvaluarPunto, args);
                })
                .catch((error)=>{
                    console.log("error this.navCtrl.remove((tamaño-5), 5, args) - 2 " + JSON.stringify(error));
                });
            })
            .catch((error)=>{
                foto = "jpg";
                args = {
                        id_punto: this.punto.id_p,
                        estado: 2,
                        id_u: this.punto.usuario,
                        tipo: this.punto.tipo,
                        nombre: this.punto.nombre,
                        foto: this.punto.tipo_id + "." + foto,
                        tipo_id: this.punto.tipo_id,
                        internet: false,
                        califico: "p"
                    };
                this.navCtrl.remove((tamaño-5), 5, args)
                .then(()=>{
                    this.navCtrl.push(MenuEvaluarPunto, args);
                })
                .catch((error)=>{
                    console.log("error this.navCtrl.remove((tamaño-5), 5, args) - 3 " + JSON.stringify(error));
                });
                console.log("error this.database.fotoTipoPunto(this.punto.tipo_id) " + JSON.stringify(error));
            });
        } 
    }

    guardarPublicacion_Internet(datos){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {guardar:1, datos: JSON.stringify(datos)}, {})
        .then(data => {
            let respuesta = JSON.parse(data.data);
            if(respuesta > 0){
                this.subirImagen(datos.exhibicion.foto, respuesta, "publicaciones");
                let publicacion = {
                    id_p: respuesta,
                    punto: datos.id_punto
                };
                this.database.enviarAvisoSupervisor(publicacion);
            }else{
                alert("¡Ha ocurrido un error al almacenar la información!");
                this.regresarVista(true);
            }
            loading.dismiss();
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("error guardarPublicacion_Internet: " + JSON.stringify(error));
            loading.dismiss();
        });
    }    

    guardarPublicacion_noInternet(datos){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.database.guardarPublicacion_noInternet(datos)
        .then((data)=>{
            let respuesta = data;
            if(respuesta > 0)
                this.regresarVista(false);
            else{
               alert("¡Ha ocurrido un error al almacenar la información!");
               this.regresarVista(false);
            }
            loading.dismiss();    
        })
        .catch((error)=>{
            console.log("error guardarPublicacion_noInternet: " + JSON.stringify(error));
            loading.dismiss();
        });
    }

    subirImagen(foto, respuesta, lugar){
        this.cameraProvider.cargarFotoServidor(foto, respuesta, lugar)
        .then(()=> {
            this.regresarVista(true);
        })
        .catch(error => {
            alert("¡Ha ocurrido un error al cargar la imágen!");
            this.regresarVista(true);
        });
    }

    presentCerrarCasoModal() {
        let cerrarCaso = this.modalCtrl.create(CerrarCasoModal, {publicacion: this.navParams.get('publicacion') });
        cerrarCaso.onDidDismiss(data => {
            let tamano = this.navCtrl.length();
            this.navCtrl.remove((tamano-4), 4);
        });
        cerrarCaso.present();
    }

}


@Component({
    template: `<ion-content class="fondo center">
                <div class="box-center">
                    <div>
                        <h3 class="center">Punto Evaluado</h3>
                        <div>He entendido toda la evaluación.</div>
                        <div>Es hora de gestionar y optimizar mi punto.</div>
                        <button class="btn btn-rounded btn-azul marginTop1" (click)="regresarCasos()">Regresar a Casos</button>
                        <button class="btn btn-rounded btn-azul marginTop1" (click)="cerrarCaso()">Cerrar Caso</button>
                    </div>
                </div>
            </ion-content>`,
    styles: [`
        .fondo{
            width: 100% !important;
            height: 100% !important;
            background: #fff !important;
        }
        button ion-icon, button{
            color: white !important;
        }
        .box-center {
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
      `]
})

export class CerrarCasoModal {
    publicacion = 0;

    constructor(public navCtrl: NavController, private http: HTTP, params: NavParams, public viewCtrl: ViewController) {
        this.publicacion = params.get('publicacion');
    }

    regresarCasos(){
        this.viewCtrl.dismiss();
    }

    cerrarCaso(){
        this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {cerrarCaso: this.publicacion }, {})
        .then(data => {
            if(data.data == '1'){
                this.viewCtrl.dismiss();
            }else
                alert("¡Error al cerrar el caso!");
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("error cerrarCaso: " + JSON.stringify(error));
        });
            
    }

}