var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Storage } from '@ionic/storage';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var RankingPage = (function () {
    function RankingPage(navCtrl, navParams, http, sanitizer, storage, database) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.sanitizer = sanitizer;
        this.storage = storage;
        this.database = database;
        this.ranking = "Trade";
        this.yo = 0;
        this.ruta_usuarios = "http://palacalle.abbottdata.com/imagenes/usuarios/";
        this.storage.get('usuario')
            .then(function (val) {
            _this.usuario = val;
        });
    }
    RankingPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.database.probarConexion()
            .then(function () {
            _this.internet = true;
            _this.trer_ranking_trade();
            _this.trer_ranking_acompanamiento();
        })
            .catch(function () {
            _this.internet = false;
        });
    };
    RankingPage.prototype.getImagen = function (imagen) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_usuarios + imagen);
    };
    RankingPage.prototype.trer_ranking_trade = function () {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v0/ranking/', { trade: 1 }, {})
            .then(function (data) {
            _this.ranking_trade = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta, en el proceso de consultar el ranking Trade!");
            }
            console.log(JSON.stringify(error));
        });
    };
    RankingPage.prototype.trer_ranking_acompanamiento = function () {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v0/ranking/', { acompañamiento: 1 }, {})
            .then(function (data) {
            _this.ranking_acompanamiento = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta, en el proceso de consultar el ranking de Acompañamiento!");
            }
            console.log(JSON.stringify(error));
        });
    };
    return RankingPage;
}());
RankingPage = __decorate([
    Component({
        selector: 'page-ranking',
        templateUrl: 'ranking.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, HTTP, DomSanitizer, Storage, DatabaseServiceProvider])
], RankingPage);
export { RankingPage };
//# sourceMappingURL=ranking.js.map