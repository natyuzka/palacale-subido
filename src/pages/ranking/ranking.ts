import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Storage } from '@ionic/storage';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';


@Component({
	selector: 'page-ranking',
	templateUrl: 'ranking.html',
})
export class RankingPage {
	ranking = "Trade";
	ranking_trade;
	ranking_acompanamiento;
	ruta_usuarios;
	usuario;
	yo = 0;
	internet;

	constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, private sanitizer: DomSanitizer, private storage: Storage, private database: DatabaseServiceProvider) {
		this.ruta_usuarios = "http://palacalle.abbottdata.com/imagenes/usuarios/";
		this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
			});
	}

	ionViewDidEnter() {
		this.database.probarConexion()
        .then(() => {
        	this.internet = true;
            this.trer_ranking_trade();
			this.trer_ranking_acompanamiento();
        })
        .catch(() => {
        	this.internet = false;
        	this.ranking_trade = [];
			this.ranking_acompanamiento = [];
        });
	}

	getImagen(imagen){
		return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_usuarios + imagen);	
	}

	trer_ranking_trade(){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/ranking/', {trade: 1}, {})
	    .then(data => {
			this.ranking_trade = JSON.parse(data.data);
		})
	    .catch(error => {
	    	if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
		    console.log(JSON.stringify(error));
	 	});
	}

	trer_ranking_acompanamiento(){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/ranking/', {acompañamiento: 1}, {})
	    .then(data => {
			this.ranking_acompanamiento = JSON.parse(data.data);
		})
	    .catch(error => {
	    	if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
		    console.log(JSON.stringify(error));
	 	});
	}

}