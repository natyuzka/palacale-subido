var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NovedadesPage } from '../novedades/novedades';
import { ExhibicionPage } from '../exhibicion/exhibicion';
import { HTTP } from '@ionic-native/http';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var MenuEvaluarPunto = (function () {
    function MenuEvaluarPunto(navCtrl, navParams, http, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.database = database;
        this.novedad_evaluado = false;
        this.punto_evaluado = false;
        this.punto = {
            nombre: "",
            foto: "",
            canal: "",
            tipo: "",
            tipo_id: ""
        };
    }
    MenuEvaluarPunto.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.id_punto = this.navParams.get('id_punto');
        this.estado = this.navParams.get('estado');
        this.id_u = this.navParams.get('id_u');
        if (this.navParams.get('internet') === true) {
            this.internet = true;
            this.punto_info(this.id_punto, this.id_u);
        }
        else {
            this.internet = false;
            this.database.puntoEstado(this.id_punto, this.id_u)
                .then(function (data) {
                var tipo = _this.navParams.get('tipo');
                tipo = tipo.split("-");
                _this.punto = {
                    nombre: _this.navParams.get('nombre'),
                    foto: _this.navParams.get('foto'),
                    canal: tipo[0],
                    tipo: tipo[1],
                    tipo_id: _this.navParams.get('tipo_id')
                };
                if (data[0] == "1") {
                    _this.punto_evaluado = true;
                }
                if (data[1] == "1") {
                    _this.novedad_evaluado = true;
                }
                if (_this.navParams.get('califico') == "n") {
                    if (_this.navParams.get('estado') == 2) {
                        _this.novedad_evaluado = true;
                    }
                }
                if (_this.navParams.get('califico') == "p") {
                    if (_this.navParams.get('estado') == 2) {
                        _this.punto_evaluado = true;
                    }
                }
            })
                .catch(function (error) {
                alert("Error database.puntoEstado: " + JSON.stringify(error));
            });
        }
    };
    MenuEvaluarPunto.prototype.punto_info = function (punto, usuario) {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/', { punto: punto, usuario: usuario }, {})
            .then(function (data) {
            var res = JSON.parse(data.data);
            _this.punto = res.datos_punto;
            if (res.datos_publicaciones[0] == "1") {
                _this.punto_evaluado = true;
            }
            if (res.datos_publicaciones[1] == "1") {
                _this.novedad_evaluado = true;
            }
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta, en el proceso de consultar la información del punto!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    MenuEvaluarPunto.prototype.irA = function (ventana) {
        if ((this.id_punto != "") || (this.estado != "") || (this.id_u != "") || (this.punto.nombre != "") || (this.punto.canal != "") || (this.punto.tipo != "")) {
            var continuar = 1;
            if (ventana == "n") {
                this.page = NovedadesPage;
                if (this.novedad_evaluado == true) {
                    continuar = 0;
                }
            }
            else {
                this.page = ExhibicionPage;
                if (this.punto_evaluado == true) {
                    continuar = 0;
                }
                if (this.estado == 0) {
                    continuar = 2;
                }
            }
            if (continuar == 1) {
                this.navCtrl.push(this.page, {
                    id_punto: this.id_punto,
                    usuario: this.id_u,
                    nombre: this.punto.nombre,
                    foto: this.punto.foto,
                    canal: this.punto.canal,
                    tipo: this.punto.tipo,
                    tipo_id: this.punto.tipo_id,
                    internet: this.internet
                });
            }
            else if (continuar == 0) {
                alert("¡No puede volver a evaluarlo, debe esperar 3 días desde la anterior evaluación!");
            }
            else
                alert("¡No tiene permiso para evaluar el punto!");
        }
        else
            alert("¡No se ha cargado bien la información del punto!");
    };
    return MenuEvaluarPunto;
}());
MenuEvaluarPunto = __decorate([
    Component({
        selector: 'page-menu-evaluar-punto',
        templateUrl: 'menu-evaluar-punto.html',
        providers: [HTTP]
    }),
    __metadata("design:paramtypes", [NavController, NavParams, HTTP, DatabaseServiceProvider])
], MenuEvaluarPunto);
export { MenuEvaluarPunto };
//# sourceMappingURL=menu-evaluar-punto.js.map