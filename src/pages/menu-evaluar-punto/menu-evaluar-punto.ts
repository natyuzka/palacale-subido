import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { NovedadesPage } from '../novedades/novedades';
// import { ExhibicionPage } from '../exhibicion/exhibicion';
import { ExhibicionFormulasPage } from '../exhibicion-formulas/exhibicion-formulas';
import { HTTP } from '@ionic-native/http';

import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

@Component({
	selector: 'page-menu-evaluar-punto',
	templateUrl: 'menu-evaluar-punto.html',
	providers: [HTTP]
})

export class MenuEvaluarPunto {

	page;
	estado;
	id_u;
	id_punto;
	novedad_evaluado:boolean = false;
	punto_evaluado:boolean = false;
	punto = {
		nombre: "", 
		foto: "", 
		canal: "", 
		tipo: "",
		tipo_id: ""
	};
	internet;

	constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, private database: DatabaseServiceProvider, public loadingCtrl: LoadingController) {}

	ionViewDidLoad() {
		this.id_punto = this.navParams.get('id_punto');
		this.estado = this.navParams.get('estado');
		this.id_u = this.navParams.get('id_u');
		if(this.navParams.get('internet') === true){
			this.internet = true;
	    	this.punto_info(this.id_punto, this.id_u);
		}else{
			this.internet = false;
	    	this.database.puntoEstado(this.id_punto, this.id_u)
	    	.then((data)=>{
	    		let tipo = this.navParams.get('tipo');
	    		tipo = tipo.split("-");
	    		let tipo1 = "";
	    		if ( tipo.hasOwnProperty(1) ){
	    			tipo1 = tipo[1];
	    		}
				this.punto = {
					nombre: this.navParams.get('nombre'),
					foto: this.navParams.get('foto'),
					canal: tipo[0],
					tipo: tipo1,
					tipo_id: this.navParams.get('tipo_id')
				};
				if(data[0] == "1"){
					this.punto_evaluado = true;
				}
				if(data[1] == "1"){
					this.novedad_evaluado = true;
				}
				if(this.navParams.get('califico') == "n"){
					if(this.navParams.get('estado') == 2){
						this.novedad_evaluado = true;
					}
				}
				if(this.navParams.get('califico') == "p"){
					if(this.navParams.get('estado') == 2){
						this.punto_evaluado = true;
					}
				}
	    	})
	    	.catch((error)=>{
	    		console.log("Error database.puntoEstado: " + JSON.stringify(error));
	    	});
		}
	}

	punto_info(punto, usuario){
		let loading = this.loadingCtrl.create();
        loading.present();
		this.http.get('http://palacalle.abbottdata.com/backend/v1/puntos/', {punto:  punto, usuario : usuario}, {})
		.then(data => {
			let res = JSON.parse(data.data);
			this.punto = res.datos_punto;
			if(res.datos_publicaciones[0] == "1"){
				this.punto_evaluado = true;
			}
			if(res.datos_publicaciones[1] == "1"){
				this.novedad_evaluado = true;
			}
			loading.dismiss();
		})
		.catch(error => {
			loading.dismiss();
			console.log("error: " + JSON.stringify(error));
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log(error.status);
			console.log(error.error);
		});
	}

	irA(ventana){
		if( (this.id_punto != "") ||  (this.estado != "") ||  (this.id_u != "") || (this.punto.nombre != "") ||  (this.punto.canal != "") ||  (this.punto.tipo != "")  ){
			let continuar = 1;
			if(ventana == "n"){
				this.page = NovedadesPage;
				if(this.novedad_evaluado == true){
					continuar = 0;
				}
			}else{
				this.page = ExhibicionFormulasPage;
				if(this.punto_evaluado == true){
					continuar = 0;
				}
				if(this.estado == 0){
					continuar = 2;
				}
			}

			if(continuar == 1){
				this.navCtrl.push(this.page, {
					id_punto: this.id_punto, 
					usuario: this.id_u, 
					nombre: this.punto.nombre, 
					foto: this.punto.foto,
					canal: this.punto.canal, 
					tipo: this.punto.tipo,
					tipo_id: this.punto.tipo_id,
					internet : this.internet
				});
			}else
			if(continuar == 0){
				alert("¡No puede volver a evaluarlo, debe esperar 10 días desde la anterior evaluación!");
			}else
				alert("¡No tiene permiso para evaluar el punto!");
		}else
			alert("¡No se ha cargado bien la información del punto!");
			
	}

}