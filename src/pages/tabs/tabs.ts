import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MapaPage } from '../mapa/mapa';
import { PublicacionesPage } from '../publicaciones/publicaciones';
import { MenuAcompanamientoPage } from '../menu-acompanamiento/menu-acompanamiento';
import { RankingPage } from '../ranking/ranking';
import { SettingsPage } from '../settings/settings';



@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PublicacionesPage;
  tab2Root = MapaPage;
  tab3Root = MenuAcompanamientoPage;
  tab4Root = RankingPage;
  tab5Root = SettingsPage;

  constructor(public navCtrl: NavController) {}
}
