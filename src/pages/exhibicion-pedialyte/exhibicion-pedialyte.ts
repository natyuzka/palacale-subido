import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NavController, NavParams, LoadingController, Platform  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { ExhibicionPage } from '../exhibicion/exhibicion';

import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
import { CamaraProvider } from '../../providers/camara/camara';

@Component({
	selector: 'page-exhibicion-pedialyte',
	templateUrl: 'exhibicion-pedialyte.html',
})

export class ExhibicionPedialytePage {

	id_punto; 
    ruta; 
    tipo;
    punto = {
        id_p : "",
        usuario : "",
        nombre: "", 
        foto: "",
        canal: "", 
        tipo: "",
        tipo_id: ""
    };

    id_califica:string;
    cinco:string = "";
    datosExhibicion;
    mostrarExhibicion = 0;
    preguntas = {
        cinco: ""
    };
    sub_tabs = "Imagen";
    btn_guardar = "Continuar Valoración";
    btn_continuar = "Continuar";
    internet;
    mobile;

    /***** AGOTADOS */
    productos_agotados: Array<string>;
	agotados = [];	
	mostrarAgotados = 0;
    sinAgotados = 0;

 	/***** PRECIO */
	productos_precio: Array<string>;
	precio = [];
	mostrarPrecios = 0;
    sinPrecio = 0;

	/***** FINAL */
	exhibicionAdicional = [];
    ruta_tipospunto = "";

	constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, private sanitizer: DomSanitizer, public cameraProvider: CamaraProvider, public loadingCtrl: LoadingController, private storage: Storage, private database: DatabaseServiceProvider, platform: Platform) {
  		if (platform.is('ios')) {
            this.mobile = "library-nosync";
        }else
        if (platform.is('android')) {
            this.mobile = "files";
        }
  	}

	ionViewDidLoad() {
		this.punto = {
            id_p : this.navParams.get('id_punto'),
            usuario : this.navParams.get('usuario'),
            nombre: this.navParams.get('nombre'), 
            foto : this.navParams.get('foto'),
            canal: this.navParams.get('canal'), 
            tipo: this.navParams.get('tipo'),
            tipo_id: this.navParams.get('tipo_id')
        };

        if(this.navParams.get('internet') == true){
            this.internet = true;
            this.ruta = "http://palacalle.abbottdata.com/imagenes/productos/";
            this.ruta_tipospunto = "http://palacalle.abbottdata.com/imagenes/tipospunto/";
            if( this.navParams.get('publicacion') ){
                this.cargarExhibicion(this.navParams.get('publicacion'));
		        this.cargarAgotados(this.navParams.get('publicacion'), this.navParams.get('tipo_id'));
		        this.cargarPrecios(this.navParams.get('publicacion'));
            }else{
                this.agotados_info(this.punto.tipo_id);
                this.precio_info(this.punto.tipo_id);
            }
            this.exhibicionesAdicionales(this.punto.id_p);
        }else{
        	this.internet = false;
            this.ruta = "cdvfile://localhost/" + this.mobile + "/productos/";
            this.ruta_tipospunto = "cdvfile://localhost/" + this.mobile + "/tipospunto/";

            this.database.agotados(this.punto.tipo_id, "p")
	    	.then((data)=>{
	    		this.colocarAgotados(data);
	    	})
	    	.catch((error)=>{
	    		console.log("Error database.agotados: " + JSON.stringify(error));
	    	});

	    	this.database.precio(this.punto.tipo_id, "p")
            .then((data)=>{
                this.colocarPrecio(data);
            })
            .catch((error)=>{
                console.log("Error database.precio: " + JSON.stringify(error));
            });
            
            this.database.exhibicionesAdicionales(this.punto.id_p)
            .then((data:any)=>{
                this.exhibicionAdicional = data;
            })
            .catch((error)=>{
                console.log("Error database.exhibicionesAdicionales: " + JSON.stringify(error));
            });
        }  

        this.preguntasExhibicion();
	}

	getImagen(imagen, ruta = ""){
        if(ruta == "exhibicion")
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_tipospunto + imagen);
        else
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta + imagen);
    }

    preguntasExhibicion(){
        if(this.internet === true)
            this.preguntasExhibicion_Internet();
        else 
            this.preguntasExhibicion_no_Internet();
    }

    preguntasExhibicion_no_Internet(){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.database.preguntasExhibicion_no_Internet()
        .then(data => {
            this.preguntas = data[0];
            loading.dismiss();
        })
        .catch(error => {
            console.log("error preguntasExhibicion_no_Internet: " + error);
            loading.dismiss();
        });
    }

    preguntasExhibicion_Internet(){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://palacalle.abbottdata.com/backend/v1/exhibiciones/', {preguntas: 1}, {})
        .then(data => {
            let res = JSON.parse(data.data);
            this.preguntas = {
                cinco: res[4]
            };
            loading.dismiss();
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("error preguntasExhibicion_Internet: " + error);
            loading.dismiss();
        });
    }

    cargarExhibicion(id_publicacion){
        this.http.get('http://palacalle.abbottdata.com/backend/v1/exhibiciones/', {cargarExhibicion: 1, publicacion: id_publicacion}, {})
        .then(data => {
            this.mostrarExhibicion = 1;
            this.datosExhibicion = JSON.parse(data.data);
            this.punto = {
                id_p : this.datosExhibicion.id_p,
                usuario : this.datosExhibicion.usuario,
                nombre: this.datosExhibicion.nombre,
                foto : this.datosExhibicion.foto,
                canal: this.datosExhibicion.canal,
                tipo: this.datosExhibicion.tipo,
                tipo_id: this.datosExhibicion.tipo_id
            };
            this.cinco = String(this.datosExhibicion.r5);
        })
        .catch(error => {
            this.mostrarExhibicion = 1;
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("error cargarExhibicion: " + JSON.stringify(error));
        });
    }

    // **************************************** 
    // ****** AGOTADOS
    // ****************************************

    colocarAgotados(data){
		this.productos_agotados = data;
		let tamaño = this.productos_agotados.length;
		for (var i = 0; i < tamaño; i++) {
			this.agotados.push("");
		}
        if(tamaño == 0)
            this.sinAgotados = 1;
	}

	agotados_info(tipo){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/productos/index2.php', {agotados: 1, tipo:  tipo, categoria : "p"}, {})
		.then(data => {
			this.colocarAgotados(JSON.parse(data.data));
		})
		.catch(error => {
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log("error agotados_info: " + JSON.stringify(error));
		});
	}

	cargarAgotados(publicacion, tipo){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/productos/index2.php', {mostrarAgotados: 1, publicacion:  publicacion, tipo : tipo, categoria : "p"}, {})
		.then(data => {
            this.colocarAgotados(JSON.parse(data.data));
			this.mostrarAgotados = 1;
		})
		.catch(error => {
			this.mostrarAgotados = 1;
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log("error cargarAgotados: " + JSON.stringify(error));
		});
	}

	// **************************************** 
    // ****** PRECIO
    // ****************************************

	colocarPrecio(data){
        this.productos_precio = data;
        let tamaño = this.productos_precio.length;
        for (var i = 0; i < tamaño; i++) {
            this.precio.push(1);
        }
        if(tamaño == 0)
            this.sinPrecio = 1;
    }

    precio_info(tipo){
        this.http.get('http://palacalle.abbottdata.com/backend/v1/productos/index2.php', {precio:1, tipo: tipo, categoria : "p"}, {})
        .then(data => {
            this.colocarPrecio(JSON.parse(data.data));
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("error precio_info: " + JSON.stringify(error));
        });
    }

    cargarPrecios(publicacion){
        this.http.get('http://palacalle.abbottdata.com/backend/v1/productos/index2.php', {mostrarPrecios: 1, publicacion: publicacion, categoria : "p"}, {})
        .then(data => {
            this.productos_precio = JSON.parse(data.data);
            this.mostrarPrecios = 1;
            let tamaño = this.productos_precio.length;
            if(tamaño == 0)
                this.sinPrecio = 1;
        })
        .catch(error => {
            this.mostrarPrecios = 1;
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("error cargarPrecios: " + JSON.stringify(error));
        });
    }

    toggleAgotado(i, producto){
        if(String(this.agotados[i]).length > 0){
            this.agotados[i] = "";
        }else{
            this.agotados[i] = producto;
        }
    }

    toggleCheckedAgotado(i){
        if(String(this.agotados[i]).length > 0){
            return true;
        }else{
            return false;
        }
    }

	// **************************************** 
    // ****** ENVIAR O GUARDAR
    // ****************************************

	continuar(mostrar = ""){
        if( (this.punto.id_p != "") ||  (this.punto.usuario != "") ||  (this.punto.canal != "") ){
            if( (this.btn_guardar != "Procesando...")  || (this.btn_continuar != "Procesando...")){

            	/* Exhibicion */
                var datos_exhibicion = this.navParams.get('exhibicion');
                datos_exhibicion.cinco = this.cinco;

                /* Agotados */
                var datos_agotados = this.navParams.get('agotados');
                if(typeof this.agotados != 'undefined')
                    datos_agotados = datos_agotados.concat(this.agotados);

                /* Precio */
                var datos_precio = this.navParams.get('precio');
                if(typeof this.productos_precio != 'undefined')
                    datos_precio = datos_precio.concat(this.productos_precio);

                if(mostrar == ""){
                    if( this.cinco != "" ){
                    	this.btn_guardar = "Procesando...";
                        this.navCtrl.push(ExhibicionPage, {
                                id_punto: this.punto.id_p, 
                                usuario: this.punto.usuario, 
                                nombre: this.punto.nombre, 
                                foto: this.punto.foto,
                                canal: this.punto.canal, 
                                tipo: this.punto.tipo,
                                tipo_id: this.punto.tipo_id,
                                exhibicion: datos_exhibicion,
                                agotados: datos_agotados,
                                precio: datos_precio,
                                exhibicionesAdicionales: this.exhibicionAdicional,
                                secuenciaExhibicionAdicional: 1,
                                exhibicionAdicionalDatos: [],
                                internet: this.internet
                            });
                    }else
                        alert("¡Completa todos los campos!");
                }else{
                	this.btn_continuar = "Procesando...";
                    let datos = {
                            cargarInfo: true,
                            publicacion: this.navParams.get('publicacion'),
                            id_punto: this.punto.id_p, 
                            usuario: this.punto.usuario, 
                            nombre: this.punto.nombre, 
                            foto: this.punto.foto,
                            canal: this.punto.canal, 
                            tipo: this.punto.tipo,
                            tipo_id: this.punto.tipo_id,
                            exhibicion: datos_exhibicion,
                            agotados: datos_agotados,
                            precio: datos_precio,
                            exhibicionesAdicionales: this.exhibicionAdicional,
                            secuenciaExhibicionAdicional: 1,
                            exhibicionAdicionalDatos: [],
                            internet: true
                        };
                    this.navCtrl.push(ExhibicionPage, datos); 
                }  // end else
            }
        }else
            alert("¡No se ha cargado bien la información del punto!");
    }


    // **************************************** 
    // ****** EXHIBICIONES ADICIONALES
    // ****************************************

    exhibicionesAdicionales(punto){
        this.http.get('http://palacalle.abbottdata.com/backend/v1/exhibiciones-adicionales/', {consultar:1, punto: punto}, {})
        .then(data => {
            this.exhibicionAdicional = JSON.parse(data.data);
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("error exhibicionesAdicionales: " + JSON.stringify(error));
        });
    }
    
}