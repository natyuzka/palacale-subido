import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { CamaraProvider } from '../../providers/camara/camara';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

@Component({
	selector: 'page-calificar-evento',
	templateUrl: 'calificar-evento.html',
})
export class CalificarEventoPage {

	usuario;
	ciudad = "Ciudad";
	evento = "";
	evento_lugar = "Lugar";
    evento_fecha = "Fecha";
    evento_hora = "";
	eventos = [
		{
			id_e:"",
			nombre: "Nombre del evento",
			encargado: "",
            fecha: "",
            hora: ""
		}
	];
	ciudades = [
		{
			nombre: "Ciudad"
		}
	];
	uno:string = "";
    dos:string = "";
    tres:string = "";
    cuatro:string = "";
    cinco:string = "";
    seis:string = "";
    siete:string = "";
    ocho:string = "";
    comentario = "";
    chosenPicture: any;
    imagenUpload = "";
    foto:boolean = false;
    foto_subida;
    mostrarEvento = 0;
    evento_mostrar;
    ruta;
    btn_guardar = "Guardar Valoración";
    internet;

	constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, public cameraProvider: CamaraProvider, public actionSheetCtrl: ActionSheetController, private storage: Storage, private sanitizer: DomSanitizer, public loadingCtrl: LoadingController, private database: DatabaseServiceProvider) {
        this.ruta = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
    }

	ionViewDidLoad() {
		this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
			}); 
	}

    ionViewDidEnter(){
        this.database.probarConexion()
        .then(() => {
            this.internet = true;
            if( this.navParams.get('publicacion') ){
                this.cargarEvento(this.navParams.get('publicacion'));
            }else
                this.mostrarCiudades_Internet();
        })
        .catch(() => {
            this.internet = false;
            this.mostrarCiudades_noInternet();
        });  
    }

    /*  --------------------------------  SIN INTERNET */
    mostrarCiudades_noInternet(){
        this.database.mostrarCiudadesEventos()
        .then((data:any)=>{
            this.ciudades = data;
        })
        .catch((error)=>{
            console.log("mostrarCiudadesEventos: " + JSON.stringify(error));
        });
    }

    mostrarEventos_noInternet(ciudad){
        this.database.mostrarEventos(ciudad)
        .then((data:any)=>{
            this.eventos = data;
        })
        .catch((error)=>{
            console.log("mostrarEventos: " + JSON.stringify(error));
        });
    }

    /*  --------------------------------  END SIN INTERNET */

	mostrarCiudades_Internet(){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/eventos/', {mostrarCiudades:  1}, {})
		.then(data => {
			this.ciudades = JSON.parse(data.data);
		})
		.catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log(error.status);
			console.log(error.error);
		});
	}

    mostrarEventos_Internet(ciudad){
        this.http.get('http://palacalle.abbottdata.com/backend/v1/eventos/', {mostrarEventos:  1, ciudad: ciudad}, {})
        .then(data => {
            this.eventos = JSON.parse(data.data);
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    }

	mostrarEventos(ciudad){
        this.evento_lugar = "Lugar";
        this.eventos = [
            {
                id_e:"",
                nombre: "Nombre del evento",
                encargado: "",
                fecha: "",
                hora: ""
            }
        ];
        this.evento_fecha = "Fecha";
        this.evento_hora = "";
		if(this.internet === true ){
            this.mostrarEventos_Internet(ciudad);
        }else
            this.mostrarEventos_noInternet(ciudad);
	}

	mostrarEncargado(id_evento){
        this.evento_lugar = "Lugar";
        this.evento_fecha = "Fecha";
        this.evento_hora = "";
		for (let evento of this.eventos) {
			if(evento.id_e == id_evento){
				this.evento_lugar = evento.encargado;
                this.evento_fecha = evento.fecha;
                this.evento_hora = evento.hora;
			}
		}
	}

    traerNombre(id_evento){
        for (let evento of this.eventos) {
            if(evento.id_e == id_evento){
                return evento.nombre;
            }
        }
    }

    getImagen(imagen){
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta + imagen);       
    }

	subirFoto() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Tomar Foto',
            buttons: [
            {
                text: 'Cámara',
                icon: 'camera',
                handler: () => {
                    this.chosenPicture = "assets/loader.gif";
                    this.cameraProvider.getPictureFromCamera().then(picture => {
                      if (picture) {
                        this.chosenPicture = picture[0];
                        this.imagenUpload = picture[1];
                        this.foto = true;
                      }
                    }, error => {
                      alert("Error, Vuelva a tomar la foto");
                      this.foto = false;
                      this.chosenPicture = "";
                    });
                }
            },{
                text: 'Cancelar',
                icon: 'close',
                role: 'destructive',
                handler: () => {
                }
            }
            ]
        });
        actionSheet.present();
    }

    guardarEvento_Internet(datos){
        this.http.post('http://palacalle.abbottdata.com/backend/v1/eventos/', {guardar:1, datos: JSON.stringify(datos)}, {})
        .then(data => {
            let respuesta = data.data;
            console.log("respuesta: " + respuesta);
            if(parseInt(respuesta) > 0){
                this.cameraProvider.cargarFotoServidor(this.imagenUpload, respuesta, "publicaciones")
                    .then(()=> {
                        alert("¡Guardado!");
                        let tamaño = this.navCtrl.length();
                        this.navCtrl.remove((tamaño-1), 1);
                    })
                    .catch(error => {
                        alert("¡Ha ocurrido un error al cargar la imágen!");
                        let tamaño = this.navCtrl.length();
                        this.navCtrl.remove((tamaño-1), 1);
                    });
            }else    
                 alert("¡Ha ocurrido un error al almacenar la información!");
        })
        .catch(error => {
            alert("¡Ha ocurrido un error al enviar la información para ser almacenada!");
            console.log(error.status);
            console.log(error.error);
        });
    }

    guardarEvento_noInternet(datos){
        this.database.guardarEvento_noInternet(datos)
        .then((data:any)=>{
            let respuesta = data;
            if(parseInt(respuesta) > 0){
                alert("¡Guardado!");
                let tamaño = this.navCtrl.length();
                this.navCtrl.remove((tamaño-1), 1);
            }else    
                 alert("¡Ha ocurrido un error al almacenar la información!");
        })
        .catch((error)=>{
            alert("¡Error al almacenar la información!");
        });
    }

    guardar(){
        if( this.usuario.id_u != "" ){
            if( (this.btn_guardar != "Procesando...") ){
            	if(   (this.evento != "") &&  (this.uno != "") && (this.dos != "") && (this.tres != "") && (this.cuatro != "")  && (this.cinco != "") && (this.seis != "") && (this.siete != "") && (this.ocho != "")  &&  (this.comentario.length > 0)  && (this.imagenUpload.length > 10) ){
                    this.btn_guardar = "Procesando...";
                    let evento_nombre = this.traerNombre(this.evento);
        			let datos = {
                            usuario: this.usuario.id_u, 
                            evento: this.evento, 
                            nombre: evento_nombre,
                            ciudad: this.ciudad,
                            uno: this.uno, 
                            dos: this.dos,
                            tres: this.tres,
                            cuatro: this.cuatro,
                            cinco: this.cinco,
                            seis: this.seis,
                            siete: this.siete,
                            ocho: this.ocho,
                            comentario: this.comentario,
                            foto: this.imagenUpload
                        };
                    if(this.internet === true){
                        this.guardarEvento_Internet(datos);
                    }else    
                        this.guardarEvento_noInternet(datos);  
                }else
                    alert("¡Completa todos los campos!");
            }
        }else
            alert("¡No se ha cargado bien la información!");
    }

    cargarEvento(id_publicacion){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://palacalle.abbottdata.com/backend/v1/eventos/', {mostrarEvento: 1, publicacion: id_publicacion}, {})
        .then(data => {
            this.evento_mostrar = JSON.parse(data.data);
            this.mostrarEvento = 1;
            this.ciudades = [{nombre: "Ciudad"}, { nombre: this.evento_mostrar.evento_ciudad}];
            this.ciudad = this.evento_mostrar.evento_ciudad;
            this.eventos = [ {id_e: "", nombre: "Nombre del evento", encargado: "", fecha: "", hora: ""}, {id_e : this.evento_mostrar.id_e, nombre : this.evento_mostrar.evento_nombre, encargado : this.evento_mostrar.encargado, fecha: this.evento_mostrar.fecha, hora: this.evento_mostrar.hora}];
            this.evento = this.evento_mostrar.id_e;
            this.evento_lugar = this.evento_mostrar.encargado;
            this.uno = String(this.evento_mostrar.uno);
            this.dos = String(this.evento_mostrar.dos);
            this.tres = String(this.evento_mostrar.tres);
            if(this.uno == "undefined")
                this.uno = '0';
            if(this.dos == "undefined")
                this.dos = '0';
            if(this.tres == "undefined")
                this.tres = '0';
            this.cuatro = this.evento_mostrar.cuatro;
            this.cinco = this.evento_mostrar.cinco;
            this.seis = this.evento_mostrar.seis;
            this.siete = this.evento_mostrar.siete;
            this.ocho = this.evento_mostrar.ocho;
            this.comentario = this.evento_mostrar.comentario;
            this.foto_subida = this.evento_mostrar.foto;
            this.foto_subida = this.navParams.get('publicacion') + ".png";
            this.mostrarEncargado(this.evento);
            loading.dismiss();
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
            loading.dismiss();
         });
    }

    regresar(){
        let tamaño = this.navCtrl.length();
        this.navCtrl.remove((tamaño-1), 1);
    }

}
