var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { CamaraProvider } from '../../providers/camara/camara';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var CalificarEventoPage = (function () {
    function CalificarEventoPage(navCtrl, navParams, http, cameraProvider, actionSheetCtrl, storage, sanitizer, loadingCtrl, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.cameraProvider = cameraProvider;
        this.actionSheetCtrl = actionSheetCtrl;
        this.storage = storage;
        this.sanitizer = sanitizer;
        this.loadingCtrl = loadingCtrl;
        this.database = database;
        this.ciudad = "Ciudad";
        this.evento = "";
        this.evento_encargado = "Representante encargado";
        this.eventos = [
            {
                id_e: "",
                nombre: "Nombre del evento",
                encargado: ""
            }
        ];
        this.ciudades = [
            {
                nombre: "Ciudad"
            }
        ];
        this.cuatro = "";
        this.cinco = "";
        this.seis = "";
        this.siete = "";
        this.ocho = "";
        this.comentario = "";
        this.foto = false;
        this.mostrarEvento = 0;
        this.btn_guardar = "Guardar Valoración";
        this.ruta = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
    }
    CalificarEventoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('usuario')
            .then(function (val) {
            _this.usuario = val;
        });
    };
    CalificarEventoPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.database.probarConexion()
            .then(function () {
            _this.internet = true;
            if (_this.navParams.get('publicacion')) {
                _this.cargarEvento(_this.navParams.get('publicacion'));
            }
            else
                _this.mostrarCiudades_Internet();
        })
            .catch(function () {
            _this.internet = false;
            _this.mostrarCiudades_noInternet();
        });
    };
    /*  --------------------------------  SIN INTERNET */
    CalificarEventoPage.prototype.mostrarCiudades_noInternet = function () {
        var _this = this;
        this.database.mostrarCiudadesEventos()
            .then(function (data) {
            _this.ciudades = data;
        })
            .catch(function (error) {
            console.log("mostrarCiudadesEventos: " + JSON.stringify(error));
        });
    };
    CalificarEventoPage.prototype.mostrarEventos_noInternet = function (ciudad) {
        var _this = this;
        this.database.mostrarEventos(ciudad)
            .then(function (data) {
            _this.eventos = data;
        })
            .catch(function (error) {
            console.log("mostrarEventos: " + JSON.stringify(error));
        });
    };
    /*  --------------------------------  END SIN INTERNET */
    CalificarEventoPage.prototype.mostrarCiudades_Internet = function () {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v0/eventos/', { mostrarCiudades: 1 }, {})
            .then(function (data) {
            _this.ciudades = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta, en el proceso de consultar la información de las especialidades!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    CalificarEventoPage.prototype.mostrarEventos_Internet = function (ciudad) {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v0/eventos/', { mostrarEventos: 1, ciudad: ciudad }, {})
            .then(function (data) {
            _this.eventos = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta, en el proceso de consultar la información de las especialidades!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    CalificarEventoPage.prototype.mostrarEventos = function (ciudad) {
        if (this.internet === true) {
            this.mostrarEventos_Internet(ciudad);
        }
        else
            this.mostrarEventos_noInternet(ciudad);
    };
    CalificarEventoPage.prototype.mostrarEncargado = function (id_evento) {
        for (var _i = 0, _a = this.eventos; _i < _a.length; _i++) {
            var evento = _a[_i];
            if (evento.id_e == id_evento) {
                this.evento_encargado = evento.encargado;
            }
        }
    };
    CalificarEventoPage.prototype.traerNombre = function (id_evento) {
        for (var _i = 0, _a = this.eventos; _i < _a.length; _i++) {
            var evento = _a[_i];
            if (evento.id_e == id_evento) {
                return evento.nombre;
            }
        }
    };
    CalificarEventoPage.prototype.getImagen = function (imagen) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta + imagen);
    };
    CalificarEventoPage.prototype.subirFoto = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Subir Imágen',
            buttons: [
                {
                    text: 'Camara',
                    icon: 'camera',
                    handler: function () {
                        _this.chosenPicture = "assets/loader.gif";
                        _this.cameraProvider.getPictureFromCamera().then(function (picture) {
                            if (picture) {
                                _this.chosenPicture = picture[0];
                                _this.imagenUpload = picture[1];
                                _this.foto = true;
                            }
                        }, function (error) {
                            alert("Error, Vuelva a subir la imágen");
                            _this.foto = false;
                            _this.chosenPicture = "";
                        });
                    }
                }, {
                    text: 'Cancelar',
                    icon: 'close',
                    role: 'destructive',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    CalificarEventoPage.prototype.guardarEvento_Internet = function (datos) {
        var _this = this;
        this.http.post('http://palacalle.abbottdata.com/backend/v0/eventos/', { guardar: 1, datos: JSON.stringify(datos) }, {})
            .then(function (data) {
            var respuesta = data.data;
            console.log("respuesta: " + respuesta);
            if (parseInt(respuesta) > 0) {
                _this.cameraProvider.cargarFotoServidor(_this.imagenUpload, respuesta, "publicaciones")
                    .then(function () {
                    alert("¡Guardado!");
                    var tamaño = _this.navCtrl.length();
                    _this.navCtrl.remove((tamaño - 1), 1);
                })
                    .catch(function (error) {
                    alert("¡Ha ocurrido un error al cargar la imágen!");
                    var tamaño = _this.navCtrl.length();
                    _this.navCtrl.remove((tamaño - 1), 1);
                });
            }
            else
                alert("¡Ha ocurrido un error al almacenar la información!");
        })
            .catch(function (error) {
            alert("¡Ha ocurrido un error al enviar la información para ser almacenada!");
            console.log(error.status);
            console.log(error.error);
        });
    };
    CalificarEventoPage.prototype.guardarEvento_noInternet = function (datos) {
        var _this = this;
        this.database.guardarEvento_noInternet(datos)
            .then(function (data) {
            var respuesta = data;
            if (parseInt(respuesta) > 0) {
                alert("¡Guardado!");
                var tamaño = _this.navCtrl.length();
                _this.navCtrl.remove((tamaño - 1), 1);
            }
            else
                alert("¡Ha ocurrido un error al almacenar la información!");
        })
            .catch(function (error) {
            alert("Error guardarEvento_noInternet: " + JSON.stringify(error));
        });
    };
    CalificarEventoPage.prototype.guardar = function () {
        if (this.usuario.id_u != "") {
            if ((this.btn_guardar != "Procesando...")) {
                if ((this.evento != "") && (this.uno != "") && (this.dos != "") && (this.tres != "") && (this.cuatro != "") && (this.cinco != "") && (this.seis != "") && (this.siete != "") && (this.ocho != "") && (this.comentario.length > 0) && (this.foto == true)) {
                    this.btn_guardar = "Procesando...";
                    var evento_nombre = this.traerNombre(this.evento);
                    var datos = {
                        usuario: this.usuario.id_u,
                        evento: this.evento,
                        nombre: evento_nombre,
                        ciudad: this.ciudad,
                        uno: this.uno,
                        dos: this.dos,
                        tres: this.tres,
                        cuatro: this.cuatro,
                        cinco: this.cinco,
                        seis: this.seis,
                        siete: this.siete,
                        ocho: this.ocho,
                        comentario: this.comentario
                    };
                    if (this.internet === true) {
                        this.guardarEvento_Internet(datos);
                    }
                    else
                        this.guardarEvento_noInternet(datos);
                }
                else
                    alert("¡Completa todos los campos!");
            }
        }
        else
            alert("¡No se ha cargado bien la información!");
    };
    CalificarEventoPage.prototype.cargarEvento = function (id_publicacion) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://palacalle.abbottdata.com/backend/v0/eventos/', { mostrarEvento: 1, publicacion: id_publicacion }, {})
            .then(function (data) {
            _this.evento_mostrar = JSON.parse(data.data);
            _this.mostrarEvento = 1;
            _this.ciudades = [{ nombre: "Ciudad" }, { nombre: _this.evento_mostrar.evento_ciudad }];
            _this.ciudad = _this.evento_mostrar.evento_ciudad;
            _this.eventos = [{ id_e: "", nombre: "Nombre del evento", encargado: "" }, { id_e: _this.evento_mostrar.id_e, nombre: _this.evento_mostrar.evento_nombre, encargado: "" }];
            _this.evento = _this.evento_mostrar.id_e;
            _this.evento_encargado = _this.evento_mostrar.encargado;
            _this.uno = String(_this.evento_mostrar.uno);
            _this.dos = String(_this.evento_mostrar.dos);
            _this.tres = String(_this.evento_mostrar.tres);
            _this.cuatro = _this.evento_mostrar.cuatro;
            _this.cinco = _this.evento_mostrar.cinco;
            _this.seis = _this.evento_mostrar.seis;
            _this.siete = _this.evento_mostrar.siete;
            _this.ocho = _this.evento_mostrar.ocho;
            _this.comentario = _this.evento_mostrar.comentario;
            _this.foto_subida = _this.evento_mostrar.foto;
            _this.foto_subida = _this.navParams.get('publicacion') + ".png";
            loading.dismiss();
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta, en el proceso de consultar la información del evento!");
            }
            console.log(error.status);
            console.log(error.error);
            loading.dismiss();
        });
    };
    CalificarEventoPage.prototype.regresar = function () {
        var tamaño = this.navCtrl.length();
        this.navCtrl.remove((tamaño - 1), 1);
    };
    return CalificarEventoPage;
}());
CalificarEventoPage = __decorate([
    Component({
        selector: 'page-calificar-evento',
        templateUrl: 'calificar-evento.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, HTTP, CamaraProvider, ActionSheetController, Storage, DomSanitizer, LoadingController, DatabaseServiceProvider])
], CalificarEventoPage);
export { CalificarEventoPage };
//# sourceMappingURL=calificar-evento.js.map