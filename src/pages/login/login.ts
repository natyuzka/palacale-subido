import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { NavController, AlertController, ModalController, ViewController, Platform } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

import { DomSanitizer } from '@angular/platform-browser';
import { PublicacionesPage } from '../publicaciones/publicaciones';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
	providers: [HTTP]
})

export class LoginPage {
	usuario = "";
	password = "";
	habeasdata = 0;
	btn_login = "Entrar";
	blob;

	constructor(public navCtrl: NavController, private http: HTTP, private database: DatabaseServiceProvider, public alertCtrl: AlertController, public modalCtrl: ModalController, private sanitizer: DomSanitizer, private storageLocal: Storage) {}

	ionViewWillEnter(){
		this.storageLocal.get('usuario')
		.then((val) => {
			if( val.hasOwnProperty("nombre") ){
				this.validarUsuario(val.nombre, val.tipo, 0);
			}
		})
		.catch((error)=> {
			console.log("error storage: " + JSON.stringify(error));
		});	
	}

	ionViewDidEnter() {
		this.btn_login = "Entrar";
	}

	internet_login(){
		this.http.post('http://palacalle.abbottdata.com/backend/v1/usuarios/', {login: 1, user: this.usuario, pass: this.password }, {})
	    .then((data) => {
	    	let datos = JSON.parse(data.data);
	    	// console.log("typo datos: " + typeof datos + ", datos:" + JSON.stringify(datos));
	    	if(datos.nombre.length > 0){
	    		this.storageLocal.set('usuario', datos);
	    	}
			this.validarUsuario(datos.nombre, datos.tipo, 1);
		})
	    .catch(error => {
	    	this.btn_login = "Entrar";
	    	if(error.status >= 500){
	        	alert("¡Error, el servidor no responde!");
        	}else
        	if(error.status >= 400){
        		alert("¡Error, no se encuentra el servidor!");
        	}else
        	if(error.hasOwnProperty("__zone_symbol__currentTask")){
        		alert("¡Usuario no registrado!");	
        	}else{
          		alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
          	}
	 	});
	}

	no_internet_login(){
		this.database.validarUsuario([this.usuario, this.password])
        .then((data) => {
            this.validarUsuario(data[0], data[1], 0);
        })
        .catch(error => console.log("error no_internet_login: " + error));
	}

	validarUsuario(nombre, tipo, internet){
		if( nombre.length > 0 ){
			if(tipo == "s")
				this.navCtrl.push(PublicacionesPage);
			else
				this.navCtrl.push(TabsPage);
		}else{
			this.btn_login = "Entrar";
			alert("¡Usuario no registrado!");	
		}						
	}

	login(){
		if( this.btn_login != "Procesando..."){
			if(  (this.usuario.length > 0) && (this.password.length > 0)  &&  (this.habeasdata > 0)  ){
				this.btn_login = "Procesando...";
				this.database.probarConexion()
			    .then(() => {
			    	this.internet_login();
			    })
			    .catch(() => {
			    	this.no_internet_login();
			    });
			}else{
				this.btn_login = "Entrar";
				alert("¡Debe completar todos los campos!");
			}
		}
	}

/*
	actualizarBaseDatos(){
        let confirm = this.alertCtrl.create({
          title: '¿Desea continuar?',
          message: 'Necesita estar conectado a internet, se recomienda usar una conexión Wifi',
          buttons: [
            {
              text: 'Cancelar',
              handler: () => {}
            },
            {
              text: 'Aceptar',
              handler: () => {
              	let modal = this.modalCtrl.create(ModalDescarga, {}, {enableBackdropDismiss: false});
                modal.present();
              }
            }
          ]
        });
        confirm.present();
    }

}

@Component({
      template: `<ion-content class="fondo center">
                  <div align="right">
                    <button (click)="dismiss()">
                        <ion-icon name="close-circle" item-right></ion-icon>
                    </button>
                </div>
                <br><br>
                <div align="center">
                    <p id="text-overlay" class="text-overlay"></p>
                    <progress max="100" value="0" id="progreso-actualizacion"></progress>
                </div>
            </ion-content>`,
      styles: [`
        .fondo{
            width: 100% !important;
            height: 100% !important;
            background: rgba(0,0,0,0.4) !important;
        }
        button ion-icon, button{
            color: white !important;
            background: transparent !important;
            font-size: 32px !important;
            width: 30px;
        }
      `]
})

export class ModalDescarga {

    constructor(private databaseService: DatabaseServiceProvider, private platform: Platform, public viewCtrl: ViewController) {}

    ionViewDidEnter() {
		this.databaseService.actualizarApp()
        .then(()=>{
        	this.viewCtrl.dismiss();
        })
        .catch((error)=>{
        	this.viewCtrl.dismiss();
        });
        this.platform.registerBackButtonAction(() => {
             this.databaseService.cancelarDescarga();
        });	
		}
	dismiss(){
		this.databaseService.cancelarDescarga();
		this.viewCtrl.dismiss();
	}
    */
} 