var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { NavController, AlertController, ModalController, ViewController, Platform } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { PublicacionesPage } from '../publicaciones/publicaciones';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var LoginPage = (function () {
    function LoginPage(navCtrl, http, database, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.database = database;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.usuario = "";
        this.password = "";
        this.habeasdata = 0;
        this.btn_login = "Entrar";
    }
    LoginPage.prototype.ionViewDidEnter = function () {
        this.btn_login = "Entrar";
    };
    LoginPage.prototype.internet_login = function () {
        var _this = this;
        this.http.post('http://palacalle.abbottdata.com/backend/v1/usuarios/', { login: 1, user: this.usuario, pass: this.password }, {})
            .then(function (data) {
            _this.validarUsuario(data.data, 1);
        })
            .catch(function (error) {
            _this.btn_login = "Entrar";
            console.log("error: " + JSON.stringify(error));
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
        });
    };
    LoginPage.prototype.no_internet_login = function () {
        var _this = this;
        var datos = [];
        datos.push(this.usuario, this.password);
        this.database.validarUsuario(datos)
            .then(function (data) {
            _this.validarUsuario(data, 0);
        })
            .catch(function (error) { return console.log("error no_internet_login: " + error); });
    };
    LoginPage.prototype.validarUsuario = function (data, internet) {
        if (internet == 1) {
            data = JSON.parse(data);
        }
        if (typeof data == 'object') {
            if (data.hasOwnProperty("nombre")) {
                if (internet == 0) {
                    alert("¡No tienes conexión a internet, accediendo a la base de datos local!");
                }
                if (data.tipo == "s") {
                    this.navCtrl.push(PublicacionesPage);
                }
                else {
                    this.navCtrl.push(TabsPage);
                }
            }
            else {
                this.btn_login = "Entrar";
                alert("¡Usuario no registrado!");
            }
        }
        else {
            this.btn_login = "Entrar";
            alert("¡Usuario no registrado!");
        }
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        if (this.btn_login != "Procesando...") {
            if ((this.usuario.length > 0) && (this.password.length > 0) && (this.habeasdata > 0)) {
                this.btn_login = "Procesando...";
                this.database.probarConexion()
                    .then(function () {
                    _this.internet_login();
                })
                    .catch(function () {
                    _this.no_internet_login();
                });
            }
            else {
                this.btn_login = "Entrar";
                alert("¡Debe completar todos los campos!");
            }
        }
    };
    LoginPage.prototype.actualizarBaseDatos = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '¿Desea continuar?',
            message: 'Necesita estar conectado a internet, se recomienda usar una conexión Wifi',
            buttons: [
                {
                    text: 'Cancelar',
                    handler: function () { }
                },
                {
                    text: 'Aceptar',
                    handler: function () {
                        var modal = _this.modalCtrl.create(ModalDescarga, {}, { enableBackdropDismiss: false });
                        modal.present();
                    }
                }
            ]
        });
        confirm.present();
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Component({
        selector: 'page-login',
        templateUrl: 'login.html',
        providers: [HTTP]
    }),
    __metadata("design:paramtypes", [NavController, HTTP, DatabaseServiceProvider, AlertController, ModalController])
], LoginPage);
export { LoginPage };
var ModalDescarga = (function () {
    function ModalDescarga(databaseService, platform, viewCtrl) {
        this.databaseService = databaseService;
        this.platform = platform;
        this.viewCtrl = viewCtrl;
    }
    ModalDescarga.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.databaseService.actualizarApp()
            .then(function () {
            _this.viewCtrl.dismiss();
        })
            .catch(function (error) {
            _this.viewCtrl.dismiss();
        });
        this.platform.registerBackButtonAction(function () {
            _this.databaseService.cancelarDescarga();
        });
    };
    ModalDescarga.prototype.dismiss = function () {
        this.databaseService.cancelarDescarga();
        this.viewCtrl.dismiss();
    };
    return ModalDescarga;
}());
ModalDescarga = __decorate([
    Component({
        template: "<ion-content class=\"fondo center\">\n                  <div align=\"right\">\n                    <button (click)=\"dismiss()\">\n                        <ion-icon name=\"close-circle\" item-right></ion-icon>\n                    </button>\n                </div>\n                <br><br>\n                <div align=\"center\">\n                    <p id=\"text-overlay\" class=\"text-overlay\"></p>\n                    <progress max=\"100\" value=\"0\" id=\"progreso-actualizacion\"></progress>\n                </div>\n            </ion-content>",
        styles: ["\n        .fondo{\n            width: 100% !important;\n            height: 100% !important;\n            background: rgba(0,0,0,0.4) !important;\n        }\n        button ion-icon, button{\n            color: white !important;\n            background: transparent !important;\n            font-size: 32px !important;\n            width: 30px;\n        }\n      "]
    }),
    __metadata("design:paramtypes", [DatabaseServiceProvider, Platform, ViewController])
], ModalDescarga);
export { ModalDescarga };
//# sourceMappingURL=login.js.map