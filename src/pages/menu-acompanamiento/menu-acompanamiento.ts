import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { VisitaMedicaPage } from '../visita-medica/visita-medica';
import { CalificarEventoPage } from '../calificar-evento/calificar-evento';

@Component({
  selector: 'page-menu-acompanamiento',
  templateUrl: 'menu-acompanamiento.html',
})
export class MenuAcompanamientoPage {

	page;

	constructor(public navCtrl: NavController, public navParams: NavParams){}

	irA(ventana){
		if(ventana == "v"){
			this.page = VisitaMedicaPage;
		}else{
			this.page = CalificarEventoPage;
		}
		this.navCtrl.push(this.page);
	}

}
