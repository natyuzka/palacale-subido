var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ActionSheetController, Content, LoadingController, ViewController, ModalController, Platform } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { CamaraProvider } from '../../providers/camara/camara';
import { MenuEvaluarPunto } from '../menu-evaluar-punto/menu-evaluar-punto';
import { Storage } from '@ionic/storage';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var ExhibicionAdicionalPage = (function () {
    function ExhibicionAdicionalPage(navCtrl, navParams, http, actionSheetCtrl, cameraProvider, sanitizer, loadingCtrl, modalCtrl, storage, database, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.actionSheetCtrl = actionSheetCtrl;
        this.cameraProvider = cameraProvider;
        this.sanitizer = sanitizer;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.database = database;
        this.punto_info = {
            nombre: "",
            canal: "",
            tipo: ""
        };
        this.exhibicionAdicional = {
            id_e: "",
            nombre: "",
            foto: ""
        };
        this.foto = false;
        this.mostrarCampos = 0;
        this.mostrarExhibicionAdicional = 0;
        this.btn_guardar = "Guardar Valoración";
        this.btn_continuar = "Continuar";
        this.esSupervisor = false;
        if (platform.is('ios')) {
            this.mobile = "library-nosync";
        }
        else if (platform.is('android')) {
            this.mobile = "files";
        }
        this.ruta_exhibicion = "http://palacalle.abbottdata.com/imagenes/exhibiciones-adicionales-usuarios/";
        this.storage.get('usuario')
            .then(function (val) {
            _this.usuario = val;
            if (_this.usuario.tipo == "s") {
                _this.esSupervisor = true;
            }
        });
    }
    ExhibicionAdicionalPage.prototype.ionViewDidLoad = function () {
        if (this.navParams.get('internet') == true) {
            this.ruta = "http://palacalle.abbottdata.com/imagenes/exhibiciones-adicionales/";
        }
        else
            this.ruta = "cdvfile://localhost/" + this.mobile + "/exhibiciones-adicionales/";
        this.punto = {
            id_punto: this.navParams.get('id_punto'),
            usuario: this.navParams.get('usuario'),
            nombre: this.navParams.get('nombre'),
            canal: this.navParams.get('canal'),
            tipo: this.navParams.get('tipo'),
            tipo_id: this.navParams.get('tipo_id'),
            exhibicion: this.navParams.get('exhibicion'),
            agotados: this.navParams.get('agotados'),
            precio: this.navParams.get('precio'),
            exhibicionesAdicionales: this.navParams.get('exhibicionesAdicionales'),
            secuenciaExhibicionAdicional: this.navParams.get('secuenciaExhibicionAdicional'),
            exhibicionAdicionalDatos: this.navParams.get('exhibicionAdicionalDatos')
        };
        this.punto_info.nombre = this.punto.nombre;
        this.punto_info.canal = this.punto.canal;
        this.punto_info.tipo = this.punto.tipo;
        if (this.navParams.get('internet') == true) {
            this.info(this.punto.id_punto);
            if (this.navParams.get('publicacion')) {
                var exhibicion = this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)];
                this.cargarExhibicionAdicional(this.navParams.get('publicacion'), exhibicion);
            }
        }
        else
            this.info_no_internet(this.punto.id_punto);
    };
    ExhibicionAdicionalPage.prototype.getImagen = function (imagen, ruta) {
        if (ruta == "exhibicion") {
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_exhibicion + imagen);
        }
        else
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta + imagen);
    };
    ExhibicionAdicionalPage.prototype.info_no_internet = function (punto) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.database.exhibicionAdicionalInfo(punto, this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)])
            .then(function (data) {
            _this.exhibicionAdicional = data;
            _this.content.scrollToTop();
            loading.dismiss();
        })
            .catch(function (error) {
            console.log("Error info_no_internet exhibicionAdicionalInfo: " + JSON.stringify(error));
            _this.content.scrollToTop();
            loading.dismiss();
        });
    };
    ExhibicionAdicionalPage.prototype.info = function (punto) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://palacalle.abbottdata.com/backend/v0/exhibiciones-adicionales/', { info: 1, punto: punto, exhibicionAdicional: this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)] }, {})
            .then(function (data) {
            _this.exhibicionAdicional = JSON.parse(data.data);
            _this.content.scrollToTop();
            loading.dismiss();
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
            _this.content.scrollToTop();
            loading.dismiss();
        });
    };
    ExhibicionAdicionalPage.prototype.subirFoto = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Subir Imágen',
            buttons: [
                {
                    text: 'Camara',
                    icon: 'camera',
                    handler: function () {
                        _this.chosenPicture = "assets/loader.gif";
                        _this.cameraProvider.getPictureFromCamera().then(function (picture) {
                            if (picture) {
                                _this.chosenPicture = picture[0];
                                _this.imagenUpload = picture[1];
                                _this.foto = true;
                            }
                        }, function (error) {
                            alert("Error, Vuelva a subir la imágen");
                            _this.foto = false;
                            _this.chosenPicture = "";
                        });
                    }
                }, {
                    text: 'Cancelar',
                    icon: 'close',
                    role: 'destructive',
                    handler: function () {
                        _this.foto = false;
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ExhibicionAdicionalPage.prototype.regresarVista_noInternet = function () {
        var _this = this;
        var foto;
        var tamaño = this.navCtrl.length();
        var args;
        this.database.fotoTipoPunto(this.punto.tipo_id)
            .then(function (data) {
            foto = data;
            args = {
                id_punto: _this.punto.id_punto,
                estado: 2,
                id_u: _this.punto.usuario,
                tipo: _this.punto.canal + "-" + _this.punto.tipo,
                nombre: _this.punto.nombre,
                foto: _this.punto.tipo_id + "." + foto,
                tipo_id: _this.punto.tipo_id,
                internet: false,
                califico: "p"
            };
            _this.navCtrl.remove((tamaño - 5), 5)
                .then(function () {
                _this.navCtrl.push(MenuEvaluarPunto, args);
            });
        })
            .catch(function (error) {
            foto = "jpg";
            args = {
                id_punto: _this.punto.id_punto,
                estado: 2,
                id_u: _this.punto.usuario,
                tipo: _this.punto.canal + "-" + _this.punto.tipo,
                nombre: _this.punto.nombre,
                foto: _this.punto.tipo_id + "." + foto,
                tipo_id: _this.punto.tipo_id,
                internet: false,
                califico: "p"
            };
            _this.navCtrl.remove((tamaño - 5), 5)
                .then(function () {
                _this.navCtrl.push(MenuEvaluarPunto, args);
            });
        });
    };
    ExhibicionAdicionalPage.prototype.guardarDatos_internet = function (datos) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.post('http://palacalle.abbottdata.com/backend/v0/publicaciones/', { guardar: 1, datos: JSON.stringify(datos) }, {})
            .then(function (data) {
            var respuesta = JSON.parse(data.data);
            console.log("respuesta: " + respuesta);
            if (respuesta.length > 0) {
                /* Subiendo fotos exhibicion */
                var args_1 = {
                    id_punto: _this.punto.id_punto,
                    estado: 2,
                    id_u: _this.punto.usuario
                };
                var tamaño_1 = _this.navCtrl.length();
                _this.cameraProvider.cargarFotoServidor(datos.exhibicion.foto, respuesta[0], "publicaciones")
                    .then(function () {
                })
                    .catch(function (error) {
                    alert("¡Ha ocurrido un error al cargar la imágen!");
                    _this.navCtrl.remove((tamaño_1 - 5), 5, args_1)
                        .then(function () {
                        _this.navCtrl.push(MenuEvaluarPunto, args_1);
                    });
                });
                /*Eliminado el id de la publicacion*/
                respuesta.splice(0, 1);
                /* Subiendo fotos exhibiciones adicionales */
                for (var i in respuesta) {
                    _this.cameraProvider.cargarFotoServidor(datos.exhibicionAdicionalDatos[i].foto, respuesta[i], "exhibiciones-adicionales-usuarios")
                        .catch(function (error) {
                        alert("¡Ha ocurrido un error al cargar las imágenes!");
                    });
                }
                _this.navCtrl.remove((tamaño_1 - 5), 5)
                    .then(function () {
                    _this.navCtrl.push(MenuEvaluarPunto, args_1);
                });
            }
            else
                alert("¡Ha ocurrido un error al almacenar la información!");
            loading.dismiss();
        })
            .catch(function (error) {
            loading.dismiss();
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    ExhibicionAdicionalPage.prototype.guardarDatos_no_internet = function (datos) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.database.guardarPublicacion_noInternet(datos)
            .then(function (data) {
            loading.dismiss();
            var respuesta = data;
            console.log("respuesta: " + respuesta);
            if (respuesta == 0) {
                alert("¡Ha ocurrido un error al almacenar la información!");
            }
            else
                _this.regresarVista_noInternet();
        })
            .catch(function (error) {
            loading.dismiss();
            console.log("Error info_no_internet exhibicionAdicionalInfo: " + JSON.stringify(error));
        });
    };
    ExhibicionAdicionalPage.prototype.guardar = function (mostrar) {
        if (mostrar === void 0) { mostrar = ""; }
        if ((this.btn_guardar != "Procesando...") || (this.btn_continuar != "Procesando...")) {
            if (mostrar == "") {
                var datos_formulario = {
                    uno: this.uno_,
                    dos: this.dos_,
                    tres: this.tres_,
                    cuatro: this.cuatro_,
                    foto: this.imagenUpload
                };
                if ((this.uno_ >= 0) && (this.dos_ >= 0) && (this.tres_ >= 0) && (this.cuatro_ >= 0) && (this.foto == true)) {
                    var secuencia = parseInt(this.punto.secuenciaExhibicionAdicional) + 1;
                    this.punto.secuenciaExhibicionAdicional = String(secuencia);
                    if (secuencia <= this.punto.exhibicionesAdicionales.length) {
                        this.punto.exhibicionAdicionalDatos.push(datos_formulario);
                        this.uno_ = null;
                        this.dos_ = null;
                        this.tres_ = null;
                        this.cuatro_ = null;
                        this.foto = false;
                        this.mostrarCampos = 0;
                        this.chosenPicture = "";
                        if (this.navParams.get('internet') == true) {
                            this.info(this.punto.id_punto);
                        }
                        else
                            this.info_no_internet(this.punto.id_punto);
                    }
                    else {
                        // Si ya no hay mas exhibiciones adicionales, guardamos...
                        this.btn_guardar = "Procesando...";
                        this.punto.exhibicionAdicionalDatos.push(datos_formulario);
                        var datos = {
                            id_punto: this.punto.id_punto,
                            usuario: this.punto.usuario,
                            nombre: this.punto.nombre,
                            canal: this.punto.canal,
                            tipo: this.punto.tipo,
                            tipo_id: this.punto.tipo_id,
                            exhibicion: this.punto.exhibicion,
                            agotados: this.punto.agotados,
                            precio: this.punto.precio,
                            exhibicionesAdicionales: this.punto.exhibicionesAdicionales,
                            secuenciaExhibicionAdicional: (secuencia),
                            exhibicionAdicionalDatos: this.punto.exhibicionAdicionalDatos
                        };
                        if (this.navParams.get('internet') == true) {
                            this.guardarDatos_internet(datos);
                        }
                        else {
                            this.guardarDatos_no_internet(datos);
                        }
                    }
                }
                else
                    alert("¡Completa todos los campos!");
            }
            else {
                /* Si se debe mostrar la info */
                var secuencia = parseInt(this.punto.secuenciaExhibicionAdicional) + 1;
                this.punto.secuenciaExhibicionAdicional = String(secuencia);
                if (secuencia <= this.punto.exhibicionesAdicionales.length) {
                    this.info(this.punto.id_punto);
                    var exhibicion = this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)];
                    this.cargarExhibicionAdicional(this.navParams.get('publicacion'), exhibicion);
                    this.content.scrollToTop();
                    if (secuencia == this.punto.exhibicionesAdicionales.length) {
                        this.btn_continuar = "Regresar a Publicaciones";
                    }
                }
                else {
                    this.btn_continuar = "Procesando...";
                    if (this.esSupervisor == true) {
                        this.presentCerrarCasoModal();
                    }
                    else {
                        var tamaño = this.navCtrl.length();
                        this.navCtrl.remove((tamaño - 4), 4);
                    }
                }
            }
        }
    };
    ExhibicionAdicionalPage.prototype.cargarExhibicionAdicional = function (publicacion, exhibicion) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://palacalle.abbottdata.com/backend/v0/exhibiciones-adicionales/', { cargarExhibicionAdicional: 1, publicacion: publicacion, exhibicion: exhibicion }, {})
            .then(function (data) {
            var datosExhibicionAdicional = JSON.parse(data.data);
            _this.mostrarExhibicionAdicional = 1;
            _this.uno = String(datosExhibicionAdicional.uno);
            if (_this.uno == '1') {
                _this.mostrarCampos = 1;
            }
            _this.dos = String(datosExhibicionAdicional.dos);
            _this.tres = String(datosExhibicionAdicional.tres);
            _this.cuatro = String(datosExhibicionAdicional.cuatro);
            _this.foto_exhibicion = datosExhibicionAdicional.foto_exhibicion;
            loading.dismiss();
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta, en el proceso de consultar la información de la exhibición adicional!");
            }
            console.log(error.status);
            console.log(error.error);
            loading.dismiss();
        });
    };
    ExhibicionAdicionalPage.prototype.masCampos = function (continuar) {
        if (continuar == 1) {
            this.mostrarCampos = 1;
        }
        else {
            this.mostrarCampos = 0;
            this.uno = null;
            this.dos = null;
            this.tres = null;
            this.cuatro = null;
            this.uno_ = null;
            this.dos_ = null;
            this.tres_ = null;
            this.cuatro_ = null;
        }
    };
    ExhibicionAdicionalPage.prototype.presentCerrarCasoModal = function () {
        var _this = this;
        var cerrarCaso = this.modalCtrl.create(CerrarCasoModalExhibicionAdicional, { publicacion: this.navParams.get('publicacion') });
        cerrarCaso.onDidDismiss(function (data) {
            var tamaño = _this.navCtrl.length();
            _this.navCtrl.remove((tamaño - 4), 4);
        });
        cerrarCaso.present();
    };
    return ExhibicionAdicionalPage;
}());
__decorate([
    ViewChild(Content),
    __metadata("design:type", Content)
], ExhibicionAdicionalPage.prototype, "content", void 0);
ExhibicionAdicionalPage = __decorate([
    Component({
        selector: 'page-exhibicion-adicional',
        templateUrl: 'exhibicion-adicional.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, HTTP, ActionSheetController, CamaraProvider, DomSanitizer, LoadingController, ModalController, Storage, DatabaseServiceProvider, Platform])
], ExhibicionAdicionalPage);
export { ExhibicionAdicionalPage };
var CerrarCasoModalExhibicionAdicional = (function () {
    function CerrarCasoModalExhibicionAdicional(navCtrl, http, params, viewCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.viewCtrl = viewCtrl;
        this.publicacion = 0;
        this.publicacion = params.get('publicacion');
    }
    CerrarCasoModalExhibicionAdicional.prototype.cerrarCaso = function () {
        var _this = this;
        this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { cerrarCaso: this.publicacion }, {})
            .then(function (data) {
            if (data.data == '1') {
                _this.viewCtrl.dismiss();
            }
            else
                alert("¡Error al cerrar el caso!");
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    return CerrarCasoModalExhibicionAdicional;
}());
CerrarCasoModalExhibicionAdicional = __decorate([
    Component({
        template: "<ion-content class=\"fondo center\">\n                <div class=\"box-center\">\n                    <div>\n                        <h3 class=\"center\">Punto Evaluado</h3>\n                        <div>He entendido toda la evaluaci\u00F3n.</div>\n                        <div>Es hora de gestionar y optimizar mi punto.</div>\n                        <button class=\"btn btn-rounded btn-azul marginTop1\" (click)=\"cerrarCaso()\">Cerrar Caso</button>\n                    </div>\n                </div>\n            </ion-content>",
        styles: ["\n        .fondo{\n            width: 100% !important;\n            height: 100% !important;\n            background: #fff !important;\n        }\n        button ion-icon, button{\n            color: white !important;\n        }\n        .box-center {\n            height: 100%;\n            display: flex;\n            align-items: center;\n            justify-content: center;\n        }\n      "]
    }),
    __metadata("design:paramtypes", [NavController, HTTP, NavParams, ViewController])
], CerrarCasoModalExhibicionAdicional);
export { CerrarCasoModalExhibicionAdicional };
//# sourceMappingURL=exhibicion-adicional.js.map