import { Component, ViewChild  } from '@angular/core';
import { NavController, NavParams, ActionSheetController, Content, LoadingController, ViewController, ModalController, Platform } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { CamaraProvider } from '../../providers/camara/camara';
import { MenuEvaluarPunto } from '../menu-evaluar-punto/menu-evaluar-punto';
import { Storage } from '@ionic/storage';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

@Component({
	selector: 'page-exhibicion-adicional',
	templateUrl: 'exhibicion-adicional.html',
})

export class ExhibicionAdicionalPage {
    @ViewChild(Content) content: Content;

    punto;
    punto_info = {
        nombre: "",  
        canal: "", 
        tipo: ""
    };
    uno:string = "";
    dos:string = "";
    tres:string = "";
    cuatro:string;
    uno_:number;
    dos_:number;
    tres_:number;
    cuatro_:number;
	exhibicionAdicional= {
        id_e: "",
        nombre: "",
        foto: ""
    };
	chosenPicture: any;
    imagenUpload = "";
    foto:boolean = false;
    ruta;
    ruta_exhibicion;
    mostrarCampos = 0;
    mostrarExhibicionAdicional = 0;
    foto_exhibicion;
    btn_guardar = "Guardar Valoración";
    btn_continuar = "Continuar";
    esSupervisor = false;
    usuario;
    mobile;

	constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, public actionSheetCtrl: ActionSheetController, public cameraProvider: CamaraProvider, private sanitizer: DomSanitizer, public loadingCtrl: LoadingController, public modalCtrl: ModalController, private storage: Storage, private database: DatabaseServiceProvider, platform: Platform) {
        if (platform.is('ios')) {
            this.mobile = "library-nosync";
        }else
        if (platform.is('android')) {
            this.mobile = "files";
        }
        this.ruta_exhibicion = "http://palacalle.abbottdata.com/imagenes/exhibiciones-adicionales-usuarios/";
        this.storage.get('usuario')
        .then((val) => {
            this.usuario = val;
            if( this.usuario.tipo == "s"){
                this.esSupervisor = true;
            }
        });
    }

	ionViewDidLoad() {
        if(this.navParams.get('internet') == true){
            this.ruta = "http://palacalle.abbottdata.com/imagenes/exhibiciones-adicionales/";
        }else
            this.ruta = "cdvfile://localhost/" + this.mobile + "/exhibiciones-adicionales/";

		this.punto = {
		          id_punto : this.navParams.get('id_punto'),
		          usuario : this.navParams.get('usuario'),
		          nombre: this.navParams.get('nombre'), 
		          canal: this.navParams.get('canal'), 
		          tipo: this.navParams.get('tipo'),
                  tipo_id: this.navParams.get('tipo_id'),
		          exhibicion: this.navParams.get('exhibicion'),
		          agotados: this.navParams.get('agotados'),
		          precio: this.navParams.get('precio'),
		          exhibicionesAdicionales: this.navParams.get('exhibicionesAdicionales'),
                  secuenciaExhibicionAdicional : this.navParams.get('secuenciaExhibicionAdicional'),
                  exhibicionAdicionalDatos : this.navParams.get('exhibicionAdicionalDatos')
		      };

        this.punto_info.nombre = this.punto.nombre;
        this.punto_info.canal = this.punto.canal;
        this.punto_info.tipo = this.punto.tipo;

        if(this.navParams.get('internet') == true){
            this.info(this.punto.id_punto);
            if( this.navParams.get('cargarInfo') ){
                let exhibicion = this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)];
                this.cargarExhibicionAdicional(this.navParams.get('publicacion'), exhibicion);
            }
        }else
            this.info_no_internet(this.punto.id_punto);
	}

	getImagen(imagen, ruta){
        if(ruta == "exhibicion"){
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_exhibicion + imagen);
        }else
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta + imagen);
    }

    info_no_internet(punto){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.database.exhibicionAdicionalInfo(punto, this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)])
        .then((data:any)=>{
            this.exhibicionAdicional = data;
            this.content.scrollToTop();
            loading.dismiss();
        })
        .catch((error)=>{
            console.log("Error info_no_internet exhibicionAdicionalInfo: " + JSON.stringify(error));
            this.content.scrollToTop();
            loading.dismiss();
        });
    }
    
	info(punto){
        let loading = this.loadingCtrl.create();
        loading.present();
		this.http.get('http://palacalle.abbottdata.com/backend/v1/exhibiciones-adicionales/', {info:1, punto: JSON.stringify(punto), exhibicionAdicional: this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)]}, {})
        .then(data => {
            this.exhibicionAdicional = JSON.parse(data.data);
            this.content.scrollToTop();
            loading.dismiss();
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("Error info: " + JSON.stringify(error));
            this.content.scrollToTop();
            loading.dismiss();
        });
	}

	subirFoto() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Tomar Foto',
            buttons: [
            {
                text: 'Cámara',
                icon: 'camera',
                handler: () => {
                    this.chosenPicture = "assets/loader.gif";
                    this.cameraProvider.getPictureFromCamera().then(picture => {
                      if (picture) {
                        this.chosenPicture = picture[0];
                        this.imagenUpload = picture[1];
                        this.foto = true;
                      }
                    }, error => {
                      alert("Error, Vuelva a tomar la foto");
                      this.foto = false;
                      this.chosenPicture = "";
                    });
                }
            },{
                text: 'Cancelar',
                icon: 'close',
                role: 'destructive',
                handler: () => {
                    this.foto = false;
                }
            }
            ]
        });
        actionSheet.present();
    }

    regresarVista_noInternet(){
        let foto;
        let tamaño = this.navCtrl.length();
        let args;
        this.database.fotoTipoPunto(this.punto.tipo_id)
        .then((data)=>{
            foto = data;
            args = {
                    id_punto: this.punto.id_punto,
                    estado: 2,
                    id_u: this.punto.usuario,
                    tipo: this.punto.canal + "-" + this.punto.tipo,
                    nombre: this.punto.nombre,
                    foto: this.punto.tipo_id + "." + foto,
                    tipo_id: this.punto.tipo_id,
                    internet: false,
                    califico: "p"
                };
            this.navCtrl.remove((tamaño-5), 5)
            .then(()=>{
                this.navCtrl.push(MenuEvaluarPunto, args);
            });
        })
        .catch((error)=>{
            foto = "jpg";
            args = {
                    id_punto: this.punto.id_punto,
                    estado: 2,
                    id_u: this.punto.usuario,
                    tipo: this.punto.canal + "-" + this.punto.tipo,
                    nombre: this.punto.nombre,
                    foto: this.punto.tipo_id + "." + foto,
                    tipo_id: this.punto.tipo_id,
                    internet: false,
                    califico: "p"
                };
            this.navCtrl.remove((tamaño-5), 5)
            .then(()=>{
                this.navCtrl.push(MenuEvaluarPunto, args);
            });
        });
    }

    guardarDatos_internet(datos){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {guardar:1, datos: JSON.stringify(datos)}, {})
        .then(data => {
            let respuesta = JSON.parse(data.data);
            if( respuesta.length > 0){
                let publicacion = {
                    id_p: respuesta[0],
                    punto: datos.id_punto
                };
                this.database.enviarAvisoSupervisor(publicacion);
                /* Subiendo fotos exhibicion */
                let args = {
                        id_punto: this.punto.id_punto,
                        estado: 2,
                        id_u: this.punto.usuario,
                        internet: true
                    };
                let tamaño = this.navCtrl.length();
                this.cameraProvider.cargarFotoServidor(datos.exhibicion.foto, respuesta[0], "publicaciones")
                    .then(()=> {
                    })
                    .catch(error => {
                        alert("¡Ha ocurrido un error al cargar la imágen!");
                        this.navCtrl.remove((tamaño-5), 5)
                        .then(()=>{
                            this.navCtrl.push(MenuEvaluarPunto, args);
                        });
                    });
                /*Eliminado el id de la publicacion*/
                respuesta.splice(0, 1);
                /* Subiendo fotos exhibiciones adicionales */
                for (let i in respuesta) {
                    if(datos.exhibicionAdicionalDatos[i].foto.length > 0){
                        this.cameraProvider.cargarFotoServidor(datos.exhibicionAdicionalDatos[i].foto, respuesta[i], "exhibiciones-adicionales-usuarios")
                        .catch(error => {
                            alert("¡Ha ocurrido un error al cargar las imágenes!");
                        });
                    }  
                }
                this.navCtrl.remove((tamaño-5), 5)
                .then(()=>{
                    this.navCtrl.push(MenuEvaluarPunto, args);
                });
            }else   
                 alert("¡Ha ocurrido un error al almacenar la información!");
            loading.dismiss();
        })
        .catch(error => {
            loading.dismiss();
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("Error guardarDatos_internet: " + JSON.stringify(error));
        });
    }

    guardarDatos_no_internet(datos){
        let loading = this.loadingCtrl.create();
        loading.present();
        this.database.guardarPublicacion_noInternet(datos)
        .then((data:any)=>{
            loading.dismiss();
            let respuesta = data;
            if( respuesta == 0){
                alert("¡Ha ocurrido un error al almacenar la información!");
            }else
                this.regresarVista_noInternet();
        })
        .catch((error)=>{
            console.log("Error guardarDatos_no_internet: " + JSON.stringify(error));
            loading.dismiss();
        });
    }

    guardar(mostrar = ""){
        if( (this.btn_guardar != "Procesando...")  || (this.btn_continuar != "Procesando...")){
            if(mostrar == ""){
                    let datos_formulario = {
                            uno: this.uno_,
                            dos: this.dos_,
                            tres: this.tres_,
                            cuatro: this.cuatro_,
                            foto: this.imagenUpload
                        }

                    if( (this.uno_ >= 0) && (this.dos_ >= 0) && (this.tres_ >= 0) && (this.cuatro_ >= 0) ){

                        let continuar = 1;
                        if(this.uno_ == 1){
                            if( this.imagenUpload.length == 0 )
                                continuar = 0;
                            if( (this.uno_ == null) || (this.dos_ == null) || (this.tres_ == null) || (this.cuatro_ == null) )
                                continuar = 2;
                        }

                        if(continuar == 1){
                            let secuencia = parseInt(this.punto.secuenciaExhibicionAdicional) + 1;
                            this.punto.secuenciaExhibicionAdicional = String(secuencia);
                            if(  secuencia  <= this.punto.exhibicionesAdicionales.length ){
                                this.punto.exhibicionAdicionalDatos.push(datos_formulario);
                                this.uno_ = null;
                                this.dos_ = null;
                                this.tres_ = null;
                                this.cuatro_ = null;
                                this.foto = false;
                                this.mostrarCampos = 0;
                                this.chosenPicture = "";
                                this.imagenUpload = "";
                                if(this.navParams.get('internet') == true){
                                    this.info(this.punto.id_punto);
                                }else{
                                    this.info_no_internet(this.punto.id_punto);
                                }
                            }else{
                                // Si ya no hay mas exhibiciones adicionales, guardamos...
                                this.btn_guardar = "Procesando...";
                                this.punto.exhibicionAdicionalDatos.push(datos_formulario);
                                let datos = {
                                                id_punto: this.punto.id_punto, 
                                                usuario: this.punto.usuario, 
                                                nombre: this.punto.nombre, 
                                                canal: this.punto.canal, 
                                                tipo: this.punto.tipo,
                                                tipo_id: this.punto.tipo_id,
                                                exhibicion: this.punto.exhibicion,
                                                agotados: this.punto.agotados,
                                                precio: this.punto.precio,
                                                exhibicionesAdicionales: this.punto.exhibicionesAdicionales,
                                                secuenciaExhibicionAdicional: (secuencia),
                                                exhibicionAdicionalDatos: this.punto.exhibicionAdicionalDatos
                                            };
                                
                                if(this.navParams.get('internet') == true){
                                    this.guardarDatos_internet(datos);
                                }else{
                                    this.guardarDatos_no_internet(datos);
                                }
                            } // end else
                        }else
                        if(continuar == 2){
                            alert("¡Completa todos los campos!");
                        }else
                            alert("¡Toma una foto!");
                    }else
                        alert("¡Completa todos los campos!");

            }else{ 
                /* Si se debe mostrar la info */
                    let secuencia = parseInt(this.punto.secuenciaExhibicionAdicional) + 1;
                    this.punto.secuenciaExhibicionAdicional = String(secuencia);
                    if(  secuencia  <= this.punto.exhibicionesAdicionales.length ){
                        this.info(this.punto.id_punto);
                        let exhibicion = this.punto.exhibicionesAdicionales[(parseInt(this.punto.secuenciaExhibicionAdicional) - 1)];
                        this.cargarExhibicionAdicional(this.navParams.get('publicacion'), exhibicion);
                        this.content.scrollToTop();
                        if(  secuencia  == this.punto.exhibicionesAdicionales.length ){
                            this.btn_continuar = "Continuar";
                        }
                    }else{
                        this.btn_continuar = "Procesando...";
                        if(this.esSupervisor == true){
                            this.presentCerrarCasoModal();
                        }else{
                            let tamaño = this.navCtrl.length();
                            this.navCtrl.remove((tamaño-5), 5);
                        }  
                    }
            }
        }
    }

    cargarExhibicionAdicional(publicacion, exhibicion){
        this.http.get('http://palacalle.abbottdata.com/backend/v1/exhibiciones-adicionales/', {cargarExhibicionAdicional: 1, publicacion: publicacion, exhibicion: exhibicion }, {})
        .then(data => {
            let datosExhibicionAdicional = JSON.parse(data.data);
            this.mostrarExhibicionAdicional = 1;
            this.uno = String(datosExhibicionAdicional.uno);
            if(this.uno == '1'){
                this.mostrarCampos = 1;
            }
            this.dos = String(datosExhibicionAdicional.dos);
            this.tres = String(datosExhibicionAdicional.tres);
            this.cuatro = String(datosExhibicionAdicional.cuatro);
            if(this.uno == "undefined")
                this.uno = '0';
            if(this.dos == "undefined")
                this.dos = '0';
            if(this.tres == "undefined")
                this.tres = '0';
            if(this.cuatro == "undefined")
                this.cuatro = '0';
            if(this.uno == "0"){
               this.foto_exhibicion = "sin-foto.jpg";
            }else
                this.foto_exhibicion = datosExhibicionAdicional.foto_exhibicion;
        })
        .catch(error => {
            this.mostrarExhibicionAdicional = 1;
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("Error cargarExhibicionAdicional: " + JSON.stringify(error));
        });
    }

    masCampos(){
            if(this.uno_ == 1){
                this.mostrarCampos = 1;
            }else{
                this.mostrarCampos = 0;
                this.uno = null;
                this.dos = null;
                this.tres = null;
                this.cuatro = null;
                this.dos_ = null;
                this.tres_ = null;
                this.cuatro_ = null;
            }
    }

    presentCerrarCasoModal() {
        let cerrarCaso = this.modalCtrl.create(CerrarCasoModalExhibicionAdicional, {publicacion: this.navParams.get('publicacion') });
        cerrarCaso.onDidDismiss(data => {
            let tamaño = this.navCtrl.length();
            this.navCtrl.remove((tamaño-5), 5);
        });
        cerrarCaso.present();
    }

}


@Component({
    template: `<ion-content class="fondo center">
                <div class="box-center">
                    <div>
                        <h3 class="center">Punto Evaluado</h3>
                        <div>He entendido toda la evaluación.</div>
                        <div>Es hora de gestionar y optimizar mi punto.</div>
                        <button class="btn btn-rounded btn-azul marginTop1" (click)="regresarCasos()">Regresar a Casos</button>
                        <button class="btn btn-rounded btn-azul marginTop1" (click)="cerrarCaso()">Cerrar Caso</button>
                    </div>
                </div>
            </ion-content>`,
    styles: [`
        .fondo{
            width: 100% !important;
            height: 100% !important;
            background: #fff !important;
        }
        button ion-icon, button{
            color: white !important;
        }
        .box-center {
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
      `]
})

export class CerrarCasoModalExhibicionAdicional {
    publicacion = 0;

    constructor(public navCtrl: NavController, private http: HTTP, params: NavParams, public viewCtrl: ViewController) {
        this.publicacion = params.get('publicacion');
    }

    regresarCasos(){
        this.viewCtrl.dismiss();
    }

    cerrarCaso(){
        this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {cerrarCaso: this.publicacion }, {})
        .then(data => {
            if(data.data == '1'){
                this.viewCtrl.dismiss();
            }else
                alert("¡Error al cerrar el caso!");
        })
        .catch(error => {
            if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log("Error cerrarCaso: " + JSON.stringify(error));
        });
            
    }

}