var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { CamaraProvider } from '../../providers/camara/camara';
import { HTTP } from '@ionic-native/http';
import { MenuEvaluarPunto } from '../menu-evaluar-punto/menu-evaluar-punto';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var NovedadesPage = (function () {
    function NovedadesPage(navCtrl, navParams, cameraProvider, actionSheetCtrl, http, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cameraProvider = cameraProvider;
        this.actionSheetCtrl = actionSheetCtrl;
        this.http = http;
        this.database = database;
        this.novedad = {
            id_p: "",
            usuario: "",
            nombre: "",
            canal: "",
            tipo: "",
            tipo_id: "",
            comentario: ""
        };
        this.foto = false;
        this.btn_guardar = "Guardar Valoración";
    }
    NovedadesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.novedad = {
            id_p: this.navParams.get('id_punto'),
            usuario: this.navParams.get('usuario'),
            nombre: this.navParams.get('nombre'),
            canal: this.navParams.get('canal'),
            tipo: this.navParams.get('tipo'),
            tipo_id: this.navParams.get('tipo_id'),
            comentario: ""
        };
        this.database.probarConexion()
            .then(function () {
            _this.internet = true;
        })
            .catch(function () {
            _this.internet = false;
        });
    };
    NovedadesPage.prototype.subirFoto = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Subir Imágen',
            buttons: [
                {
                    text: 'Camara',
                    icon: 'camera',
                    handler: function () {
                        _this.chosenPicture = "assets/loader.gif";
                        _this.cameraProvider.getPictureFromCamera().then(function (picture) {
                            if (picture) {
                                _this.chosenPicture = picture[0];
                                _this.imagenUpload = picture[1];
                                _this.foto = true;
                            }
                        }, function (error) {
                            alert("Error, Vuelva a subir la imágen");
                            _this.foto = false;
                            _this.chosenPicture = "";
                        });
                    }
                }, {
                    text: 'Cancelar',
                    icon: 'close',
                    role: 'destructive',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    NovedadesPage.prototype.guardar_Internet = function (datos) {
        var _this = this;
        this.http.post('http://palacalle.abbottdata.com/backend/v1/novedades/', { guardar: 1, datos: datos }, {})
            .then(function (data) {
            var respuesta = JSON.parse(data.data);
            if (respuesta > 0) {
                var args_1 = {
                    id_punto: _this.novedad.id_p,
                    estado: 1,
                    id_u: _this.novedad.usuario
                };
                _this.cameraProvider.cargarFotoServidor(_this.imagenUpload, respuesta, "publicaciones")
                    .then(function () {
                    var tamaño = _this.navCtrl.length();
                    _this.navCtrl.remove((tamaño - 2), 2)
                        .then(function () {
                        _this.navCtrl.push(MenuEvaluarPunto, args_1);
                    });
                })
                    .catch(function (error) { return error; });
            }
            else {
                alert("¡Ha ocurrido un error al almacenar la información!");
            }
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            alert("error: " + JSON.stringify(error));
            _this.btn_guardar = "Guardar Valoración";
        });
    };
    NovedadesPage.prototype.guardar_noInternet = function (datos) {
        var _this = this;
        this.database.guardarNovedad_noInternet(datos)
            .then(function (data) {
            var respuesta = data;
            var foto, args;
            if (respuesta > 0) {
                _this.database.fotoTipoPunto(_this.novedad.tipo_id)
                    .then(function (data) {
                    foto = data;
                    args = {
                        id_punto: _this.novedad.id_p,
                        estado: 2,
                        id_u: _this.novedad.usuario,
                        tipo: _this.novedad.canal + "-" + _this.novedad.tipo,
                        nombre: _this.novedad.nombre,
                        foto: _this.novedad.tipo_id + "." + foto,
                        tipo_id: _this.novedad.tipo_id,
                        internet: false,
                        califico: "n"
                    };
                    var tamaño = _this.navCtrl.length();
                    _this.navCtrl.remove((tamaño - 2), 2)
                        .then(function () {
                        _this.navCtrl.push(MenuEvaluarPunto, args);
                    });
                })
                    .catch(function (error) {
                    foto = "jpg";
                    args = {
                        id_punto: _this.novedad.id_p,
                        estado: 2,
                        id_u: _this.novedad.usuario,
                        tipo: _this.novedad.canal + "-" + _this.novedad.tipo,
                        nombre: _this.novedad.nombre,
                        foto: _this.novedad.tipo_id + "." + foto,
                        tipo_id: _this.novedad.tipo_id,
                        internet: false,
                        califico: "n"
                    };
                    var tamaño = _this.navCtrl.length();
                    _this.navCtrl.remove((tamaño - 2), 2)
                        .then(function () {
                        _this.navCtrl.push(MenuEvaluarPunto, args);
                    });
                });
            }
            else {
                alert("¡Ha ocurrido un error al almacenar la información!");
            }
        })
            .catch(function (error) {
            alert("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
    };
    NovedadesPage.prototype.guardar = function () {
        if ((this.novedad.usuario != "") || (this.novedad.id_p != "") || (this.novedad.comentario != "")) {
            if ((this.btn_guardar != "Procesando...")) {
                if ((this.novedad.comentario.length > 0) && (this.foto == true)) {
                    var datos = {
                        usuario: this.novedad.usuario,
                        punto: this.novedad.id_p,
                        comentario: this.novedad.comentario,
                        foto: "png"
                    };
                    this.btn_guardar = "Procesando...";
                    if (this.internet === true) {
                        this.guardar_Internet(datos);
                    }
                    else
                        this.guardar_noInternet(datos);
                }
                else {
                    if (this.foto == false) {
                        alert("¡Seleccione una foto!");
                    }
                    if (this.novedad.comentario.length == 0) {
                        alert("¡Ingrese un comentario!");
                    }
                }
            }
        }
        else
            alert("¡No se ha cargado bien la información del punto!");
    };
    return NovedadesPage;
}());
NovedadesPage = __decorate([
    Component({
        selector: 'page-novedades',
        templateUrl: 'novedades.html',
        providers: [HTTP]
    }),
    __metadata("design:paramtypes", [NavController, NavParams, CamaraProvider, ActionSheetController, HTTP, DatabaseServiceProvider])
], NovedadesPage);
export { NovedadesPage };
//# sourceMappingURL=novedades.js.map