import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { CamaraProvider } from '../../providers/camara/camara';
import { HTTP } from '@ionic-native/http';
import { MenuEvaluarPunto } from '../menu-evaluar-punto/menu-evaluar-punto';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

declare var cordova: any;

@Component({
	selector: 'page-novedades',
	templateUrl: 'novedades.html',
	providers: [HTTP]
})
export class NovedadesPage {
	novedad = {
		id_p : "",
		usuario : "",
		nombre: "", 
		canal: "", 
		tipo: "",
		tipo_id: "",
		comentario: ""
	};
	chosenPicture: any;
	imagenUpload = "";
	foto:boolean = false;
	btn_guardar = "Guardar Valoración";
	internet;

	constructor(public navCtrl: NavController, public navParams: NavParams, public cameraProvider: CamaraProvider, public actionSheetCtrl: ActionSheetController, private http: HTTP, private database: DatabaseServiceProvider) {}

	ionViewDidLoad() {
		this.novedad = {
			id_p : this.navParams.get('id_punto'),
			usuario : this.navParams.get('usuario'),
			nombre: this.navParams.get('nombre'), 
			canal: this.navParams.get('canal'), 
			tipo: this.navParams.get('tipo'),
			tipo_id: this.navParams.get('tipo_id'),
			comentario: ""
		};
		this.database.probarConexion()
        .then(() => {
        	this.internet = true;
        })
        .catch(() => {
            this.internet = false;
        });
	}
	
	subirFoto() {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Tomar Foto',
			buttons: [
			{
				text: 'Cámara',
				icon: 'camera',
				handler: () => {
					this.chosenPicture = "assets/loader.gif";
					this.cameraProvider.getPictureFromCamera().then(picture => {
		              if (picture) {
		                this.chosenPicture = picture[0];
		                this.imagenUpload = picture[1];
		                this.foto = true;
		              }
		            }, error => {
		              alert("Error, Vuelva a tomar la foto");
		              this.foto = false;
		              this.chosenPicture = "";
		            });
				}
			},{
				text: 'Cancelar',
				icon: 'close',
				role: 'destructive',
				handler: () => {
				}
			}
			]
		});
		actionSheet.present();
	}

	guardar_Internet(datos){
		this.http.post('http://palacalle.abbottdata.com/backend/v1/novedades/', {guardar:  1, datos : JSON.stringify(datos) }, {})
		.then(data => {
			let respuesta = JSON.parse(data.data);
            if(respuesta > 0){
            	let args = {
	                        id_punto: this.novedad.id_p,
	                        estado: 2,
	                        id_u: this.novedad.usuario,
	                        internet: true
	                    };
                /* Subiendo foto online*/
            	this.cameraProvider.cargarFotoServidor(this.imagenUpload, respuesta, "publicaciones")
        		.then(()=> {})
        		.catch(error => console.log("Error cargar foto" + JSON.stringify(error)));
        		/* Enviando mail al supervisor*/
        		let publicacion = {
                    id_p: respuesta,
                    punto: datos.punto
                };
                this.database.enviarAvisoSupervisor(publicacion);
                /* Regresando la interfaz*/
        		let tamaño = this.navCtrl.length();
                this.navCtrl.remove((tamaño-2), 2)
                .then(()=>{
                    this.navCtrl.push(MenuEvaluarPunto, args);
                })
                .catch(error => console.log("Error navCtrl.remove novedad: " + JSON.stringify(error)));

                
            }else{
                alert("¡Ha ocurrido un error al almacenar la información!");
            }
		})
		.catch(error => {
			if(error.status >= 500){
	        	alert("¡Error, el servidor no responde!");
        	}else
        	if(error.status >= 400){
        		alert("¡Error, no se encuentra el servidor!");
        	}else
        	if(error.status == 0){
          		alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
          	}
          	console.log("error: " + JSON.stringify(error));
          	this.btn_guardar = "Guardar Valoración";
		});
	}

	guardar_noInternet(datos){
		this.database.guardarNovedad_noInternet(datos)
        .then((data:any)=>{
            let respuesta = data;
            let foto, args;
            if(respuesta > 0){
            	this.database.fotoTipoPunto(this.novedad.tipo_id)
	            .then((data)=>{
	                foto = data;
	                args = {
	                        id_punto: this.novedad.id_p,
	                        estado: 2,
	                        id_u: this.novedad.usuario,
	                        tipo: this.novedad.canal + "-" + this.novedad.tipo,
	                        nombre: this.novedad.nombre,
	                        foto: this.novedad.tipo_id + "." + foto,
	                        tipo_id: this.novedad.tipo_id,
	                        internet: false,
	                        califico: "n"
	                    };
	                let tamaño = this.navCtrl.length();
	                this.navCtrl.remove((tamaño-2), 2)
	                .then(()=>{
	                    this.navCtrl.push(MenuEvaluarPunto, args);
	                });
	            })
	            .catch((error)=>{
	                foto = "jpg";
	                args = {
	                		id_punto: this.novedad.id_p,
	                        estado: 2,
	                        id_u: this.novedad.usuario,
	                        tipo: this.novedad.canal + "-" + this.novedad.tipo,
	                        nombre: this.novedad.nombre,
	                        foto: this.novedad.tipo_id + "." + foto,
	                        tipo_id: this.novedad.tipo_id,
	                        internet: false,
	                        califico: "n"
	                    };
	                let tamaño = this.navCtrl.length();
	                this.navCtrl.remove((tamaño-2), 2)
	                .then(()=>{
	                    this.navCtrl.push(MenuEvaluarPunto, args);
	                });
	            });
            }else{
                alert("¡Ha ocurrido un error al almacenar la información!");
            }
        })
        .catch((error)=>{
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
	}

	guardar(){
		if( (this.novedad.usuario != "") ||  (this.novedad.id_p != "") ||  (this.novedad.comentario != "") ){
			if( (this.btn_guardar != "Procesando...") ){
				if( (this.novedad.comentario.length > 0 ) && (this.imagenUpload.length > 10) ){
					let datos = {
						usuario : this.novedad.usuario,
						punto : this.novedad.id_p,
						comentario : this.novedad.comentario,
						foto : this.imagenUpload
					};
					this.btn_guardar = "Procesando...";
					
					if(this.internet === true){
						this.guardar_Internet(datos);
					}else
						this.guardar_noInternet(datos);
				}else {
					if(this.foto == false){
						alert("¡Seleccione una foto!");
					}
					if(this.novedad.comentario.length == 0){
						alert("¡Ingrese un comentario!");
					}
				}	
			}
		}else
			alert("¡No se ha cargado bien la información del punto!");
	}

}