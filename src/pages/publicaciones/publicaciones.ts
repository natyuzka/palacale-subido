import { Component, ViewChild } from '@angular/core';
import { App, NavController, NavParams, ActionSheetController, Content, LoadingController, AlertController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { ExhibicionFormulasPage } from '../exhibicion-formulas/exhibicion-formulas';
import { CalificarEventoPage } from '../calificar-evento/calificar-evento';
import { NovedadesResponderPage } from '../novedades-responder/novedades-responder';
import { CamaraProvider } from '../../providers/camara/camara';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

@Component({
	selector: 'page-publicaciones',
	templateUrl: 'publicaciones.html',
	providers: [HTTP]
})
export class PublicacionesPage {
	@ViewChild(Content) content: Content;
	publicaciones = [];
	ruta_usuarios:string; 
	ruta_publicaciones:string; 
	usuario = {
		id_u : "", 
		nombre: "", 
		foto: "",
		tipo: ""
	};
	casos = {
		activos: 0, 
		cerrados : 0
	};
	comentario = "";
	chosenPicture: any;
	imagenUpload: any;
	foto:boolean = false;
	esSupervisor = false;
	internet;
	mobile;

		constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private http: HTTP, private sanitizer: DomSanitizer, public actionSheetCtrl: ActionSheetController, public cameraProvider: CamaraProvider, public loadingCtrl: LoadingController, private database: DatabaseServiceProvider, public alertCtrl: AlertController, platform: Platform) {
			if (platform.is('ios')) {
	           this.mobile = "library-nosync";
	        }else
	        if (platform.is('android')) {
	           this.mobile = "files";
	        }
		}

		ionViewDidEnter() {
			this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				if( this.usuario.tipo == "s"){
					this.esSupervisor = true;
				}

				this.database.probarConexion()
			    .then(() => {
			    	this.ruta_usuarios = "http://palacalle.abbottdata.com/imagenes/usuarios/";
					this.ruta_publicaciones = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
			    	this.internet = true;
			    	this.casos_(this.usuario.id_u);
					this.publicaciones_all();
			    })
			    .catch((error) => {
			    	this.ruta_usuarios = "cdvfile://localhost/" + this.mobile + "/usuarios/";
					this.ruta_publicaciones = "cdvfile://localhost/" + this.mobile + "/publicaciones/";
			    	this.internet = false;
			    	this.publicaciones = [];
					this.casos = {
							activos: 0, 
							cerrados : 0
						};
			    });
			})
			.catch((error)=> {
				console.log("error storage: " + JSON.stringify(error));
			});			
		}

		getImagen(imagen, ruta){
			if(ruta == "usuario"){
				let version = String(Math.floor((Math.random() * 10) + 1));
				version = version + "." + String(Math.floor((Math.random() * 10) + 1));
				version = version + "." + String(Math.floor((Math.random() * 1000) + 1));
				return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_usuarios + imagen + "?v=" + version);
			}else
			if(ruta == "publicacion"){
				return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_publicaciones + imagen);
			}		
		}

		publicaciones_all(showLoading = ""){
			let datos;
			if( this.esSupervisor == true ){
				datos = {
					supervisor : true,
					usuario : this.usuario.id_u
				}
			}
			if(showLoading == ""){
				this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {publicaciones_all: 1, datos: JSON.stringify(datos)}, {})
				.then(data => {
					// console.log("publicaciones supervisor traidas: " + data.data);
					this.publicaciones = JSON.parse(data.data);
				})
				.catch(error => {
					if(error.status >= 500){
		                alert("¡Error, el servidor no responde!");
		            }else
		            if(error.status >= 400){
		                alert("¡Error, no se encuentra el servidor!");
		            }else
		            if(error.status == 0){
		                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
		            }
					console.log("error publicaciones_all : " + JSON.stringify(error));
				});
			}else{
				let loading = this.loadingCtrl.create();
				loading.present();
				this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {publicaciones_all: 1, datos: JSON.stringify(datos)}, {})
				.then(data => {
					// console.log("publicaciones normal traidas: " + data.data);
					this.publicaciones = JSON.parse(data.data);
					loading.dismiss();
				})
				.catch(error => {
					if(error.status >= 500){
		                alert("¡Error, el servidor no responde!");
		            }else
		            if(error.status >= 400){
		                alert("¡Error, no se encuentra el servidor!");
		            }else
		            if(error.status == 0){
		                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
		            }
					console.log("error publicaciones_all : " + JSON.stringify(error));
				});
			}			
		}

		casos_(usuario){
			let datos;
			if( this.esSupervisor == true ){
				datos = {
					supervisor : true,
					usuario : this.usuario.id_u
				}
			}
			this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {casos: 1, usuario: usuario, datos: JSON.stringify(datos)}, {})
			.then(data => {
				this.casos = JSON.parse(data.data);
			})
			.catch(error => {
				if(error.status >= 500){
	                alert("¡Error, el servidor no responde!");
	            }else
	            if(error.status >= 400){
	                alert("¡Error, no se encuentra el servidor!");
	            }else
	            if(error.status == 0){
	                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
	            }
				console.log("error casos_ : " + JSON.stringify(error));
			});
		}

		irPublicacion(publicacion){
			if( this.usuario.tipo == "s" ){
				if(publicacion.tipo == 'p'){
					this.navCtrl.push(ExhibicionFormulasPage, {
						publicacion: publicacion.id_p,
						internet: true
					});
				}else
				if(publicacion.tipo == 'e'){
					this.navCtrl.push(CalificarEventoPage, {
						publicacion: publicacion.id_p,
						internet: true
					});
				}else
				if(publicacion.tipo == 'n'){
					if( this.esSupervisor == true ){
						this.navCtrl.push(NovedadesResponderPage, {
							publicacion: publicacion
						});
					}else
					if(publicacion.respuesta != null){
						this.navCtrl.push(NovedadesResponderPage, {
							publicacion: publicacion
						});
					}
				}			
			}
					
		}

		verComentarios(publicacion){
			if(publicacion.vercomentarios == 1){
				publicacion.vercomentarios = 0;
				this.comentario = "";
			}else
			publicacion.vercomentarios = 1;
		}

		comentar(id_u, nombre, foto, publicacion){
			let foto_completa = foto;
			foto = foto_completa.substring(foto_completa.length - 3);
			let loading = this.loadingCtrl.create();
			loading.present();
			this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {guardarComentario: 1, usuario: id_u, usuario_nombre: nombre, usuario_foto: foto, publicacion: publicacion.id_p, comentario : this.comentario}, {})
			.then(data => {
				if(data.data == 0){
					alert("¡Ha ocurrido un error al guardar el comentario!");
				}else{
					let fecha = data.data;
					let comentario = {
						comentario : this.comentario,
						usuario_nombre : nombre,
						usuario_foto : foto_completa,
						fecha : fecha
					}
					this.comentario = "";
					publicacion.comentarios.push(comentario);
					publicacion.comentarios_total = parseInt(publicacion.comentarios_total) + 1;
				}
				loading.dismiss();
			})
			.catch(error => {
				if(error.status >= 500){
	                alert("¡Error, el servidor no responde!");
	            }else
	            if(error.status >= 400){
	                alert("¡Error, no se encuentra el servidor!");
	            }else
	            if(error.status == 0){
	                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
	            }
				console.log("error comentar : " + JSON.stringify(error));
				loading.dismiss();
			});
		}

		editarPerfil(){
			if(this.internet === true){
				let loading = this.loadingCtrl.create();
				loading.present();
				let actionSheet = this.actionSheetCtrl.create({
					title: 'Seleccione la opción',
					buttons: [
					{
						text: 'Cámara',
						icon: 'camera',
						handler: () => {
							this.chosenPicture = "assets/loader.gif";
							this.cameraProvider.getPictureFromCamera("1").then(picture => {
								if (picture) {
									this.chosenPicture = picture[0];
									this.imagenUpload = picture[1];
									this.foto = true;
									this.subirFoto(this.imagenUpload, this.usuario.id_u);
									loading.dismiss();
								}
							}, error => {
								alert("Error, Vuelva a tomar la foto");
								this.foto = false;
								this.chosenPicture = "";
								loading.dismiss();
							});
						}
					},
					{
						text: 'Galería',
						icon: 'image',
						handler: () => {
							this.chosenPicture = "assets/loader.gif";
							this.cameraProvider.getPictureFromPhotoLibrary("1").then(picture => {
								if (picture) {
									this.chosenPicture = picture[0];
									this.imagenUpload = picture[1];
									this.foto = true;
									this.subirFoto(this.imagenUpload, this.usuario.id_u);
									loading.dismiss();
								}
							}, error => {
								alert("Error, Vuelva a seleccionar la foto");
								this.foto = false;
								this.chosenPicture = "";
								loading.dismiss();
							});
						}
					},{
						text: 'Cancelar',
						icon: 'close',
						role: 'destructive',
						handler: () => {
							loading.dismiss();
						}
					}
					]
				});
				actionSheet.present();
			}	
		}

		//Hace proceso de subir la foto al servidor
		subirFoto(imagen, id){
			let loading = this.loadingCtrl.create();
			loading.present();
			this.cameraProvider.cargarFotoServidor(imagen, id, "usuarios")
    		.then(()=> {
                this.usuario.foto = id + ".png";
                this.storage.set('usuario', this.usuario);
                loading.dismiss();
    		}, error => {
				loading.dismiss();
			});
		}

		like(publicacion){
			let loading = this.loadingCtrl.create();
			loading.present();
			this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {guardarLike: 1, usuario: this.usuario.id_u,  publicacion: publicacion.id_p }, {})
			.then(data => {
				if(data.data > 0){
					publicacion.likes = parseInt(publicacion.likes) + 1;
				}
				loading.dismiss();
			})
			.catch(error => {
				if(error.status >= 500){
	                alert("¡Error, el servidor no responde!");
	            }else
	            if(error.status >= 400){
	                alert("¡Error, no se encuentra el servidor!");
	            }else
	            if(error.status == 0){
	                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
	            }
				console.log("error like : " + JSON.stringify(error));
				loading.dismiss();
			});
		}
		
		//Se envia un correo a el usuario que pidio ver la publicacion
		enviarPublicacion(publicacion){
			alert("¡Se envio la publicación a tu correo que tenemos registrado!");
			this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', {enviarPublicacion: 1,  publicacion: publicacion.id_p, usuario: this.usuario.id_u, tipo: publicacion.tipo}, {});
		}

		actualizarPublicaciones(){
			if(this.internet === true){
				this.publicaciones_all("loading");
				this.casos_(this.usuario.id_u);
			}
		}

		casosActivos(){
			if(this.internet === true){
				let loading = this.loadingCtrl.create();
				loading.present();
				let datos;
				if( this.esSupervisor == true ){
					datos = {
						supervisor : true,
						usuario : this.usuario.id_u
					}
				}
				this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { casosActivos: 1, usuario : this.usuario.id_u, datos: JSON.stringify(datos)}, {})
				.then(data => {
					loading.dismiss();
					this.publicaciones = JSON.parse(data.data);
				})
				.catch(error => {
					if(error.status >= 500){
		                alert("¡Error, el servidor no responde!");
		            }else
		            if(error.status >= 400){
		                alert("¡Error, no se encuentra el servidor!");
		            }else
		            if(error.status == 0){
		                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
		            }
					console.log("error casosActivos : " + JSON.stringify(error));
				});
			}
		}

		casosCerrados(){
			if(this.internet === true){
				let loading = this.loadingCtrl.create();
				loading.present();
				let datos;
				if( this.esSupervisor == true ){
					datos = {
						supervisor : true,
						usuario : this.usuario.id_u
					}
				}
				this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { casosCerrados: 1, usuario : this.usuario.id_u, datos: JSON.stringify(datos) }, {})
				.then(data => {
					loading.dismiss();
					this.publicaciones = JSON.parse(data.data);
				})
				.catch(error => {
					if(error.status >= 500){
		                alert("¡Error, el servidor no responde!");
		            }else
		            if(error.status >= 400){
		                alert("¡Error, no se encuentra el servidor!");
		            }else
		            if(error.status == 0){
		                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
		            }
					console.log("error casosCerrados : " + JSON.stringify(error));
				});
			}
		}

		cerrarSesion(){
			this.storage.remove('usuario')
			.then(()=>{
				this.app.getRootNav().popToRoot()
				.then(()=>{})
				.catch((error)=>{
				  console.log("error: " + JSON.stringify(error));
				});
			})
			.catch(()=>{
				alert("¡No se pudo cerrar la sesión!");
			});
		}
				
}