var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ActionSheetController, Content, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { DomSanitizer } from '@angular/platform-browser';
import { ExhibicionPage } from '../exhibicion/exhibicion';
import { CalificarEventoPage } from '../calificar-evento/calificar-evento';
import { NovedadesResponderPage } from '../novedades-responder/novedades-responder';
import { CamaraProvider } from '../../providers/camara/camara';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var PublicacionesPage = (function () {
    function PublicacionesPage(navCtrl, navParams, storage, http, sanitizer, actionSheetCtrl, cameraProvider, loadingCtrl, database, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.sanitizer = sanitizer;
        this.actionSheetCtrl = actionSheetCtrl;
        this.cameraProvider = cameraProvider;
        this.loadingCtrl = loadingCtrl;
        this.database = database;
        this.alertCtrl = alertCtrl;
        this.publicaciones = [];
        this.usuario = {
            id_u: "",
            nombre: "",
            foto: "",
            tipo: ""
        };
        this.casos = {
            activos: 0,
            cerrados: 0
        };
        this.comentario = "";
        this.foto = false;
        this.esSupervisor = false;
        this.ruta_usuarios = "http://palacalle.abbottdata.com/imagenes/usuarios/";
        this.ruta_publicaciones = "http://palacalle.abbottdata.com/imagenes/publicaciones/";
    }
    PublicacionesPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.storage.get('usuario')
            .then(function (val) {
            _this.usuario = val;
            if (_this.usuario.tipo == "s") {
                _this.esSupervisor = true;
            }
            _this.database.probarConexion()
                .then(function () {
                _this.internet = true;
                _this.casos_(_this.usuario.id_u);
                _this.publicaciones_all();
            })
                .catch(function () {
                _this.internet = false;
                _this.publicaciones = [];
                _this.casos = {
                    activos: 0,
                    cerrados: 0
                };
            });
        });
    };
    PublicacionesPage.prototype.getImagen = function (imagen, ruta) {
        if (ruta == "usuario") {
            var version = String(Math.floor((Math.random() * 10) + 1));
            version = version + "." + String(Math.floor((Math.random() * 10) + 1));
            version = version + "." + String(Math.floor((Math.random() * 1000) + 1));
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_usuarios + imagen + "?v=" + version);
        }
        else if (ruta == "publicacion") {
            return this.sanitizer.bypassSecurityTrustResourceUrl(this.ruta_publicaciones + imagen);
        }
    };
    PublicacionesPage.prototype.publicaciones_all = function (showLoading) {
        var _this = this;
        if (showLoading === void 0) { showLoading = ""; }
        var datos;
        if (this.esSupervisor == true) {
            datos = {
                supervisor: true,
                usuario: this.usuario.id_u
            };
        }
        if (showLoading == "") {
            this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { publicaciones_all: 1, datos: datos }, {})
                .then(function (data) {
                _this.publicaciones = JSON.parse(data.data);
            })
                .catch(function (error) {
                if (error.status >= 500) {
                    alert("¡Error, el servidor no responde!");
                }
                else if (error.status >= 400) {
                    alert("¡Error, no se encuentra el servidor!");
                }
                else if (error.status == 0) {
                    alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
                }
                console.log(error.status);
                console.log(error.error);
            });
        }
        else {
            var loading_1 = this.loadingCtrl.create();
            loading_1.present();
            this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { publicaciones_all: 1, datos: datos }, {})
                .then(function (data) {
                loading_1.dismiss();
                _this.publicaciones = JSON.parse(data.data);
            })
                .catch(function (error) {
                if (error.status >= 500) {
                    alert("¡Error, el servidor no responde!");
                }
                else if (error.status >= 400) {
                    alert("¡Error, no se encuentra el servidor!");
                }
                else if (error.status == 0) {
                    alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
                }
                console.log(error.status);
                console.log(error.error);
            });
        }
    };
    PublicacionesPage.prototype.casos_ = function (usuario) {
        var _this = this;
        var datos;
        if (this.esSupervisor == true) {
            datos = {
                supervisor: true,
                usuario: this.usuario.id_u
            };
        }
        this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { casos: 1, usuario: usuario, datos: datos }, {})
            .then(function (data) {
            _this.casos = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    PublicacionesPage.prototype.irPublicacion = function (publicacion) {
        if (publicacion.tipo == 'p') {
            this.navCtrl.push(ExhibicionPage, {
                publicacion: publicacion.id_p,
                internet: true
            });
        }
        else if (publicacion.tipo == 'e') {
            this.navCtrl.push(CalificarEventoPage, {
                publicacion: publicacion.id_p,
                internet: true
            });
        }
        else if (publicacion.tipo == 'n') {
            if (this.esSupervisor == true) {
                this.navCtrl.push(NovedadesResponderPage, {
                    publicacion: publicacion
                });
            }
            else if (publicacion.respuesta != null) {
                this.navCtrl.push(NovedadesResponderPage, {
                    publicacion: publicacion
                });
            }
        }
    };
    PublicacionesPage.prototype.verComentarios = function (publicacion) {
        if (publicacion.vercomentarios == 1) {
            publicacion.vercomentarios = 0;
            this.comentario = "";
        }
        else
            publicacion.vercomentarios = 1;
    };
    PublicacionesPage.prototype.comentar = function (id_u, nombre, foto, publicacion) {
        var _this = this;
        var foto_completa = foto;
        foto = foto_completa.substring(foto_completa.length - 3);
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { guardarComentario: 1, usuario: id_u, usuario_nombre: nombre, usuario_foto: foto, publicacion: publicacion.id_p, comentario: this.comentario }, {})
            .then(function (data) {
            if (data.data == 0) {
                alert("¡Ha ocurrido un error al guardar el comentario!");
            }
            else {
                var fecha = data.data;
                var comentario = {
                    comentario: _this.comentario,
                    usuario_nombre: nombre,
                    usuario_foto: foto_completa,
                    fecha: fecha
                };
                _this.comentario = "";
                publicacion.comentarios.push(comentario);
                publicacion.comentarios_total = parseInt(publicacion.comentarios_total) + 1;
            }
            loading.dismiss();
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
            loading.dismiss();
        });
    };
    PublicacionesPage.prototype.editarPerfil = function () {
        var _this = this;
        if (this.internet === true) {
            var loading_2 = this.loadingCtrl.create();
            loading_2.present();
            var actionSheet = this.actionSheetCtrl.create({
                title: 'Subir Imágen',
                buttons: [
                    {
                        text: 'Camara',
                        icon: 'camera',
                        handler: function () {
                            _this.chosenPicture = "assets/loader.gif";
                            _this.cameraProvider.getPictureFromCamera("1").then(function (picture) {
                                if (picture) {
                                    _this.chosenPicture = picture[0];
                                    _this.imagenUpload = picture[1];
                                    _this.foto = true;
                                    _this.subirFoto(_this.imagenUpload, _this.usuario.id_u);
                                    loading_2.dismiss();
                                }
                            }, function (error) {
                                alert("Error, Vuelva a subir la imágen");
                                _this.foto = false;
                                _this.chosenPicture = "";
                                loading_2.dismiss();
                            });
                        }
                    },
                    {
                        text: 'Galería',
                        icon: 'image',
                        handler: function () {
                            _this.chosenPicture = "assets/loader.gif";
                            _this.cameraProvider.getPictureFromPhotoLibrary("1").then(function (picture) {
                                if (picture) {
                                    _this.chosenPicture = picture[0];
                                    _this.imagenUpload = picture[1];
                                    _this.foto = true;
                                    _this.subirFoto(_this.imagenUpload, _this.usuario.id_u);
                                    loading_2.dismiss();
                                }
                            }, function (error) {
                                alert("Error, Vuelva a subir la imágen");
                                _this.foto = false;
                                _this.chosenPicture = "";
                                loading_2.dismiss();
                            });
                        }
                    }, {
                        text: 'Cancelar',
                        icon: 'close',
                        role: 'destructive',
                        handler: function () {
                            loading_2.dismiss();
                        }
                    }
                ]
            });
            actionSheet.present();
        }
    };
    PublicacionesPage.prototype.subirFoto = function (imagen, id) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.cameraProvider.cargarFotoServidor(imagen, id, "usuarios")
            .then(function () {
            _this.usuario.foto = id + ".png";
            _this.storage.set('usuario', _this.usuario);
            loading.dismiss();
        }, function (error) {
            loading.dismiss();
        });
    };
    PublicacionesPage.prototype.like = function (publicacion) {
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.post('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { guardarLike: 1, usuario: this.usuario.id_u, publicacion: publicacion.id_p }, {})
            .then(function (data) {
            if (data.data > 0) {
                publicacion.likes = parseInt(publicacion.likes) + 1;
            }
            loading.dismiss();
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
            loading.dismiss();
        });
    };
    PublicacionesPage.prototype.enviarPublicacion = function (publicacion) {
        this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { enviarPublicacion: 1, publicacion: publicacion.id_p, usuario: this.usuario.id_u }, {})
            .then(function (data) {
            if (data.data == "1") {
                alert("¡Se envio un link de la publicación a tu correo que tenemos registrado!");
            }
            else
                alert("¡Error al enviar el mail!");
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    PublicacionesPage.prototype.actualizarPublicaciones = function () {
        if (this.internet === true) {
            this.publicaciones_all("loading");
        }
    };
    PublicacionesPage.prototype.casosActivos = function () {
        var _this = this;
        if (this.internet === true) {
            var loading_3 = this.loadingCtrl.create();
            loading_3.present();
            var datos = void 0;
            if (this.esSupervisor == true) {
                datos = {
                    supervisor: true,
                    usuario: this.usuario.id_u
                };
            }
            this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { casosActivos: 1, usuario: this.usuario.id_u, datos: datos }, {})
                .then(function (data) {
                loading_3.dismiss();
                _this.publicaciones = JSON.parse(data.data);
            })
                .catch(function (error) {
                if (error.status >= 500) {
                    alert("¡Error, el servidor no responde!");
                }
                else if (error.status >= 400) {
                    alert("¡Error, no se encuentra el servidor!");
                }
                else if (error.status == 0) {
                    alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
                }
                console.log(error.status);
                console.log(error.error);
            });
        }
    };
    PublicacionesPage.prototype.casosCerrados = function () {
        var _this = this;
        if (this.internet === true) {
            var loading_4 = this.loadingCtrl.create();
            loading_4.present();
            var datos = void 0;
            if (this.esSupervisor == true) {
                datos = {
                    supervisor: true,
                    usuario: this.usuario.id_u
                };
            }
            this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { casosCerrados: 1, usuario: this.usuario.id_u, datos: datos }, {})
                .then(function (data) {
                loading_4.dismiss();
                _this.publicaciones = JSON.parse(data.data);
            })
                .catch(function (error) {
                if (error.status >= 500) {
                    alert("¡Error, el servidor no responde!");
                }
                else if (error.status >= 400) {
                    alert("¡Error, no se encuentra el servidor!");
                }
                else if (error.status == 0) {
                    alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
                }
                console.log(error.status);
                console.log(error.error);
            });
        }
    };
    PublicacionesPage.prototype.uploadInfo = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '¿Desea continuar?',
            message: 'Necesita estar conectado a internet, se recomienda usar una conexión Wifi',
            buttons: [
                {
                    text: 'Cancelar',
                    handler: function () { }
                },
                {
                    text: 'Aceptar',
                    handler: function () {
                        var loading = _this.loadingCtrl.create();
                        loading.present();
                        _this.database.uploadInfo()
                            .then(function () {
                            loading.dismiss();
                            alert("¡Se cargo con éxito!");
                        })
                            .catch(function () {
                            loading.dismiss();
                            alert("¡Error al cargar la información!");
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    return PublicacionesPage;
}());
__decorate([
    ViewChild(Content),
    __metadata("design:type", Content)
], PublicacionesPage.prototype, "content", void 0);
PublicacionesPage = __decorate([
    Component({
        selector: 'page-publicaciones',
        templateUrl: 'publicaciones.html',
        providers: [HTTP]
    }),
    __metadata("design:paramtypes", [NavController, NavParams, Storage, HTTP, DomSanitizer, ActionSheetController, CamaraProvider, LoadingController, DatabaseServiceProvider, AlertController])
], PublicacionesPage);
export { PublicacionesPage };
//# sourceMappingURL=publicaciones.js.map