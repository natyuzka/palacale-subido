import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

@Component({
	selector: 'page-visita-medica',
	templateUrl: 'visita-medica.html',
})
export class VisitaMedicaPage {

	usuario;
	fuerza = "";
	especialidad = "";
	visitador = "";
	fuerzas = [
		{
			id_f:"",
			nombre: "Fuerza"
		}
	];
	especialidades = [
		{
			id_e:"",
			nombre: "Especialidad"
		}
	];
	visitadores = [
		{
			id_v:"",
			nombre: "Visitador",
			telefono: "Teléfono"
		}
	];
	productos;
	uno:number;
    dos:number;
    tres:number;
    cuatro:number;
    comentario = "";
    btn_guardar = "Guardar Valoración";
    fuerza_m = "";
    especialidad_m = "";
	visitador_m = "";
    visitador_mes = {
    	v_id	: "",
		v_nombre	: "",
		v_telefono	: "",
		f_id	: "",
		f_nombre	: "",
		especialidades : [
			{
				id_e:"",
				nombre: "-- Seleccione --"
			}]
    };
    visita_tipo = "v-mes";
    internet;
    visitador_telefono = "Teléfono";

	constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP, private storage: Storage, public loadingCtrl: LoadingController, private database: DatabaseServiceProvider) {}

	ionViewDidEnter(){
		this.database.probarConexion()
        .then(() => {
        	this.internet = true;
        	this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				this.mostrarVisitadorMes_Internet();
				this.mostrarFuerzas();
			});
        })
        .catch(() => {
            this.internet = false;
            this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				this.mostrarVisitadorMes_no_Internet();
        		this.mostrarFuerzas_noInternet();
			});
        });
        this.productos = [];
	}

	mostrarVisitadorMes_Internet(){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', {mostrarVisitadorMes:  1, usuario: this.usuario.id_u }, {})
		.then(data => {
			let res = JSON.parse(data.data);
			this.visitador_mes = res[0];
			this.fuerza_m = this.visitador_mes.f_id;
			this.visitador_m = this.visitador_mes.v_id;
			this.visitador_mes.especialidades = res[1];
		})
		.catch(error => {
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log(error.status);
			console.log(error.error);
		});
	}

	/*   
	* --------------------------------- SIN INTERNET
	*/

	mostrarFuerzas_noInternet(){
		this.database.mostrarFuerzas_noInternet()
        .then((data:any)=>{
            this.fuerzas = data;
        })
        .catch((error)=>{
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
	}

	mostrarEspecialidades_noInternet(fuerza){
		this.database.mostrarEspecialidades_noInternet(fuerza)
        .then((data:any)=>{
            this.especialidades = data;
        })
        .catch((error)=>{
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
	}

	mostrarVisitadores_noInternet(especialidad){
		this.database.mostrarVisitadores_noInternet(especialidad)
        .then((data:any)=>{
            this.visitadores = data;
        })
        .catch((error)=>{
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
		this.mostrarProductos_noInternet(especialidad);
	}

	mostrarProductos_noInternet(especialidad){
		let loading = this.loadingCtrl.create();
		loading.present();
		this.database.mostrarProductos_noInternet(especialidad)
        .then((data:any)=>{
            this.productos = data;
            loading.dismiss();
        })
        .catch((error)=>{
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
            loading.dismiss();
        });
	}

	mostrarVisitadorMes_no_Internet(){
		this.database.mostrarVisitadorMes_no_Internet(this.usuario.id_u)
        .then((data)=>{
			this.visitador_mes = data[0];
			this.fuerza_m = this.visitador_mes.f_id;
			this.visitador_m = this.visitador_mes.v_id;
			this.visitador_mes.especialidades = data[1];
        })
        .catch((error)=>{
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
	}

	/*   
	* --------------------------------- END SIN INTERNET
	*/

	mostrarFuerzas(){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', {mostrarFuerzas:  1}, {})
		.then(data => {
			this.fuerzas = JSON.parse(data.data);
		})
		.catch(error => {
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log(error.status);
			console.log(error.error);
		});
	}

	mostrarEspecialidades(fuerza){
		this.especialidades = [
			{
				id_e:"",
				nombre: "Especialidad"
			}
		];
		this.visitadores = [
			{
				id_v:"",
				nombre: "Visitador",
				telefono: "Teléfono"
			}
		];
		this.visitador_telefono = "Teléfono";
		if(this.internet === true){
			this.mostrarEspecialidades_Internet(fuerza);
		}else
			this.mostrarEspecialidades_noInternet(fuerza);
	}

	mostrarVisitadores(especialidad){
		this.visitadores = [
			{
				id_v:"",
				nombre: "Visitador",
				telefono: "Teléfono"
			}
		];
		this.visitador_telefono = "Teléfono";
		if(this.internet === true){
			this.mostrarVisitadores_Internet(especialidad);
		}else
			this.mostrarVisitadores_noInternet(especialidad);
	}

	mostrarProductos(especialidad){
		if(this.internet === true){
			this.mostrarProductos_Internet(especialidad);
		}else
			this.mostrarProductos_noInternet(especialidad);
	}

	mostrarEspecialidades_Internet(fuerza){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', {mostrarEspecialidades:  1, fuerza : fuerza}, {})
		.then(data => {
			this.especialidades = JSON.parse(data.data);
		})
		.catch(error => {
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log(error.status);
			console.log(error.error);
		});
	}

	mostrarVisitadores_Internet(especialidad){
		this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', {mostrarVisitadores:  1, especialidad : especialidad}, {})
		.then(data => {
			this.visitadores = JSON.parse(data.data);
		})
		.catch(error => {
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log(error.status);
			console.log(error.error);
		});
		this.mostrarProductos(especialidad);
	}

	colocarTelefonoVisitador(visitador_id){
		for (let visitador of this.visitadores) {
			if(visitador.id_v == visitador_id){
				this.visitador_telefono = visitador.telefono;
				break;
			}
		}
	}

	mostrarProductos_Internet(especialidad){
		let loading = this.loadingCtrl.create();
		loading.present();
		this.http.get('http://palacalle.abbottdata.com/backend/v1/productos/', {mostrarProductosVisitas:  1, especialidad : especialidad}, {})
		.then(data => {
			this.productos = JSON.parse(data.data);
			loading.dismiss();
		})
		.catch(error => {
			loading.dismiss();
			if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
			console.log(error.status);
			console.log(error.error);
		});
	}

	guardarDatos_Internet(datos){
		this.http.post('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', {guardar:1, datos: JSON.stringify(datos)}, {})
        .then(data => {
            let respuesta = data.data;
            if( parseInt(respuesta) == 1){
            	alert("¡Guardado!");
            	let tamaño = this.navCtrl.length();
                this.navCtrl.remove((tamaño-1), 1);
            }else{
            	this.btn_guardar = "Guardar Valoración";
            	alert("¡Ha ocurrido un error al almacenar la información!");
            }   
        })
        .catch(error => {
        	this.btn_guardar = "Guardar Valoración";
        	if(error.status >= 500){
                alert("¡Error, el servidor no responde!");
            }else
            if(error.status >= 400){
                alert("¡Error, no se encuentra el servidor!");
            }else
            if(error.status == 0){
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta");
            }
            console.log(error.status);
            console.log(error.error);
        });
	}

	guardarDatos_noInternet(datos){
		this.database.guardarVisitaMedica_noInternet(datos)
        .then((data:any)=>{
            let respuesta = data;
            if( parseInt(respuesta) == 1){
            	alert("¡Guardado!");
            	let tamaño = this.navCtrl.length();
                this.navCtrl.remove((tamaño-1), 1);
            }else{
            	this.btn_guardar = "Guardar Valoración";
            	alert("¡Ha ocurrido un error al almacenar la información!");
            }
        })
        .catch((error)=>{
        	this.btn_guardar = "Guardar Valoración";
            console.log("Error visita medica guardarVisitaMedica_noInternet: " + JSON.stringify(error));
        });
	}

	guardar(){
		if( this.usuario.id_u != "" ){
			if( (this.btn_guardar != "Procesando...") ){
		        if(  (this.comentario.length > 0) && (this.uno >= 0) && (this.dos >= 0) && (this.tres >= 0) && (this.cuatro >= 0)  ){
		        	this.btn_guardar = "Procesando...";
		        	var datos = {
		            	usuario: this.usuario.id_u,
		                comentario: this.comentario,
		                fuerza: "",
						especialidad: "",
						visitador: "",
		                uno : this.uno,
		                dos : this.dos,
		                tres : this.tres,
		                cuatro : this.cuatro
		            };

		            if(this.visita_tipo == "v-mes"){
		        		datos.fuerza = this.fuerza_m;
		        		datos.especialidad = this.especialidad_m;
		        		datos.visitador = this.visitador_m;
		        	}else{
		        		datos.fuerza = this.fuerza;
		        		datos.especialidad = this.especialidad;
		        		datos.visitador = this.visitador;
		        	}

		            if(this.internet === true){
						this.guardarDatos_Internet(datos);
					}else
						this.guardarDatos_noInternet(datos);
		        }else
		            alert("¡Completa todos los campos!");
		    }
		}else
			alert("¡No se ha cargado bien la información!");
          
    }

}
