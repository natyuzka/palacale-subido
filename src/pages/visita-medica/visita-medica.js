var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
var VisitaMedicaPage = (function () {
    function VisitaMedicaPage(navCtrl, navParams, http, storage, loadingCtrl, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.database = database;
        this.fuerza = "";
        this.especialidad = "";
        this.visitador = "";
        this.fuerzas = [
            {
                id_f: "",
                nombre: "Fuerza"
            }
        ];
        this.especialidades = [
            {
                id_e: "",
                nombre: "Especialidad"
            }
        ];
        this.visitadores = [
            {
                id_v: "",
                nombre: "Visitador"
            }
        ];
        this.comentario = "";
        this.btn_guardar = "Guardar Valoración";
        this.fuerza_m = "";
        this.especialidad_m = "";
        this.visitador_m = "";
        this.visitador_mes = {
            v_id: "",
            v_nombre: "",
            f_id: "",
            f_nombre: "",
            especialidades: [
                {
                    id_e: "",
                    nombre: "-- Seleccione --"
                }
            ]
        };
        this.visita_tipo = "v-mes";
    }
    VisitaMedicaPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('usuario')
            .then(function (val) {
            _this.usuario = val;
            // this.mostrarVisitadorMes();
        });
    };
    VisitaMedicaPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.database.probarConexion()
            .then(function () {
            _this.internet = true;
            _this.mostrarFuerzas();
        })
            .catch(function () {
            _this.internet = false;
            _this.mostrarFuerzas_noInternet();
        });
    };
    /*
    * --------------------------------- SIN INTERNET
    */
    VisitaMedicaPage.prototype.mostrarFuerzas_noInternet = function () {
        var _this = this;
        this.database.mostrarFuerzas_noInternet()
            .then(function (data) {
            _this.fuerzas = data;
        })
            .catch(function (error) {
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
    };
    VisitaMedicaPage.prototype.mostrarEspecialidades_noInternet = function (fuerza) {
        var _this = this;
        this.database.mostrarEspecialidades_noInternet(fuerza)
            .then(function (data) {
            _this.especialidades = data;
        })
            .catch(function (error) {
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
    };
    VisitaMedicaPage.prototype.mostrarVisitadores_noInternet = function (especialidad) {
        var _this = this;
        this.database.mostrarVisitadores_noInternet(especialidad)
            .then(function (data) {
            _this.visitadores = data;
        })
            .catch(function (error) {
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
        });
        this.mostrarProductos_noInternet(especialidad);
    };
    VisitaMedicaPage.prototype.mostrarProductos_noInternet = function (especialidad) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.database.mostrarProductos_noInternet(especialidad)
            .then(function (data) {
            _this.productos = data;
            loading.dismiss();
        })
            .catch(function (error) {
            console.log("Error mostrarFuerzas_noInternet: " + JSON.stringify(error));
            loading.dismiss();
        });
    };
    /*
    * --------------------------------- END SIN INTERNET
    */
    VisitaMedicaPage.prototype.mostrarVisitadorMes = function () {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', { mostrarVisitadorMes: 1, usuario: this.usuario.id_u }, {})
            .then(function (data) {
            var res = JSON.parse(data.data);
            _this.visitador_mes = res[0];
            _this.fuerza_m = _this.visitador_mes.f_id;
            _this.visitador_m = _this.visitador_mes.v_id;
            _this.visitador_mes.especialidades = res[1];
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    VisitaMedicaPage.prototype.mostrarFuerzas = function () {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', { mostrarFuerzas: 1 }, {})
            .then(function (data) {
            _this.fuerzas = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    VisitaMedicaPage.prototype.mostrarEspecialidades = function (fuerza) {
        if (this.internet === true) {
            this.mostrarEspecialidades_Internet(fuerza);
        }
        else
            this.mostrarEspecialidades_noInternet(fuerza);
    };
    VisitaMedicaPage.prototype.mostrarVisitadores = function (especialidad) {
        if (this.internet === true) {
            this.mostrarVisitadores_Internet(especialidad);
        }
        else
            this.mostrarVisitadores_noInternet(especialidad);
    };
    VisitaMedicaPage.prototype.mostrarEspecialidades_Internet = function (fuerza) {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', { mostrarEspecialidades: 1, fuerza: fuerza }, {})
            .then(function (data) {
            _this.especialidades = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    VisitaMedicaPage.prototype.mostrarVisitadores_Internet = function (especialidad) {
        var _this = this;
        this.http.get('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', { mostrarVisitadores: 1, especialidad: especialidad }, {})
            .then(function (data) {
            _this.visitadores = JSON.parse(data.data);
        })
            .catch(function (error) {
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
        this.mostrarProductos(especialidad);
    };
    VisitaMedicaPage.prototype.mostrarProductos = function (especialidad) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.http.get('http://palacalle.abbottdata.com/backend/v0/productos/', { mostrarProductosVisitas: 1, especialidad: especialidad }, {})
            .then(function (data) {
            _this.productos = JSON.parse(data.data);
            loading.dismiss();
        })
            .catch(function (error) {
            loading.dismiss();
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    VisitaMedicaPage.prototype.guardarDatos_Internet = function (datos) {
        var _this = this;
        this.http.post('http://palacalle.abbottdata.com/backend/v1/visitas-medicas/', { guardar: 1, datos: JSON.stringify(datos) }, {})
            .then(function (data) {
            var respuesta = data.data;
            if (parseInt(respuesta) == 1) {
                alert("¡Guardado!");
                var tamaño = _this.navCtrl.length();
                _this.navCtrl.remove((tamaño - 1), 1);
            }
            else {
                _this.btn_guardar = "Guardar Valoración";
                alert("¡Ha ocurrido un error al almacenar la información!");
            }
        })
            .catch(function (error) {
            _this.btn_guardar = "Guardar Valoración";
            if (error.status >= 500) {
                alert("¡Error, el servidor no responde!");
            }
            else if (error.status >= 400) {
                alert("¡Error, no se encuentra el servidor!");
            }
            else if (error.status == 0) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta");
            }
            console.log(error.status);
            console.log(error.error);
        });
    };
    VisitaMedicaPage.prototype.guardarDatos_noInternet = function (datos) {
        var _this = this;
        this.database.guardarVisitaMedica_noInternet(datos)
            .then(function (data) {
            var respuesta = data;
            if (parseInt(respuesta) == 1) {
                alert("¡Guardado!");
                var tamaño = _this.navCtrl.length();
                _this.navCtrl.remove((tamaño - 1), 1);
            }
            else {
                _this.btn_guardar = "Guardar Valoración";
                alert("¡Ha ocurrido un error al almacenar la información!");
            }
        })
            .catch(function (error) {
            _this.btn_guardar = "Guardar Valoración";
            alert("Error visita medica guardarVisitaMedica_noInternet: " + JSON.stringify(error));
        });
    };
    VisitaMedicaPage.prototype.guardar = function () {
        if (this.usuario.id_u != "") {
            if ((this.btn_guardar != "Procesando...")) {
                if ((this.comentario.length > 0) && (this.uno >= 0) && (this.dos >= 0) && (this.tres >= 0) && (this.cuatro >= 0)) {
                    var datos = {
                        usuario: this.usuario.id_u,
                        comentario: this.comentario,
                        fuerza: "",
                        especialidad: "",
                        visitador: "",
                        uno: this.uno,
                        dos: this.dos,
                        tres: this.tres,
                        cuatro: this.cuatro
                    };
                    datos.fuerza = this.fuerza;
                    datos.especialidad = this.especialidad;
                    datos.visitador = this.visitador;
                    this.btn_guardar = "Procesando...";
                    if (this.internet === true) {
                        this.guardarDatos_Internet(datos);
                    }
                    else
                        this.guardarDatos_noInternet(datos);
                }
                else
                    alert("¡Completa todos los campos!");
            }
        }
        else
            alert("¡No se ha cargado bien la información!");
    };
    return VisitaMedicaPage;
}());
VisitaMedicaPage = __decorate([
    Component({
        selector: 'page-visita-medica',
        templateUrl: 'visita-medica.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, HTTP, Storage, LoadingController, DatabaseServiceProvider])
], VisitaMedicaPage);
export { VisitaMedicaPage };
//# sourceMappingURL=visita-medica.js.map