import { Component } from '@angular/core';
import { App, NavController, AlertController, ModalController, ViewController, Platform, LoadingController } from 'ionic-angular';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
import { CamaraProvider } from '../../providers/camara/camara';
import { Storage } from '@ionic/storage';

@Component({
	selector: 'page-settings',
	templateUrl: 'settings.html',
})

export class SettingsPage {

	internet;

	constructor(public app: App, public navCtrl: NavController, private database: DatabaseServiceProvider, public alertCtrl: AlertController, public modalCtrl: ModalController, private storageLocal: Storage, public loadingCtrl: LoadingController, public cameraProvider: CamaraProvider) { }

	ionViewDidEnter() {
		this.storageLocal.get('internet')
			.then((val) => {
				this.internet = val;
				if (val == null)
					this.internet = true;
				else
					this.internet = val;
			})
			.catch((error) => {
				this.internet = true;
				console.log("error storage: " + JSON.stringify(error));
			});
	}

	cambioInternet() {
		this.storageLocal.set('internet', this.internet);
	}
	//Llega un array de arrays y se iitera por el primer arraya para subir la foto al servidor
	traerFotos(data) {
		return new Promise((resolve, reject) => {
			let size = data.length;
			for (var i = 0; i < size; i++) {
				console.log(data[i]);
				this.database.traerFotos(data[i])
					.then((datos) => {
						this.cameraProvider.cargarFotoServidor(datos[0], datos[1], datos[2])
							.then(() => {
								resolve(true)
							})
							.catch(error => {
								alert("¡Ha ocurrido un error al cargar las imágenes!");
								reject(error);
							});
					})
					.catch((error) => {
						alert("¡Error al cargar la información!");
						reject(error);
					});
			};
		});
	}

	//Sube la informacion que se hizo offline 
	uploadInfo() {
		let confirm = this.alertCtrl.create({
			title: '¿Desea continuar?',
			message: 'Necesita estar conectado a internet, se recomienda usar una conexión Wifi',
			buttons: [{
				text: 'Cancelar',
				handler: () => { }
			},
			{
				text: 'Aceptar',
				handler: () => {
					let loading = this.loadingCtrl.create();
					loading.present();
					this.database.uploadInfo()
						.then((data) => {
							// console.log("this.database.uploadInfo: " + JSON.stringify(data));
							if (data != 1) {
								if (data != 0) {
									this.traerFotos(data)
										.then(() => {
											// console.log("traerFotos data != 0 traerFotos bn: ");
											loading.dismiss();
											this.database.eliminarInfoLocal();
											alert("¡Se cargo con éxito!");
										})
										.catch((error) => {
											// console.log("traerFotos data != 0 traerFotos mal: ");
											loading.dismiss();
											this.database.eliminarInfoLocal();
											alert("¡Error al consultar las fotos! ");
										});
								} else {
									// console.log("uploadInfo else data != 0");
									loading.dismiss();
									this.database.eliminarInfoLocal();
									alert("¡Se cargo con éxito!");
								}
							} else {
								// console.log("data == 0");
								loading.dismiss();
								alert("¡No tienes información que cargar!");
							}
						})
						.catch((error) => {
							console.log("Error, this.database.uploadInfo: " + JSON.stringify(error));
							loading.dismiss();
							alert("¡Error al cargar la información!");
						});
				}
			}]
		});
		confirm.present();
	}

	actualizarBaseDatos() {
		let confirm = this.alertCtrl.create({
			title: '¿Desea continuar?',
			message: 'Necesita estar conectado a internet, se recomienda usar una conexión Wifi',
			buttons: [{
				text: 'Cancelar',
				handler: () => { }
			},
			{
				text: 'Aceptar',
				handler: () => {
					let modal = this.modalCtrl.create(ModalDescarga2, {}, { enableBackdropDismiss: false });
					modal.present();
				}
			}]
		});
		confirm.present();
	}

	cerrarSesion() {
		this.storageLocal.remove('usuario')
			.then(() => {
				this.app.getRootNav().popToRoot()
					.then(() => { })
					.catch((error) => {
						console.log("error: " + JSON.stringify(error));
					});
			})
			.catch(() => {
				alert("¡No se pudo cerrar la sesión!");
			});
	}

}


@Component({
	template: `<ion-content class="fondo center">
	<div align="right">
	<button (click)="dismiss()">
	<ion-icon name="close-circle" item-right></ion-icon>
	</button>
	</div>
	<br><br>
	<div align="center">
	<p id="text-overlay" class="text-overlay"></p>
	<progress max="100" value="0" id="progreso-actualizacion"></progress>
	</div>
	</ion-content>`,
	styles: [`
	.fondo{
		width: 100% !important;
		height: 100% !important;
		background: rgba(0,0,0,0.4) !important;
	}
	button ion-icon, button{
		color: white !important;
		background: transparent !important;
		font-size: 32px !important;
		width: 30px;
	}
	`]
})


export class ModalDescarga2 {

	constructor(private databaseService: DatabaseServiceProvider, private platform: Platform, public viewCtrl: ViewController, private storage: Storage) { }
	usuario;
	ionViewDidEnter() {
		this.storage.get('usuario')
			.then((val) => {
				this.usuario = val;
				console.log("Usuario logueado" + this.usuario.id_u);
			})
			.catch((error) => {
				console.log("Error" + error);
				
			});
		this.databaseService.actualizarApp()
			.then(() => {
				this.viewCtrl.dismiss();
			})
			.catch((error) => {
				this.viewCtrl.dismiss();
			});
		this.platform.registerBackButtonAction(() => {
			this.databaseService.cancelarDescarga();
		});
	}

	dismiss() {
		this.databaseService.cancelarDescarga();
		this.viewCtrl.dismiss();
	}

}