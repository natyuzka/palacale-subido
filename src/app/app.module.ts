import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage} from '../pages/login/login';
import { PublicacionesPage } from '../pages/publicaciones/publicaciones';
import { MapaPage } from '../pages/mapa/mapa';
import { MenuEvaluarPunto } from '../pages/menu-evaluar-punto/menu-evaluar-punto';
import { NovedadesPage } from '../pages/novedades/novedades';
import { NovedadesResponderPage } from '../pages/novedades-responder/novedades-responder';
import { ExhibicionPage, CerrarCasoModal } from '../pages/exhibicion/exhibicion';
import { ExhibicionFormulasPage } from '../pages/exhibicion-formulas/exhibicion-formulas';
import { ExhibicionComplementosPage } from '../pages/exhibicion-complementos/exhibicion-complementos';
import { ExhibicionPedialytePage } from '../pages/exhibicion-pedialyte/exhibicion-pedialyte';
import { ExhibicionAdicionalPage, CerrarCasoModalExhibicionAdicional } from '../pages/exhibicion-adicional/exhibicion-adicional';
import { MenuAcompanamientoPage } from '../pages/menu-acompanamiento/menu-acompanamiento';
import { VisitaMedicaPage } from '../pages/visita-medica/visita-medica';
import { CalificarEventoPage } from '../pages/calificar-evento/calificar-evento';
import { RankingPage } from '../pages/ranking/ranking';
import { SettingsPage, ModalDescarga2 } from '../pages/settings/settings';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { CamaraProvider } from '../providers/camara/camara';
import { Transfer } from '@ionic-native/transfer';
import { HTTP } from '@ionic-native/http';
import { SQLite } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { DatabaseServiceProvider } from '../providers/database-service/database-service';

import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    LoginPage,
    PublicacionesPage,
    MapaPage,
    MenuEvaluarPunto,
    NovedadesPage,
    NovedadesResponderPage,
    ExhibicionPage,
    CerrarCasoModal,
    ExhibicionFormulasPage,
    ExhibicionComplementosPage,
    ExhibicionPedialytePage,
    ExhibicionAdicionalPage,
    CerrarCasoModalExhibicionAdicional,
    MenuAcompanamientoPage,
    VisitaMedicaPage,
    CalificarEventoPage,
    RankingPage,
    SettingsPage,
    ModalDescarga2
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    LoginPage,
    PublicacionesPage,
    MapaPage,
    MenuEvaluarPunto,
    NovedadesPage,
    NovedadesResponderPage,
    ExhibicionPage,
    CerrarCasoModal,
    ExhibicionFormulasPage,
    ExhibicionComplementosPage,
    ExhibicionPedialytePage,
    ExhibicionAdicionalPage,
    CerrarCasoModalExhibicionAdicional,
    MenuAcompanamientoPage,
    VisitaMedicaPage,
    CalificarEventoPage,
    RankingPage,
    SettingsPage,
    ModalDescarga2
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    CamaraProvider,
    Transfer,
    HTTP,
    SQLite,
    Network,
    DatabaseServiceProvider,
    File,
    FileTransfer,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
