var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage, ModalDescarga } from '../pages/login/login';
import { PublicacionesPage } from '../pages/publicaciones/publicaciones';
import { MapaPage } from '../pages/mapa/mapa';
import { MenuEvaluarPunto } from '../pages/menu-evaluar-punto/menu-evaluar-punto';
import { NovedadesPage } from '../pages/novedades/novedades';
import { NovedadesResponderPage } from '../pages/novedades-responder/novedades-responder';
import { ExhibicionPage } from '../pages/exhibicion/exhibicion';
import { AgotadosPage } from '../pages/agotados/agotados';
import { PrecioPage, CerrarCasoModal } from '../pages/precio/precio';
import { ExhibicionAdicionalPage, CerrarCasoModalExhibicionAdicional } from '../pages/exhibicion-adicional/exhibicion-adicional';
import { MenuAcompanamientoPage } from '../pages/menu-acompanamiento/menu-acompanamiento';
import { VisitaMedicaPage } from '../pages/visita-medica/visita-medica';
import { CalificarEventoPage } from '../pages/calificar-evento/calificar-evento';
import { RankingPage } from '../pages/ranking/ranking';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { CamaraProvider } from '../providers/camara/camara';
import { Transfer } from '@ionic-native/transfer';
import { HTTP } from '@ionic-native/http';
import { SQLite } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { DatabaseServiceProvider } from '../providers/database-service/database-service';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            MyApp,
            TabsPage,
            LoginPage,
            ModalDescarga,
            PublicacionesPage,
            MapaPage,
            MenuEvaluarPunto,
            NovedadesPage,
            NovedadesResponderPage,
            ExhibicionPage,
            AgotadosPage,
            PrecioPage,
            CerrarCasoModal,
            ExhibicionAdicionalPage,
            CerrarCasoModalExhibicionAdicional,
            MenuAcompanamientoPage,
            VisitaMedicaPage,
            CalificarEventoPage,
            RankingPage
        ],
        imports: [
            BrowserModule,
            IonicModule.forRoot(MyApp),
            IonicStorageModule.forRoot()
        ],
        bootstrap: [IonicApp],
        entryComponents: [
            MyApp,
            TabsPage,
            LoginPage,
            ModalDescarga,
            PublicacionesPage,
            MapaPage,
            MenuEvaluarPunto,
            NovedadesPage,
            NovedadesResponderPage,
            ExhibicionPage,
            AgotadosPage,
            PrecioPage,
            CerrarCasoModal,
            ExhibicionAdicionalPage,
            CerrarCasoModalExhibicionAdicional,
            MenuAcompanamientoPage,
            VisitaMedicaPage,
            CalificarEventoPage,
            RankingPage
        ],
        providers: [
            StatusBar,
            SplashScreen,
            Geolocation,
            Camera,
            CamaraProvider,
            Transfer,
            HTTP,
            SQLite,
            Network,
            DatabaseServiceProvider,
            File,
            FileTransfer,
            { provide: ErrorHandler, useClass: IonicErrorHandler }
        ]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map