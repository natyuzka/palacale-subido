var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Camera } from '@ionic-native/camera';
import { Platform } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
var CamaraProvider = (function () {
    function CamaraProvider(camera, platform, http) {
        this.camera = camera;
        this.platform = platform;
        this.http = http;
    }
    CamaraProvider.prototype.getPictureFromCamera = function (foto_perfil) {
        if (foto_perfil === void 0) { foto_perfil = ""; }
        return this.getImage(this.camera.PictureSourceType.CAMERA, foto_perfil);
    };
    CamaraProvider.prototype.getPictureFromPhotoLibrary = function (foto_perfil) {
        if (foto_perfil === void 0) { foto_perfil = ""; }
        return this.getImage(this.camera.PictureSourceType.PHOTOLIBRARY, foto_perfil);
    };
    CamaraProvider.prototype.getImage = function (pictureSourceType, foto_perfil, crop, quality, allowEdit, saveToAlbum) {
        if (crop === void 0) { crop = true; }
        if (quality === void 0) { quality = 1; }
        if (allowEdit === void 0) { allowEdit = true; }
        if (saveToAlbum === void 0) { saveToAlbum = false; }
        var options = {
            quality: 0,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: pictureSourceType,
            allowEdit: allowEdit,
            encodingType: this.camera.EncodingType.PNG,
            saveToPhotoAlbum: saveToAlbum
        };
        if (foto_perfil == 1) {
            options["targetWidth"] = 100;
        }
        else {
            if (crop) {
                options["targetWidth"] = 400;
                if (this.platform.is('ios')) {
                    options["targetHeight"] = 400;
                }
            }
        }
        return this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/png;base64,' + imageData;
            return [base64Image, imageData];
        }, function (error) {
            console.log("CAMERA ERROR -> " + JSON.stringify(error));
            return error;
        });
    };
    CamaraProvider.prototype.cargarFotoServidor = function (imagen, id, carpeta) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post('http://palacalle.abbottdata.com/backend/v0/cargar-imagen.php', { imagen: imagen, id: id, carpeta: carpeta }, {})
                .then(function (data) {
                resolve("1");
            })
                .catch(function (error) {
                alert("¡Ha ocurrido un error al cargar la imágen!");
                console.log(error.status);
                console.log(error.error);
                reject(error);
            });
        });
    };
    return CamaraProvider;
}());
CamaraProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Camera, Platform, HTTP])
], CamaraProvider);
export { CamaraProvider };
//# sourceMappingURL=camara.js.map