import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Camera } from '@ionic-native/camera';

import { Platform } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';

@Injectable()
export class CamaraProvider {

	constructor(private camera: Camera, private platform: Platform, private http: HTTP) {}

	getPictureFromCamera(foto_perfil = "") {
		return this.getImage(this.camera.PictureSourceType.CAMERA, foto_perfil);
	}

	getPictureFromPhotoLibrary(foto_perfil = "") {
		return this.getImage(this.camera.PictureSourceType.PHOTOLIBRARY, foto_perfil);
	}

	getImage(pictureSourceType, foto_perfil, crop = true,  allowEdit = true, saveToAlbum = false) {
		
		let options = {
			quality: 100,
			destinationType: this.camera.DestinationType.DATA_URL,
			sourceType: pictureSourceType,
			allowEdit: allowEdit,
			encodingType: this.camera.EncodingType.PNG,
			saveToPhotoAlbum: saveToAlbum
		};

		if(foto_perfil == 1){
			options["targetWidth"] = 70;
		}else{
			if (crop) {
				options["targetWidth"] = 350;
				if(this.platform.is('ios')){
					options["targetHeight"] = 350;
				}
				if(this.platform.is('android')){
					options["targetHeight"] = 350;
				}
			}
		}

		return this.camera.getPicture(options).then(imageData => {
			let base64Image = 'data:image/png;base64,' + imageData;
			return [base64Image, imageData];
		}, error => {
			console.log("CAMERA ERROR -> " + JSON.stringify(error));
			return error;
		});

	}

	//Sube la foto al servidor en la carpte e id que llegaron
	cargarFotoServidor(imagen, id, carpeta){
		return new Promise((resolve, reject) => {
			this.http.post('http://palacalle.abbottdata.com/backend/v1/cargar-imagen.php', { imagen : imagen, id : id, carpeta : carpeta}, {})
				.then(data => {
		             resolve("1");
				})
				.catch(error => {
					alert("¡Ha ocurrido un error al cargar la imágen!");
					console.log(error.status);
					console.log(error.error);
					reject(error);
				});
		});
	}

}