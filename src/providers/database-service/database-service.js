var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import { SQLite } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
var DatabaseServiceProvider = (function () {
    function DatabaseServiceProvider(sqlite, platform, http, network, file, transfer, storageLocal) {
        var _this = this;
        this.sqlite = sqlite;
        this.http = http;
        this.network = network;
        this.file = file;
        this.transfer = transfer;
        this.storageLocal = storageLocal;
        this.conexion = false;
        this.abortarDescarga = 0;
        platform.ready().then(function () {
            _this.sqlite.create({
                name: 'pa-la-calle.db',
                location: 'default'
            })
                .then(function (db) {
                _this.storage = db;
                console.log("Se abrio correctamente");
                _this.storage.executeSql('SELECT usuario FROM usuarios LIMIT 1;', {})
                    .then(function () {
                    console.log("Si existe la base de datos");
                    _this.verPuntos();
                    // this.eliminarInfoLocal()
                    // .then(() => alert("Eliminada"))
                    // .catch(() => alert("NO Eliminada"));
                })
                    .catch(function (error) {
                    console.log("No existen tablas: " + JSON.stringify(error));
                    _this.crearEstructuraBaseDatos();
                });
            })
                .catch(function (error) { return console.log("Error crear base de datos: " + JSON.stringify(error)); });
        });
        if (platform.is('ios')) {
            this.externalDirectory = cordova.file.dataDirectory;
        }
        else if (platform.is('android')) {
            this.externalDirectory = cordova.file.externalRootDirectory;
        }
    }
    DatabaseServiceProvider.prototype.formatoNumero = function (numero) {
        return numero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    };
    DatabaseServiceProvider.prototype.crearEstructuraBaseDatos = function () {
        var estructura = [
            "CREATE TABLE IF NOT EXISTS usuarios(\n\t\t\t   id_u INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   usuario  varchar(50),\n\t\t\t   password varchar(50),\n\t\t\t   nombre varchar(50),\n\t\t\t   cargo varchar(70),\n\t\t\t   correo varchar(50),\n\t\t\t   tipo char(1),\n\t\t\t   ciudad int,\n\t\t\t   foto varchar(3)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS puntos(\n\t\t\t   id_p INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre  varchar(70),\n\t\t\t   tipo int,\n\t\t\t   direccion varchar(200),\n\t\t\t   numero int,\n\t\t\t   canal varchar(50),\n\t\t\t   latitud varchar(20), \n\t\t\t   longitud varchar(20), \n\t\t\t   foto varchar(3), \n\t\t\t   ciudad varchar(45), \n\t\t\t   zona varchar(25), \n\t\t\t   rv varchar(15), \n\t\t\t   cadena varchar(45), \n\t\t\t   nombre_fantasia varchar(70), \n\t\t\t   distribuidor varchar(45), \n\t\t\t   telefono varchar(45), \n\t\t\t   supervisor varchar(45)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS usuarios_puntos(\n\t\t\t   id_up INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   usuario int,\n\t\t\t   punto int,\n\t\t\t   sector int,\n\t\t\t   orden int,\n\t\t\t   fecha datetime\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS tipospunto(\n\t\t\t   id_t INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(60),\n\t\t\t   foto varchar(3)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS productos(\n\t\t\t   id_p INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   referencia  varchar(150),\n\t\t\t   precio_min int,\n\t\t\t   precio_max int,\n\t\t\t   foto varchar(3)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS productos_cartilla(\n\t\t\t   id_p INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(50),\n\t\t\t   descripcion text,\n\t\t\t   descripcion2 text,\n\t\t\t   imagen varchar(5)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS tipospunto_productos(\n\t\t\t   id_tp INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   tipopunto int,\n\t\t\t   producto int,\n\t\t\t   infaltable tinyint(1)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS exhibicionesadicionales(\n\t\t\t   id_e INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(70),\n\t\t\t   foto varchar(3)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS preguntas(\n\t\t\t   id_p INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   pregunta varchar(255),\n\t\t\t   tipo varchar(2),\n\t\t\t   titulo varchar(40)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS respuestasvisitamedica(\n\t\t\t   id_r INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   usuario int,\n\t\t\t   fuerza int,\n\t\t\t   especialidad int,\n\t\t\t   visitador int,\n\t\t\t   comentario varchar(255)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS respuestaspreguntasvisita(\n\t\t\t   id_r INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   respuestavisitamedica int,\n\t\t\t   pregunta int,\n\t\t\t   respuesta int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS publicaciones(\n\t\t\t   id_p INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   usuario int,\n\t\t\t   punto int,\n\t\t\t   tipo char(1),\n\t\t\t   comentario varchar(255),\n\t\t\t   foto varchar(3),\n\t\t\t   fecha datetime,\n\t\t\t   comentario_agotados varchar(255),\n\t\t\t   respuesta varchar(255),\n\t\t\t   evento_nombre varchar(70),\n\t\t\t   evento_ciudad varchar(50)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS calificacionexhibicion(\n\t\t\t   id_c INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   publicacion int,\n\t\t\t   r1 tinyint(1),\n\t\t\t   r2 tinyint(1),\n\t\t\t   r3 tinyint(1),\n\t\t\t   r4 tinyint(1),\n\t\t\t   r5 tinyint(1)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS calificacionagotados(\n\t\t\t   id_c INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   publicacion int,\n\t\t\t   producto int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS calificacionprecios(\n\t\t\t   id_c INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   publicacion int,\n\t\t\t   producto int,\n\t\t\t   precio int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS calificacionexhibicionadicional(\n\t\t\t   id_c INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   publicacion int,\n\t\t\t   exhibicionadicional int,\n\t\t\t   foto varchar(3)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS respuestasexhibicionadicional(\n\t\t\t   id_r INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   calificacionexhibicion int,\n\t\t\t   pregunta int,\n\t\t\t   respuesta int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS visitadores(\n\t\t\t   id_v INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(50)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS especialidades(\n\t\t\t   id_e INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(50),\n\t\t\t   fuerza int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS visitadores_especialidades(\n\t\t\t   id_ve INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   visitador int,\n\t\t\t   especialidad int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS fuerzas(\n\t\t\t   id_f INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(50)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS especialidades_productos(\n\t\t\t   id_ep INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   especialidad int,\n\t\t\t   producto int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS eventos(\n\t\t\t   id_e INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   ciudad  varchar(50),\n\t\t\t   nombre  varchar(70),\n\t\t\t   asistentes  int,\n\t\t\t   encargado varchar(50)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS calificacioneseventos(\n\t\t\t   id_c INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   usuario  int,\n\t\t\t   evento  int,\n\t\t\t   publicacion int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS respuestaseventos(\n\t\t\t   id_r INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   calificacioneseventos  int,\n\t\t\t   pregunta  int,\n\t\t\t   respuesta tinyint(1)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS comentarios(\n\t\t\t   id_c INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   publicacion  int,\n\t\t\t   usuario  int,\n\t\t\t   usuario_nombre varchar(50),\n\t\t\t   usuario_foto varchar(3),\n\t\t\t   fecha datetime,\n\t\t\t   comentario varchar(255)\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS likes(\n\t\t\t   id_l INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   publicacion  int,\n\t\t\t   usuario int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS puntos_exhibicionesadicionales(\n\t\t\t   id_pe INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   punto int,\n\t\t\t   exhibicionadicional int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS usuarios_visitadores(\n\t\t\t   id_uv INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   usuario int,\n\t\t\t   visitador int,\n\t\t\t   orden int,\n\t\t\t   fecha datetime\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS ciudades(\n\t\t\t   id_c INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(50),\n\t\t\t   grupo int\n\t\t\t)",
            "CREATE TABLE IF NOT EXISTS ciudades_grupos(\n\t\t\t   id_g INTEGER PRIMARY KEY AUTOINCREMENT,\n\t\t\t   nombre varchar(50)\n\t\t\t)"
        ];
        this.ejecutarBatch(estructura)
            .then(function (result) {
            console.log("ejecutarBatch estructura bn");
        })
            .catch(function (error) { });
    };
    DatabaseServiceProvider.prototype.crearCarpeta = function (lugar, carpeta) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.file.createDir(lugar, carpeta, false)
                .then(function () {
                resolve(true);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.borrarCarpeta = function (lugar, carpeta) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.file.removeRecursively(lugar, carpeta)
                .then(function () {
                _this.crearCarpeta(lugar, carpeta).then(function () {
                    resolve(true);
                }).catch(function (error) {
                    reject(error);
                });
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.validarCarpeta = function (lugar, carpeta) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.file.checkDir(lugar, carpeta)
                .then(function () {
                _this.borrarCarpeta(lugar, carpeta).then(function () {
                    resolve(true);
                }).catch(function (error) {
                    reject(error);
                });
            })
                .catch(function () {
                _this.crearCarpeta(lugar, carpeta).then(function () {
                    resolve(true);
                }).catch(function (error) {
                    reject(error);
                });
            });
        });
    };
    DatabaseServiceProvider.prototype.validandoCarpetas = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var dataDirectory = cordova.file.dataDirectory;
            _this.validarCarpeta(dataDirectory, "tipospunto").then(function () {
                _this.validarCarpeta(dataDirectory, "productos").then(function () {
                    _this.validarCarpeta(dataDirectory, "exhibiciones-adicionales")
                        .then(function () {
                        resolve(true);
                    })
                        .catch(function (error) {
                        reject(error);
                    }); // exhibiciones
                }); // productos
            }); // tipospunto
        }); // return Promise
    };
    DatabaseServiceProvider.prototype.cancelarDescarga = function () {
        this.abortarDescarga = 1;
    };
    DatabaseServiceProvider.prototype.descargarArchivos = function (archivos, urlDescarga, destinoApp, carpeta) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fileTransfer = _this.transfer.create();
            var archivos_descargados = 0;
            var archivos_por_descargar = archivos.length;
            var _error = 0;
            var progreso = 0;
            console.log("archivos " + carpeta + ": " + archivos);
            var _loop_1 = function (archivo) {
                console.log("archivo: " + archivo);
                fileTransfer.onProgress(function (progressEvent) {
                    if (progressEvent.lengthComputable) {
                        var progress = Math.round((progressEvent.loaded / progressEvent.total) * 100);
                        if (progress > progreso) {
                            progreso = progress;
                            document.getElementById("progreso-actualizacion").value = String(progreso);
                        }
                    }
                });
                fileTransfer.download("" + urlDescarga + archivo, "" + destinoApp + carpeta + archivo)
                    .then(function (entry) {
                    console.log("Bn archivo: " + archivo);
                    progreso = 0;
                    archivos_descargados++;
                    if (archivos_descargados == 20) {
                        fileTransfer.abort();
                        resolve(true);
                    }
                    else if (archivos_descargados == archivos_por_descargar) {
                        resolve(true);
                    }
                })
                    .catch(function (error) {
                    console.log("Mal archivo: " + archivo);
                    if (error.http_status >= 500) {
                        alert("¡Error en el servidor! COD-253 [" + carpeta + archivo + "]");
                    }
                    else if (error.http_status >= 400) {
                        alert("¡Error en el servidor! COD-252 [" + carpeta + archivo + "]");
                    }
                    else {
                        console.log("error: " + JSON.stringify(error));
                        alert("¡No tienes conexión de internet! COD-251");
                    }
                    _error = 1;
                    reject(error);
                });
                if ((_this.abortarDescarga == 1) || (_error == 1)) {
                    fileTransfer.abort();
                    alert("¡Actualización Cancelada!");
                    return "break";
                }
            };
            for (var _i = 0, archivos_1 = archivos; _i < archivos_1.length; _i++) {
                var archivo = archivos_1[_i];
                var state_1 = _loop_1(archivo);
                if (state_1 === "break")
                    break;
            }
        });
    };
    DatabaseServiceProvider.prototype.administradorDescarga = function (archivos, urlDescarga, destinoApp, carpeta, loop_total, loop) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (loop_total == loop) {
                resolve(true);
            }
            else {
                _this.descargarArchivos(archivos[loop], urlDescarga, destinoApp, carpeta)
                    .then(function () {
                    loop++;
                    _this.administradorDescarga(archivos, urlDescarga, destinoApp, carpeta, loop_total, loop)
                        .then(function () {
                        resolve(true);
                    })
                        .catch(function (error) {
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    reject(error);
                });
            }
        });
    };
    DatabaseServiceProvider.prototype.dividirArray = function (array, posiciones) {
        var i, tamaño = Math.ceil((array.length) / 10) * 10, aux = [];
        for (i = 0; i < tamaño; i += posiciones) {
            aux.push(array.slice(i, i + posiciones));
        }
        return aux;
    };
    DatabaseServiceProvider.prototype.procesoActualizacion = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            data.data = JSON.parse(data.data);
            var datos = [];
            var tupla = [];
            var imagenes_tipopunto = [];
            var imagenes_productos = [];
            var imagenes_exhibiciones = [];
            datos.push(['DELETE FROM ciudades', []]);
            datos.push(['DELETE FROM ciudades_grupos', []]);
            datos.push(['DELETE FROM especialidades', []]);
            datos.push(['DELETE FROM especialidades_productos', []]);
            datos.push(['DELETE FROM eventos', []]);
            datos.push(['DELETE FROM exhibicionesadicionales', []]);
            datos.push(['DELETE FROM productos', []]);
            datos.push(['DELETE FROM productos_cartilla', []]);
            datos.push(['DELETE FROM puntos', []]);
            datos.push(['DELETE FROM puntos_exhibicionesadicionales', []]);
            datos.push(['DELETE FROM fuerzas', []]);
            datos.push(['DELETE FROM tipospunto', []]);
            datos.push(['DELETE FROM tipospunto_productos', []]);
            datos.push(['DELETE FROM usuarios', []]);
            datos.push(['DELETE FROM usuarios_puntos', []]);
            datos.push(['DELETE FROM visitadores', []]);
            datos.push(['DELETE FROM visitadores_especialidades', []]);
            var sql = {
                ciudades: 'INSERT INTO ciudades VALUES (?,?,?)',
                ciudades_grupos: 'INSERT INTO ciudades_grupos VALUES (?,?)',
                especialidades: 'INSERT INTO especialidades VALUES (?,?,?)',
                especialidades_productos: 'INSERT INTO especialidades_productos VALUES (?,?,?)',
                eventos: 'INSERT INTO eventos VALUES (?,?,?,?,?)',
                exhibicionesadicionales: 'INSERT INTO exhibicionesadicionales VALUES (?,?,?)',
                fuerzas: 'INSERT INTO fuerzas VALUES (?,?)',
                productos: 'INSERT INTO productos VALUES (?,?,?,?,?)',
                productos_cartilla: 'INSERT INTO productos_cartilla VALUES (?,?,?,?,?)',
                puntos: 'INSERT INTO puntos VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                puntos_exhibicionesadicionales: 'INSERT INTO puntos_exhibicionesadicionales VALUES (?,?,?)',
                tipospunto: 'INSERT INTO tipospunto VALUES (?,?,?)',
                tipospunto_productos: 'INSERT INTO tipospunto_productos VALUES (?,?,?,?)',
                usuarios: 'INSERT INTO usuarios VALUES (?,?,?,?,?,?,?,?,?)',
                usuarios_puntos: 'INSERT INTO usuarios_puntos VALUES (?,?,?,?,?,?)',
                visitadores: 'INSERT INTO visitadores VALUES (?,?)',
                visitadores_especialidades: 'INSERT INTO visitadores_especialidades VALUES (?,?,?)'
            };
            /*   ------------- Creando sql Actualizacion  */
            for (var indice in data.data[0]) {
                if (indice == "tipospunto") {
                    for (var _i = 0, _a = data.data[0][indice]; _i < _a.length; _i++) {
                        var fila = _a[_i];
                        tupla = [];
                        tupla.push(sql[indice], fila);
                        datos.push(tupla);
                        imagenes_tipopunto.push(fila[0] + "." + fila[2]);
                    }
                }
                else if (indice == "productos") {
                    for (var _b = 0, _c = data.data[0][indice]; _b < _c.length; _b++) {
                        var fila = _c[_b];
                        tupla = [];
                        tupla.push(sql[indice], fila);
                        datos.push(tupla);
                        imagenes_productos.push(fila[0] + "." + fila[4]);
                    }
                }
                else if (indice == "exhibicionesadicionales") {
                    for (var _d = 0, _e = data.data[0][indice]; _d < _e.length; _d++) {
                        var fila = _e[_d];
                        tupla = [];
                        tupla.push(sql[indice], fila);
                        datos.push(tupla);
                        imagenes_exhibiciones.push(fila[0] + "." + fila[2]);
                    }
                }
                else
                    for (var _f = 0, _g = data.data[0][indice]; _f < _g.length; _f++) {
                        var fila = _g[_f];
                        tupla = [];
                        tupla.push(sql[indice], fila);
                        datos.push(tupla);
                    }
            }
            // console.log("datos: " + JSON.stringify(datos));
            // console.log("imagenes_tipopunto: " + JSON.stringify(imagenes_tipopunto));
            // console.log("imagenes_productos: " + JSON.stringify(imagenes_productos));
            // console.log("imagenes_exhibiciones: " + JSON.stringify(imagenes_exhibiciones));
            /*   ------------- Acualizando la base de datos  */
            _this.ejecutarBatch(datos)
                .then(function (result) {
                console.log("ejecutarBatch actualizarApp bn");
                /*   ------------- Transfiriendo archivos al dispositivo  */
                _this.descargaArchivos(imagenes_tipopunto, imagenes_productos, imagenes_exhibiciones)
                    .then(function () {
                    resolve(true);
                })
                    .catch(function (error) { return reject(error); });
            })
                .catch(function (error) { return reject(error); });
        }); // end return promise
    };
    DatabaseServiceProvider.prototype.descargaArchivos = function (imagenes_tipopunto, imagenes_productos, imagenes_exhibiciones) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url_imagenes_tipopunto = 'http://palacalle.abbottdata.com/imagenes/tipospunto/';
            var url_imagenes_productos = 'http://palacalle.abbottdata.com/imagenes/productos/';
            var url_imagenes_exhibiciones = 'http://palacalle.abbottdata.com/imagenes/exhibiciones-adicionales/';
            var dataDirectory = cordova.file.dataDirectory;
            var divisiones_array = 0;
            /* Descargando archivos*/
            _this.abortarDescarga = 0;
            document.getElementById("text-overlay").innerHTML = "Descargando...";
            divisiones_array = Math.ceil((imagenes_tipopunto.length) / 10);
            _this.administradorDescarga(_this.dividirArray(imagenes_tipopunto, 10), url_imagenes_tipopunto, dataDirectory, "tipospunto/", divisiones_array, 0)
                .then(function () {
                document.getElementById("text-overlay").innerHTML = "Descargando Productos";
                divisiones_array = Math.ceil((imagenes_productos.length) / 10);
                _this.administradorDescarga(_this.dividirArray(imagenes_productos, 10), url_imagenes_productos, dataDirectory, "productos/", divisiones_array, 0)
                    .then(function () {
                    document.getElementById("text-overlay").innerHTML = "Descargando Exhibiciones Adicionales";
                    divisiones_array = Math.ceil((imagenes_exhibiciones.length) / 10);
                    _this.administradorDescarga(_this.dividirArray(imagenes_exhibiciones, 10), url_imagenes_exhibiciones, dataDirectory, "exhibiciones-adicionales/", divisiones_array, 0)
                        .then(function () {
                        alert("¡Actualización completa!, Debe reiniciar la aplicación");
                        resolve(true);
                    })
                        .catch(function (error) {
                        reject(error);
                    }); /* exhibiciones adicionales*/
                })
                    .catch(function (error) {
                    reject(error);
                }); /* productos*/
            })
                .catch(function (error) {
                reject(error);
            }); /* tipospunto*/
        });
    };
    DatabaseServiceProvider.prototype.actualizarApp = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('http://palacalle.abbottdata.com/backend/v1/actualizar-app/', {}, {})
                .then(function (data) {
                _this.validandoCarpetas()
                    .then(function () {
                    _this.procesoActualizacion(data)
                        .then(function () {
                        resolve(true);
                    })
                        .catch(function (error) {
                        alert("¡Error al actualizar la información!");
                        console.log("¡Error al actualizar la información! " + JSON.stringify(error));
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    alert("¡Error al actualizar la información! COD-24");
                    console.log("¡Error al actualizar la información! COD-24" + JSON.stringify(error));
                    reject(error);
                });
            })
                .catch(function (error) {
                if (error.status >= 500) {
                    alert("¡Error, el servidor no responde!");
                }
                else if (error.status >= 400) {
                    alert("¡Error, no se encuentra el servidor!");
                }
                else {
                    alert("¡No tienes conexión de internet o tu red es muy lenta!");
                }
                console.log(error.status);
                console.log(error.error);
                console.log(error.headers);
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.ejecutarBatch = function (datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.sqlBatch(datos)
                .then(function () {
                resolve(true);
            })
                .catch(function (error) {
                alert("¡Error en el sistema! COD-211 " + JSON.stringify(error.message));
                console.log("ERROR ejecutarBatch: " + JSON.stringify(error.message));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.probarConexion = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.network.type === 'none') {
                reject(1);
            }
            else
                resolve(1);
        });
    };
    DatabaseServiceProvider.prototype.validarUsuario = function (datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT id_u, nombre, foto, tipo FROM usuarios WHERE usuario = ? AND password = ? LIMIT 1", datos)
                .then(function (data) {
                var res = {};
                if (data.rows.length > 0) {
                    for (var i = 0; i < data.rows.length; i++) {
                        res = {
                            id_u: data.rows.item(i).id_u,
                            nombre: data.rows.item(i).nombre,
                            foto: data.rows.item(i).foto,
                            tipo: data.rows.item(i).tipo
                        };
                    }
                    _this.storageLocal.set('usuario', res);
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta datos base de datos: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.sprintf = function (numero) {
        if (String(numero).length == 1) {
            numero = "0" + String(numero);
        }
        return numero;
    };
    DatabaseServiceProvider.prototype.fecha = function () {
        var dt = new Date();
        var time_offset = Math.floor(dt.getTimezoneOffset() / 60);
        if (time_offset == 0) {
            time_offset = 5;
        }
        var hora = dt.getHours() - time_offset;
        var dia = dt.getDate();
        if (hora <= 0) {
            hora = 24 - hora;
            dia = dia - 1;
        }
        var mes = this.sprintf(dt.getMonth() + 1);
        dia = this.sprintf(dia);
        hora = this.sprintf(hora);
        var minutos = this.sprintf(dt.getMinutes());
        var segundos = this.sprintf(dt.getSeconds());
        return dt.getFullYear() + "-" + mes + "-" + dia + " " + hora + ":" + minutos + ":" + segundos;
    };
    DatabaseServiceProvider.prototype.consultaOrden = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT MAX(orden) AS orden FROM usuarios_puntos;", [])
                .then(function (data) {
                var orden = 0;
                if (data.rows.length > 0) {
                    orden = data.rows.item(0).orden;
                }
                resolve(orden);
            })
                .catch(function (error) {
                alert("Error consulta consultaOrden: " + JSON.stringify(error));
                reject(0);
            });
        });
    };
    DatabaseServiceProvider.prototype.consultaCiudadesUsuarios = function (usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT ciudad FROM usuarios WHERE id_u = ?;", [usuario])
                .then(function (data) {
                var ciudad = "";
                if (data.rows.length > 0) {
                    ciudad = data.rows.item(0).ciudad;
                }
                resolve(ciudad);
            })
                .catch(function (error) {
                alert("Error consulta consultaCiudadesUsuarios: " + JSON.stringify(error));
                reject(0);
            });
        });
    };
    DatabaseServiceProvider.prototype.loadMapMisPuntosVisitados = function (usuario, mispuntos, res) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fecha = _this.fecha();
            _this.storage.executeSql("SELECT punto\n\t\t\t\tFROM publicaciones\n\t\t\t\tWHERE fecha > DATETIME(?,'-3 day') AND usuario = ? AND tipo = 'p';", [fecha, usuario])
                .then(function (data) {
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        for (var _i = 0, mispuntos_1 = mispuntos; _i < mispuntos_1.length; _i++) {
                            var punto = mispuntos_1[_i];
                            if (data.rows.item(i).punto == punto.id_p) {
                                punto.estado = 2;
                                break;
                            }
                        }
                    }
                }
                mispuntos.unshift({
                    id_p: "",
                    nombre: "SELECCIONE",
                    foto: "",
                    latitud: "",
                    longitud: "",
                    tipo_nombre: "",
                    usuario: "",
                    estado: ""
                });
                resolve(mispuntos.concat(res));
            })
                .catch(function (error) {
                alert("Error consulta loadMapMisPuntosVisitados: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.loadMapMisPuntos = function (usuario, orden, res) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT p.id_p, p.nombre, tp.foto, p.latitud, p.longitud, tp.nombre AS tipo_nombre, up.usuario, p.direccion, tp.id_t\n\t\t\t\tFROM puntos p \n\t\t\t\tINNER JOIN tipospunto tp ON p.tipo = tp.id_t \n\t\t\t\tLEFT JOIN usuarios_puntos up ON up.punto = p.id_p\n\t\t\t\tWHERE up.usuario = ? AND up.orden = ?", [usuario, orden])
                .then(function (data) {
                var mispuntos = [];
                var tamaño = data.rows.length;
                var estado;
                if (tamaño > 0) {
                    mispuntos.unshift({
                        id_p: "",
                        nombre: "---------------- FIN ----------------",
                        foto: "",
                        latitud: "",
                        longitud: "",
                        tipo_nombre: "",
                        usuario: "",
                        estado: ""
                    });
                    mispuntos.unshift({
                        id_p: "",
                        nombre: "-------- MIS PUNTOS --------",
                        foto: "",
                        latitud: "",
                        longitud: "",
                        tipo_nombre: "",
                        usuario: "",
                        estado: ""
                    });
                    for (var i = 0; i < tamaño; i++) {
                        if (data.rows.item(i).usuario == usuario) {
                            estado = 1;
                        }
                        else
                            estado = 0;
                        mispuntos.unshift({
                            id_p: data.rows.item(i).id_p,
                            nombre: data.rows.item(i).nombre,
                            foto: data.rows.item(i).id_t + "." + data.rows.item(i).foto,
                            latitud: data.rows.item(i).latitud,
                            longitud: data.rows.item(i).longitud,
                            tipo_nombre: data.rows.item(i).tipo_nombre,
                            tipo_id: data.rows.item(i).id_t,
                            usuario: data.rows.item(i).usuario,
                            direccion: data.rows.item(i).direccion,
                            estado: estado
                        });
                    }
                    mispuntos.unshift({
                        id_p: "",
                        nombre: "-------- MIS PUNTOS --------",
                        foto: "",
                        latitud: "",
                        longitud: "",
                        tipo_nombre: "",
                        usuario: "",
                        estado: ""
                    });
                }
                _this.loadMapMisPuntosVisitados(usuario, mispuntos, res)
                    .then(function (data) {
                    resolve(data);
                })
                    .catch(function (error) {
                    reject(error);
                });
            })
                .catch(function (error) {
                alert("Error loadMapMisPuntos: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.loadMapPuntos = function (orden, ciudad, usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT p.id_p, p.nombre, tp.foto, p.latitud, p.longitud, tp.nombre AS tipo_nombre, up.usuario, p.direccion, tp.id_t\n\t\t\t\t\tFROM puntos p \n\t\t\t\t\tLEFT JOIN tipospunto tp ON p.tipo = tp.id_t \n\t\t\t\t\tLEFT JOIN usuarios_puntos up ON up.punto = p.id_p \n\t\t\t\t\tLEFT JOIN ciudades c ON c.id_c = p.ciudad \n\t\t\t\t\tWHERE up.orden = ? AND c.grupo = ?\n\t\t\t\t\tGROUP BY p.id_p\n\t\t\t\t\tLIMIT 90;", [orden, ciudad])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                var estado;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        if (data.rows.item(i).usuario == usuario) {
                            estado = 1;
                        }
                        else
                            estado = 0;
                        res.push({
                            id_p: data.rows.item(i).id_p,
                            nombre: data.rows.item(i).nombre,
                            foto: data.rows.item(i).id_t + "." + data.rows.item(i).foto,
                            latitud: data.rows.item(i).latitud,
                            longitud: data.rows.item(i).longitud,
                            tipo_nombre: data.rows.item(i).tipo_nombre,
                            tipo_id: data.rows.item(i).id_t,
                            usuario: data.rows.item(i).usuario,
                            direccion: data.rows.item(i).direccion,
                            estado: estado
                        });
                    }
                }
                _this.loadMapMisPuntos(usuario, orden, res)
                    .then(function (data) {
                    resolve(data);
                })
                    .catch(function (error) {
                    reject(error);
                });
            })
                .catch(function (error) {
                alert("Error consulta loadMap: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.loadMap = function (usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.consultaOrden()
                .then(function (data) {
                var orden = data;
                _this.consultaCiudadesUsuarios(usuario)
                    .then(function (data) {
                    var ciudad = data;
                    _this.loadMapPuntos(orden, ciudad, usuario)
                        .then(function (data) {
                        resolve(data);
                    })
                        .catch(function (error) {
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    console.log("Error - consultaOrden: " + JSON.stringify(error));
                    reject(error);
                });
            })
                .catch(function (error) {
                console.log("Error - consultaCiudadesUsuarios: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.puntoEstado = function (punto, usuario) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fecha = _this.fecha();
            _this.storage.executeSql("SELECT tipo\n\t\t\t\tFROM publicaciones\n\t\t\t\tWHERE fecha > DATETIME(?,'-3 day') AND usuario = ? AND punto = ?;", [fecha, usuario, punto])
                .then(function (data) {
                var res = [];
                res[0] = 0;
                res[1] = 0;
                if (data.rows.length > 0) {
                    for (var i = 0; i < data.rows.length; i++) {
                        if (data.rows.item(i).tipo == "p") {
                            res[0] = 1;
                        }
                        else if (data.rows.item(i).tipo == "n") {
                            res[1] = 1;
                        }
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta puntoEstado: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.agotados = function (tipo) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT p.id_p, p.referencia, p.foto, tpp.infaltable\n\t\t\t\tFROM  productos p \n\t\t\t\tINNER JOIN tipospunto_productos tpp ON p.id_p = tpp.producto \n\t\t\t\tINNER JOIN tipospunto tp ON tpp.tipopunto = tp.id_t \n\t\t\t\tWHERE tp.id_t = ?\n\t\t\t\tORDER BY tpp.infaltable DESC;", [tipo])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            id_p: data.rows.item(i).id_p,
                            nombre: data.rows.item(i).referencia,
                            foto: data.rows.item(i).id_p + "." + data.rows.item(i).foto,
                            infaltable: data.rows.item(i).infaltable
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta agotados: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.precio = function (tipo) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT p.id_p, p.referencia,p.foto, p.precio_min, p.precio_max \n\t\t\t\tFROM  productos p \n\t\t\t\tINNER JOIN tipospunto_productos tpp ON p.id_p = tpp.producto \n\t\t\t\tINNER JOIN tipospunto tp ON tpp.tipopunto = tp.id_t \n\t\t\t\tWHERE tp.id_t = ?\n\t\t\t\tORDER BY tpp.infaltable DESC", [tipo])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            id_p: data.rows.item(i).id_p,
                            nombre: data.rows.item(i).referencia,
                            foto: data.rows.item(i).id_p + "." + data.rows.item(i).foto,
                            precio_min: _this.formatoNumero(data.rows.item(i).precio_min),
                            precio_max: _this.formatoNumero(data.rows.item(i).precio_max),
                            precio_new: ""
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta precio: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.exhibicionesAdicionales = function (punto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT exhibicionadicional\n\t\t\t\t\t\t\t\t\tFROM puntos_exhibicionesadicionales\n\t\t\t\t\t\t\t\t\tWHERE punto = ?;", [punto])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push(data.rows.item(i).exhibicionadicional);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta exhibicionesAdicionales: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.guardarPublicacion_noInternet_ExhibicionesAdicionalesDatos = function (calificacionexhibicion, exhibicionAdicionalDatos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var continuar = 1;
            var error;
            if (exhibicionAdicionalDatos.uno > 0) {
                _this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '1', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.uno])
                    .catch(function (error) {
                    continuar = 0;
                    error = error;
                });
            }
            if (exhibicionAdicionalDatos.dos > 0) {
                _this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '2', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.dos])
                    .catch(function (error) {
                    continuar = 0;
                    error = error;
                });
            }
            if (exhibicionAdicionalDatos.tres > 0) {
                _this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '3', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.tres])
                    .catch(function (error) {
                    continuar = 0;
                    error = error;
                });
            }
            if (exhibicionAdicionalDatos.cuatro > 0) {
                _this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '4', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.cuatro])
                    .catch(function (error) {
                    continuar = 0;
                    error = error;
                });
            }
            if (continuar == 1) {
                resolve(true);
            }
            else
                reject(error);
        });
    };
    DatabaseServiceProvider.prototype.guardarPublicacion_noInternet_ExhibicionesAdicionales = function (publicacion, exhibicionAdicional, exhibicionAdicionalDatos, secuencia) {
        var _this = this;
        if (secuencia === void 0) { secuencia = 0; }
        return new Promise(function (resolve, reject) {
            var id_e;
            var cantidad_exhibiciones = exhibicionAdicional.length;
            // for(let exhibicion of exhibicionAdicionalDatos){
            id_e = exhibicionAdicional[secuencia];
            _this.storage.executeSql("INSERT INTO calificacionexhibicionadicional (publicacion, exhibicionadicional, foto) VALUES (?, ?, 'png')", [publicacion, id_e])
                .then(function () {
                var calificacionexhibicion = 0;
                _this.storage.executeSql("SELECT MAX(id_c) AS id_c FROM calificacionexhibicionadicional", [])
                    .then(function (data) {
                    calificacionexhibicion = data.rows.item(0).id_c;
                    _this.guardarPublicacion_noInternet_ExhibicionesAdicionalesDatos(calificacionexhibicion, exhibicionAdicionalDatos[secuencia])
                        .then(function () {
                        alert("secuencia: " + secuencia + ", cantidad_exhibiciones: " + cantidad_exhibiciones);
                        if ((secuencia + 1) == cantidad_exhibiciones) {
                            alert("total exhibiciones");
                            resolve(1);
                        }
                        secuencia++;
                        if (secuencia < cantidad_exhibiciones) {
                            alert("otra exhibicion");
                            _this.guardarPublicacion_noInternet_ExhibicionesAdicionales(publicacion, exhibicionAdicional, exhibicionAdicionalDatos, secuencia)
                                .then(function () {
                                alert("otra exhibicion bn");
                                resolve(1);
                            })
                                .catch(function (error) {
                                alert("otra exhibicion mal");
                                reject(error);
                            });
                        }
                    })
                        .catch(function (error) {
                        alert("Error consulta guardarPublicacion_noInternet_ExhibicionesAdicionalesDatos: " + JSON.stringify(error));
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    alert("Error consulta MAX(id_c) guardarPublicacion_noInternet_ExhibicionesAdicionales: " + JSON.stringify(error));
                    reject(error);
                });
            })
                .catch(function (error) {
                alert("Error consulta guardarPublicacion_noInternet_ExhibicionesAdicionales INSERT: " + JSON.stringify(error));
                reject(error);
            });
            // }
        });
    };
    DatabaseServiceProvider.prototype.guardarPublicacion_noInternet_Precio = function (precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var continuar = 1;
            var error;
            for (var _i = 0, precio_1 = precio; _i < precio_1.length; _i++) {
                var producto = precio_1[_i];
                if (producto.precio_new != "") {
                    _this.storage.executeSql("INSERT INTO calificacionprecios (publicacion, producto, precio) VALUES (?, ?, ?)", [publicacion, producto.id_p, producto.precio_new])
                        .catch(function (error) {
                        continuar = 0;
                        error = error;
                    });
                }
            }
            if (continuar == 1) {
                if (exhibicionAdicional != "") {
                    _this.guardarPublicacion_noInternet_ExhibicionesAdicionales(publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
                        .then(function (data) {
                        resolve(1);
                    })
                        .catch(function (error) {
                        alert("Error consulta guardarPublicacion_noInternet_ExhibicionesAdicionales: " + JSON.stringify(error));
                        reject(error);
                    });
                }
                else {
                    resolve(publicacion);
                }
            }
            else {
                alert("Error consulta guardarPublicacion_noInternet_Precio: " + JSON.stringify(error));
                reject(error);
            }
        });
    };
    DatabaseServiceProvider.prototype.guardarPublicacion_noInternet_Agotados = function (agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var productos_agotados = [];
            var producto;
            var continuar = 1;
            var error;
            for (var _i = 0, agotados_1 = agotados; _i < agotados_1.length; _i++) {
                producto = agotados_1[_i];
                if (producto != "") {
                    productos_agotados.push(producto);
                }
            }
            for (var _a = 0, productos_agotados_1 = productos_agotados; _a < productos_agotados_1.length; _a++) {
                producto = productos_agotados_1[_a];
                _this.storage.executeSql("INSERT INTO calificacionagotados (publicacion, producto) VALUES (?, ?)", [publicacion, producto])
                    .catch(function (error) {
                    continuar = 0;
                    error = error;
                });
            }
            if (continuar == 1) {
                _this.guardarPublicacion_noInternet_Precio(precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
                    .then(function (data) {
                    resolve(data);
                })
                    .catch(function (error) {
                    alert("Error consulta guardarPublicacion_noInternet_Precio: " + JSON.stringify(error));
                    reject(error);
                });
            }
            else {
                alert("Error consulta guardarPublicacion_noInternet_Agotados: " + JSON.stringify(error));
                reject(error);
            }
        });
    };
    DatabaseServiceProvider.prototype.guardarPublicacion_noInternet_Exhibicion = function (exhibicion, agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("INSERT INTO calificacionexhibicion (publicacion, r1, r2, r3, r4, r5) VALUES (?, ?, ?, ?, ?, ?);", [publicacion, exhibicion.uno, exhibicion.dos, exhibicion.tres, exhibicion.cuatro, exhibicion.cinco])
                .then(function () {
                _this.guardarPublicacion_noInternet_Agotados(agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
                    .then(function (data) {
                    resolve(data);
                })
                    .catch(function (error) {
                    alert("Error consulta guardarPublicacion_noInternet_Agotados: " + JSON.stringify(error));
                    reject(error);
                });
            })
                .catch(function (error) {
                alert("Error consulta guardarPublicacion_noInternet_Exhibicion: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.guardarPublicacion_noInternet = function (datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var exhibicion = datos.exhibicion;
            var agotados = datos.agotados;
            var precio = datos.precio;
            var fecha = _this.fecha();
            var publicacion;
            var exhibicionAdicional;
            var exhibicionAdicionalDatos;
            if (datos.hasOwnProperty("exhibicionesAdicionales")) {
                exhibicionAdicional = datos.exhibicionesAdicionales;
                exhibicionAdicionalDatos = datos.exhibicionAdicionalDatos;
            }
            else {
                exhibicionAdicional = "";
                exhibicionAdicionalDatos = "";
            }
            /*  Publicacion  */
            _this.storage.executeSql("INSERT INTO publicaciones(usuario, punto, tipo, comentario, foto, fecha) VALUES (?, ?, ?, ?, ?, ?)", [datos.usuario, datos.id_punto, 'p', exhibicion.comentario, 'png', fecha])
                .then(function (data) {
                _this.storage.executeSql("SELECT MAX(id_p) AS id_p FROM publicaciones", [])
                    .then(function (data) {
                    publicacion = data.rows.item(0).id_p;
                    _this.guardarPublicacion_noInternet_Exhibicion(exhibicion, agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
                        .then(function (data) {
                        resolve(data);
                    })
                        .catch(function (error) {
                        alert("Error consulta guardarPublicacion_noInternet_Exhibicion: " + JSON.stringify(error));
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    alert("Error consulta MAX(id_p) guardarPublicacion_noInternet: " + JSON.stringify(error));
                    reject(error);
                });
            })
                .catch(function (error) {
                alert("Error consulta 1er insert guardarPublicacion_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.fotoTipoPunto = function (tipo) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT foto FROM tipospunto WHERE id_t = ?", [tipo])
                .then(function (data) {
                resolve(data.rows.item(0).foto);
            })
                .catch(function (error) {
                alert("Error consulta fotoTipoPunto: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.exhibicionAdicionalInfo = function (punto, exhibicionAdicional) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT e.id_e, e.nombre, e.foto\n\t\tFROM puntos_exhibicionesadicionales pe\n\t\tINNER JOIN exhibicionesadicionales e ON pe.exhibicionadicional = e.id_e \n\t\tWHERE pe.punto = ? AND exhibicionadicional = ? LIMIT 1;", [punto, exhibicionAdicional])
                .then(function (data) {
                var res = {
                    id_e: "",
                    nombre: "",
                    foto: ""
                };
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res = {
                            id_e: data.rows.item(i).id_e,
                            nombre: data.rows.item(i).nombre,
                            foto: data.rows.item(i).id_e + "." + data.rows.item(i).foto
                        };
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta exhibicionAdicionalInfo: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.mostrarFuerzas_noInternet = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT id_f, nombre\n\t\t\t\tFROM fuerzas;", [])
                .then(function (data) {
                var res = [{
                        id_f: "",
                        nombre: "Fuerza"
                    }];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            id_f: data.rows.item(i).id_f,
                            nombre: data.rows.item(i).nombre
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta mostrarFuerzas_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.mostrarEspecialidades_noInternet = function (fuerza) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT id_e, nombre\n\t\t\t\tFROM especialidades \n\t\t\t\tWHERE fuerza = ?;", [fuerza])
                .then(function (data) {
                var res = [{
                        id_e: "",
                        nombre: "Especialidad"
                    }];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            id_e: data.rows.item(i).id_e,
                            nombre: data.rows.item(i).nombre
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta mostrarEspecialidades_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.mostrarVisitadores_noInternet = function (especialidad) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT v.id_v, v.nombre\n\t\t\t\tFROM visitadores v\n\t\t\t\tINNER JOIN visitadores_especialidades ve ON v.id_v = ve.visitador \n\t\t\t\tWHERE ve.especialidad = ?", [especialidad])
                .then(function (data) {
                var res = [{
                        id_v: "",
                        nombre: "Visitador"
                    }];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            id_v: data.rows.item(i).id_v,
                            nombre: data.rows.item(i).nombre
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta mostrarVisitadores_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.mostrarProductos_noInternet = function (especialidad) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT p.nombre, p.descripcion\n\t\t\t\tFROM especialidades_productos ep\n\t\t\t\tINNER JOIN productos_cartilla p ON ep.producto = p.id_p\n\t\t\t\tWHERE ep.especialidad = ?\n\t\t\t\tORDER BY ep.id_ep ASC;", [especialidad])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            nombre: data.rows.item(i).nombre,
                            descripcion: data.rows.item(i).descripcion
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta mostrarProductos_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.guardarDatosVisitaMedica_noInternet = function (respuestasvisitamedica, datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var continuar = 1;
            var error_;
            if (datos.uno > 0) {
                _this.storage.executeSql("INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)", [respuestasvisitamedica, 1, datos.uno])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.dos > 0) {
                _this.storage.executeSql("INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)", [respuestasvisitamedica, 2, datos.dos])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.tres > 0) {
                _this.storage.executeSql("INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)", [respuestasvisitamedica, 3, datos.tres])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.cuatro > 0) {
                _this.storage.executeSql("INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)", [respuestasvisitamedica, 4, datos.cuatro])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (continuar == 1) {
                resolve(true);
            }
            else
                reject(error_);
        });
    };
    DatabaseServiceProvider.prototype.guardarVisitaMedica_noInternet = function (datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("INSERT INTO respuestasvisitamedica (usuario, fuerza, especialidad, visitador, comentario) VALUES (?, ?, ?, ?, ?)", [datos.usuario, datos.fuerza, datos.especialidad, datos.visitador, datos.comentario])
                .then(function (data) {
                var respuestasvisitamedica = 0;
                _this.storage.executeSql("SELECT MAX(id_r) AS id_r FROM respuestasvisitamedica", [])
                    .then(function (data) {
                    respuestasvisitamedica = data.rows.item(0).id_r;
                    _this.guardarDatosVisitaMedica_noInternet(respuestasvisitamedica, datos)
                        .then(function () {
                        resolve(1);
                    })
                        .catch(function (error) {
                        alert("Error consulta guardarDatosVisitaMedica_noInternet: " + JSON.stringify(error));
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    alert("Error consulta MAX respuestasvisitamedica: " + JSON.stringify(error));
                });
            })
                .catch(function (error) {
                alert("Error consulta guardarVisitaMedica_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.guardarNovedad_noInternet = function (datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fecha = _this.fecha();
            _this.storage.executeSql("INSERT INTO publicaciones (usuario, punto, tipo, comentario, fecha, foto) VALUES (?, ?, ?, ?, ?, ?)", [datos.usuario, datos.punto, "n", datos.comentario, fecha, datos.foto])
                .then(function () {
                resolve(1);
            })
                .catch(function (error) {
                alert("Error consulta guardarNovedad_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.mostrarCiudadesEventos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT ciudad FROM eventos \n\t\t\t\tGROUP BY ciudad;", [])
                .then(function (data) {
                var res = [{
                        nombre: "Ciudad"
                    }];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            nombre: data.rows.item(i).ciudad
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta mostrarCiudadesEventos: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.mostrarEventos = function (ciudad) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT id_e, nombre, encargado FROM eventos \n\t\t\t\tWHERE ciudad = ?\n\t\t\t\tORDER BY nombre ASC;", [ciudad])
                .then(function (data) {
                var res = [{
                        id_e: "",
                        nombre: "Nombre del evento",
                        encargado: ""
                    }];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            id_e: data.rows.item(i).id_e,
                            nombre: data.rows.item(i).nombre,
                            encargado: data.rows.item(i).encargado
                        });
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta mostrarEventos: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.guardarDatosEvento_noInternet = function (calificacioneseventos, datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var continuar = 1;
            var error_;
            if (datos.uno > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 1, datos.uno])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.dos > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 2, datos.dos])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.tres > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 3, datos.tres])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.cuatro > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 4, datos.cuatro])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.cinco > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 5, datos.cinco])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.seis > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 6, datos.seis])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.siete > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 7, datos.siete])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (datos.ocho > 0) {
                _this.storage.executeSql("INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)", [calificacioneseventos, 8, datos.ocho])
                    .catch(function (error) {
                    continuar = 0;
                    error_ = error;
                });
            }
            if (continuar == 1) {
                resolve(true);
            }
            else
                reject(error_);
        });
    };
    DatabaseServiceProvider.prototype.guardarCalificacionEvento_noInternet = function (publicacion, datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("INSERT INTO calificacioneseventos (usuario, evento, publicacion) VALUES (?, ?, ?)", [datos.usuario, datos.evento, publicacion])
                .then(function () {
                var calificacioneseventos = 0;
                _this.storage.executeSql("SELECT MAX(id_c) AS id_c FROM calificacioneseventos", [])
                    .then(function (data) {
                    calificacioneseventos = data.rows.item(0).id_c;
                    _this.guardarDatosEvento_noInternet(calificacioneseventos, datos)
                        .then(function () {
                        resolve(1);
                    })
                        .catch(function (error) {
                        alert("Error consulta guardarDatosEvento_noInternet: " + JSON.stringify(error));
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    alert("Error consulta MAX calificacioneseventos: " + JSON.stringify(error));
                });
            })
                .catch(function (error) {
                alert("Error consulta guardarCalificacionEvento_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.guardarEvento_noInternet = function (datos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fecha = _this.fecha();
            _this.storage.executeSql("INSERT INTO publicaciones(usuario, tipo, comentario, evento_nombre, evento_ciudad, foto, fecha) VALUES (?, ?, ?, ?, ?, ?, ?)", [datos.usuario, 'e', datos.comentario, datos.nombre, datos.ciudad, 'png', fecha])
                .then(function () {
                var publicacion = 0;
                _this.storage.executeSql("SELECT MAX(id_p) AS id_p FROM publicaciones", [])
                    .then(function (data) {
                    publicacion = data.rows.item(0).id_p;
                    _this.guardarCalificacionEvento_noInternet(publicacion, datos)
                        .then(function () {
                        resolve(1);
                    })
                        .catch(function (error) {
                        alert("Error consulta guardarCalificacionEvento_noInternet: " + JSON.stringify(error));
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    alert("Error consulta MAX publicaciones: " + JSON.stringify(error));
                });
            })
                .catch(function (error) {
                alert("Error consulta guardarEvento_noInternet: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.uploadInfo = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var datos = [];
            _this.probarConexion()
                .then(function () {
                _this.publicaciones()
                    .then(function (data) {
                    datos.push(data);
                    _this.respuestasvisitamedica()
                        .then(function (data) {
                        datos.push(data);
                        _this.enviarInfo(datos)
                            .then(function () { return resolve(1); })
                            .catch(function (error) { return reject(error); });
                    }).catch(function (error) { return reject(error); });
                }).catch(function (error) { return reject(error); });
            })
                .catch(function (error) {
                alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.enviarInfo = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post('http://palacalle.abbottdata.com/backend/v1/upload/', { uploadInfo: 1, datos: data }, {})
                .then(function (data) {
                alert("enviarInfo: " + data.data);
                console.log("enviarInfo: " + data.data);
                if (data.data == 1) {
                    resolve(1);
                    // this.eliminarInfoLocal()
                    // .then(() => resolve(1))
                    // .catch(() => reject(1));
                }
                else
                    reject(JSON.parse(data.data));
            })
                .catch(function (error) {
                if (error.status >= 500) {
                    alert("¡Error, el servidor no responde!");
                }
                else if (error.status >= 400) {
                    alert("¡Error, no se encuentra el servidor!");
                }
                else {
                    alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
                }
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.eliminarInfoLocal = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var datos = [];
            datos.push(['DELETE FROM publicaciones', []]);
            datos.push(['DELETE FROM calificacionexhibicion', []]);
            datos.push(['DELETE FROM calificacionagotados', []]);
            datos.push(['DELETE FROM calificacionprecios', []]);
            datos.push(['DELETE FROM calificacionexhibicionadicional', []]);
            datos.push(['DELETE FROM respuestasexhibicionadicional', []]);
            datos.push(['DELETE FROM calificacioneseventos', []]);
            datos.push(['DELETE FROM respuestaseventos', []]);
            datos.push(['DELETE FROM respuestasvisitamedica', []]);
            datos.push(['DELETE FROM respuestaspreguntasvisita', []]);
            _this.ejecutarBatch(datos)
                .then(function (result) {
                resolve(1);
            })
                .catch(function (error) { return reject(error); });
        });
    };
    DatabaseServiceProvider.prototype.publicaciones = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT id_p, usuario, punto, tipo, comentario, foto, fecha, comentario_agotados, respuesta, evento_nombre, evento_ciudad FROM publicaciones;", [])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                var h = 1;
                alert("tamaño: " + tamaño);
                if (tamaño > 0) {
                    var _loop_2 = function (i) {
                        var calificacionexhibicion;
                        var calificacionagotados;
                        var calificacionprecios;
                        var calificacionexhibicionadicional;
                        var publicacion = data.rows.item(i).id_p;
                        var usuario = data.rows.item(i).usuario;
                        var punto = data.rows.item(i).punto;
                        var tipo = data.rows.item(i).tipo;
                        var comentario = data.rows.item(i).comentario;
                        var foto = data.rows.item(i).foto;
                        var fecha = data.rows.item(i).fecha;
                        var comentario_agotados = data.rows.item(i).comentario_agotados;
                        var respuesta = data.rows.item(i).respuesta;
                        var evento_nombre = data.rows.item(i).evento_nombre;
                        var evento_ciudad = data.rows.item(i).evento_ciudad;
                        _this.calificacionexhibicion(publicacion)
                            .then(function (data) {
                            calificacionexhibicion = data;
                            _this.calificacionagotados(publicacion)
                                .then(function (data) {
                                calificacionagotados = data;
                                _this.calificacionprecios(publicacion)
                                    .then(function (data) {
                                    calificacionprecios = data;
                                    _this.calificacionexhibicionadicional(publicacion)
                                        .then(function (data) {
                                        calificacionexhibicionadicional = data;
                                        var tamaño_2 = calificacionexhibicionadicional.length;
                                        var j = 1;
                                        if (tamaño_2 == 0) {
                                            res.push([
                                                usuario,
                                                punto,
                                                tipo,
                                                comentario,
                                                foto,
                                                fecha,
                                                comentario_agotados,
                                                respuesta,
                                                evento_nombre,
                                                evento_ciudad,
                                                calificacionexhibicion,
                                                calificacionagotados,
                                                calificacionprecios,
                                                calificacionexhibicionadicional
                                            ]);
                                            if (h == tamaño) {
                                                resolve(res);
                                            }
                                            h++;
                                        }
                                        var _loop_3 = function (exhibicionadicional) {
                                            _this.respuestasexhibicionadicional(calificacionexhibicionadicional[exhibicionadicional][0])
                                                .then(function (data) {
                                                if (j == tamaño_2) {
                                                    calificacionexhibicionadicional[exhibicionadicional][4] = data;
                                                    res.push([
                                                        usuario,
                                                        punto,
                                                        tipo,
                                                        comentario,
                                                        foto,
                                                        fecha,
                                                        comentario_agotados,
                                                        respuesta,
                                                        evento_nombre,
                                                        evento_ciudad,
                                                        calificacionexhibicion,
                                                        calificacionagotados,
                                                        calificacionprecios,
                                                        calificacionexhibicionadicional
                                                    ]);
                                                    if (h == tamaño) {
                                                        resolve(res);
                                                    }
                                                    h++;
                                                }
                                                else
                                                    calificacionexhibicionadicional[exhibicionadicional][4] = data;
                                                j++;
                                            })
                                                .catch(function (error) { return reject(error); });
                                        };
                                        for (var exhibicionadicional in calificacionexhibicionadicional) {
                                            _loop_3(exhibicionadicional);
                                        }
                                    })
                                        .catch(function (error) { return reject(error); });
                                })
                                    .catch(function (error) { return reject(error); });
                            })
                                .catch(function (error) { return reject(error); });
                        })
                            .catch(function (error) { return reject(error); });
                    };
                    for (var i = 0; i < tamaño; i++) {
                        _loop_2(i);
                    }
                }
                else
                    resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta publicaciones: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.calificacionexhibicion = function (publicacion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT publicacion, r1, r2, r3, r4, r5 FROM calificacionexhibicion WHERE publicacion = ?;", [publicacion])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).publicacion,
                            data.rows.item(i).r1,
                            data.rows.item(i).r2,
                            data.rows.item(i).r3,
                            data.rows.item(i).r4,
                            data.rows.item(i).r5
                        ]);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta calificacionexhibicion: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.calificacionagotados = function (publicacion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT publicacion, producto FROM calificacionagotados WHERE publicacion = ?;", [publicacion])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).publicacion,
                            data.rows.item(i).producto
                        ]);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta calificacionagotados: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.calificacionprecios = function (publicacion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT publicacion, producto, precio FROM calificacionprecios WHERE publicacion = ?;", [publicacion])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).publicacion,
                            data.rows.item(i).producto,
                            data.rows.item(i).precio
                        ]);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta calificacionprecios: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.calificacionexhibicionadicional = function (publicacion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT id_c, publicacion, exhibicionadicional, foto FROM calificacionexhibicionadicional WHERE publicacion = ?;", [publicacion])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).id_c,
                            data.rows.item(i).publicacion,
                            data.rows.item(i).exhibicionadicional,
                            data.rows.item(i).foto
                        ]);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta calificacionexhibicionadicional: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.respuestasexhibicionadicional = function (calificacionexhibicionadicional) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT calificacionexhibicion, pregunta, respuesta FROM respuestasexhibicionadicional WHERE calificacionexhibicion = ?;", [calificacionexhibicionadicional])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).calificacionexhibicion,
                            data.rows.item(i).pregunta,
                            data.rows.item(i).respuesta
                        ]);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta respuestasexhibicionadicional: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.calificacioneseventos = function (publicacion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT id_c, usuario, evento, publicacion FROM calificacioneseventos WHERE publicacion = ?;", [publicacion])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).id_c,
                            data.rows.item(i).usuario,
                            data.rows.item(i).evento,
                            data.rows.item(i).publicacion
                        ]);
                    }
                }
                _this.respuestaseventos(res)
                    .then(function (data) {
                    resolve(data);
                })
                    .catch(function (error) { return reject(error); });
            })
                .catch(function (error) {
                alert("Error consulta calificacioneseventos: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.respuestaseventos = function (res, secuencia) {
        var _this = this;
        if (secuencia === void 0) { secuencia = 0; }
        return new Promise(function (resolve, reject) {
            var tamaño = res.length;
            _this.storage.executeSql("SELECT calificacioneseventos, pregunta, respuesta FROM respuestaseventos WHERE calificacioneseventos = ?;", [res[secuencia][0]])
                .then(function (data) {
                var respuestas = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        respuestas.push([
                            data.rows.item(i).calificacioneseventos,
                            data.rows.item(i).pregunta,
                            data.rows.item(i).respuesta
                        ]);
                    }
                }
                res[secuencia][4] = respuestas;
                if ((secuencia + 1) == tamaño) {
                    resolve(res);
                }
                secuencia++;
                if (secuencia < tamaño) {
                    _this.respuestaseventos(res, secuencia = 0)
                        .then(function () {
                    })
                        .catch(function (error) { return reject(error); });
                }
            })
                .catch(function (error) {
                alert("Error consulta respuestaseventos: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.respuestasvisitamedica = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT usuario, fuerza, especialidad, visitador, comentario FROM respuestasvisitamedica;", [])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).usuario,
                            data.rows.item(i).fuerza,
                            data.rows.item(i).especialidad,
                            data.rows.item(i).visitador,
                            data.rows.item(i).comentario
                        ]);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta respuestasvisitamedica: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.respuestaspreguntasvisita = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT respuestavisitamedica, pregunta, respuesta FROM respuestaspreguntasvisita;", [])
                .then(function (data) {
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push([
                            data.rows.item(i).respuestavisitamedica,
                            data.rows.item(i).pregunta,
                            data.rows.item(i).respuesta
                        ]);
                    }
                }
                resolve(res);
            })
                .catch(function (error) {
                alert("Error consulta respuestaspreguntasvisita: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    DatabaseServiceProvider.prototype.verPuntos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.executeSql("SELECT calificacionexhibicion, pregunta, respuesta FROM respuestasexhibicionadicional", [])
                .then(function (data) {
                alert("respuestaspreguntasvisita: " + data.rows.length);
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            calificacionexhibicion: data.rows.item(i).calificacionexhibicion,
                            pregunta: data.rows.item(i).pregunta,
                            respuesta: data.rows.item(i).respuesta
                        });
                    }
                }
                alert("datos: " + JSON.stringify(res));
                resolve(true);
            })
                .catch(function (error) {
                alert("Error consulta publicaciones: " + JSON.stringify(error));
                reject(error);
            });
            _this.storage.executeSql("SELECT usuario FROM publicaciones", [])
                .then(function (data) {
                alert("publicaciones: " + data.rows.length);
                var res = [];
                var tamaño = data.rows.length;
                if (tamaño > 0) {
                    for (var i = 0; i < tamaño; i++) {
                        res.push({
                            usuario: data.rows.item(i).usuario,
                        });
                    }
                }
                alert("datos publicaciones: " + JSON.stringify(res));
                resolve(true);
            })
                .catch(function (error) {
                alert("Error consulta publicaciones: " + JSON.stringify(error));
                reject(error);
            });
        });
    };
    return DatabaseServiceProvider;
}());
DatabaseServiceProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [SQLite, Platform, HTTP, Network, File, FileTransfer, Storage])
], DatabaseServiceProvider);
export { DatabaseServiceProvider };
//# sourceMappingURL=database-service.js.map