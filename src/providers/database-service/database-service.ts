import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { CamaraProvider } from '../../providers/camara/camara';

declare var cordova: any;

@Injectable()

export class DatabaseServiceProvider {

	private storage: SQLiteObject;
	public conexion = false;
	abortarDescarga = 0;
	reader;

	constructor(private sqlite: SQLite, platform: Platform, private http: HTTP, private network: Network, private file: File, private transfer: FileTransfer, private storageLocal: Storage, public cameraProvider: CamaraProvider) {

		platform.ready().then(() => {
			this.sqlite.create({
				name: 'pa-la-calle.db',
				location: 'default'
			})
				.then((db: SQLiteObject) => {
					this.storage = db;
					this.storage.executeSql('SELECT usuario FROM usuarios LIMIT 1;', {})
						.then(() => {
							//this.consultaPrueba();
						})
						.catch(error => {
							// console.log("No existen tablas: " + JSON.stringify(error));
							this.crearEstructuraBaseDatos();
						});
				})
				.catch(error => console.log("Error crear base de datos: " + JSON.stringify(error)));
		});
	}

	formatoNumero(numero) {
		return numero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}

	crearEstructuraBaseDatos() {
		let estructura = [
			`CREATE TABLE IF NOT EXISTS usuarios(
			   id_u INTEGER PRIMARY KEY AUTOINCREMENT,
			   usuario  varchar(50),
			   password varchar(50),
			   nombre varchar(50),
			   cargo varchar(70),
			   correo varchar(50),
			   tipo char(1),
			   ciudad int,
			   foto varchar(3)
			)`,
			`CREATE TABLE IF NOT EXISTS puntos(
			   id_p INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre  varchar(70),
			   tipo int,
			   direccion varchar(200),
			   numero int,
			   canal varchar(50),
			   latitud varchar(20), 
			   longitud varchar(20), 
			   foto varchar(3), 
			   ciudad varchar(45), 
			   zona varchar(25), 
			   rv varchar(15), 
			   cadena int, 
			   nombre_fantasia varchar(70), 
			   distribuidor varchar(45), 
			   telefono varchar(45), 
			   supervisor varchar(45)
			)`,
			`CREATE TABLE IF NOT EXISTS usuarios_puntos(
			   id_up INTEGER PRIMARY KEY AUTOINCREMENT,
			   usuario int,
			   punto int,
			   sector int,
			   fecha datetime,
			   estado tinyint(1)
			)`,
			`CREATE TABLE IF NOT EXISTS tipospunto(
			   id_t INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(60),
			   foto varchar(3)
			)`,
			`CREATE TABLE IF NOT EXISTS productos(
			   id_p INTEGER PRIMARY KEY AUTOINCREMENT,
			   referencia  varchar(150),
			   precio_min int,
			   precio_max int,
			   foto varchar(3),
			   categoria varchar(1)
			)`,
			`CREATE TABLE IF NOT EXISTS productos_cartilla(
			   id_p INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(50),
			   descripcion text,
			   descripcion2 text,
			   imagen varchar(5)
			)`,
			`CREATE TABLE IF NOT EXISTS tipospunto_productos(
			   id_tp INTEGER PRIMARY KEY AUTOINCREMENT,
			   tipopunto int,
			   producto int,
			   infaltable tinyint(1)
			)`,
			`CREATE TABLE IF NOT EXISTS exhibicionesadicionales(
			   id_e INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(70),
			   foto varchar(3)
			)`,
			`CREATE TABLE IF NOT EXISTS exhibicionesadicionales_especificacion(
			   id_e INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(120)
			)`,
			`CREATE TABLE IF NOT EXISTS exhibicionesadicionales_exhibicionesadicionales_especificacion(
			   id_ee INTEGER PRIMARY KEY AUTOINCREMENT,
			   exhibicionadicional int,
			   especificacion int
			)`,
			`CREATE TABLE IF NOT EXISTS respuestasvisitamedica(
			   id_r INTEGER PRIMARY KEY AUTOINCREMENT,
			   usuario int,
			   fuerza int,
			   especialidad int,
			   visitador int,
			   comentario varchar(255),
			   fecha varchar(80)
			)`,
			`CREATE TABLE IF NOT EXISTS respuestaspreguntasvisita(
			   id_r INTEGER PRIMARY KEY AUTOINCREMENT,
			   respuestavisitamedica int,
			   pregunta int,
			   respuesta int
			)`,
			`CREATE TABLE IF NOT EXISTS publicaciones(
			   id_p INTEGER PRIMARY KEY AUTOINCREMENT,
			   usuario int,
			   punto int,
			   tipo char(1),
			   comentario text(800),
			   foto varchar(3),
			   fecha datetime,
			   comentario_agotados varchar(255),
			   respuesta varchar(255),
			   evento_nombre varchar(70),
			   evento_ciudad varchar(50),
			   foto_upload TEXT
			)`,
			`CREATE TABLE IF NOT EXISTS calificacionexhibicion(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   publicacion int,
			   r1 tinyint(1),
			   r2 tinyint(1),
			   r3 tinyint(1),
			   r4 tinyint(1),
			   r5 tinyint(1)
			)`,
			`CREATE TABLE IF NOT EXISTS calificacionagotados(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   publicacion int,
			   producto int
			)`,
			`CREATE TABLE IF NOT EXISTS calificacionprecios(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   publicacion int,
			   producto int,
			   precio int
			)`,
			`CREATE TABLE IF NOT EXISTS calificacionexhibicionadicional(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   publicacion int,
			   exhibicionadicional int,
			   foto varchar(3),
			   foto_upload TEXT
			)`,
			`CREATE TABLE IF NOT EXISTS respuestasexhibicionadicional(
			   id_r INTEGER PRIMARY KEY AUTOINCREMENT,
			   calificacionexhibicion int,
			   pregunta int,
			   respuesta int
			)`,
			`CREATE TABLE IF NOT EXISTS visitadores(
			   id_v INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(50),
			   ciudad int,
			   telefono varchar(39)
			)`,
			`CREATE TABLE IF NOT EXISTS especialidades(
			   id_e INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(50),
			   fuerza int
			)`,
			`CREATE TABLE IF NOT EXISTS visitadores_especialidades(
			   id_ve INTEGER PRIMARY KEY AUTOINCREMENT,
			   visitador int,
			   especialidad int
			)`,
			`CREATE TABLE IF NOT EXISTS fuerzas(
			   id_f INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(50)
			)`,
			`CREATE TABLE IF NOT EXISTS especialidades_productos(
			   id_ep INTEGER PRIMARY KEY AUTOINCREMENT,
			   especialidad int,
			   producto int
			)`,
			`CREATE TABLE IF NOT EXISTS eventos(
			   id_e INTEGER PRIMARY KEY AUTOINCREMENT,
			   ciudad  varchar(50),
			   nombre  varchar(70),
			   asistentes  int,
			   encargado varchar(50),
			   fecha varchar(80),
			   hora varchar(20)
			)`,
			`CREATE TABLE IF NOT EXISTS calificacioneseventos(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   usuario  int,
			   evento  int,
			   publicacion int
			)`,
			`CREATE TABLE IF NOT EXISTS respuestaseventos(
			   id_r INTEGER PRIMARY KEY AUTOINCREMENT,
			   calificacioneseventos  int,
			   pregunta  int,
			   respuesta tinyint(1)
			)`,
			`CREATE TABLE IF NOT EXISTS comentarios(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   publicacion  int,
			   usuario  int,
			   usuario_nombre varchar(50),
			   usuario_foto varchar(3),
			   fecha datetime,
			   comentario varchar(255)
			)`,
			`CREATE TABLE IF NOT EXISTS likes(
			   id_l INTEGER PRIMARY KEY AUTOINCREMENT,
			   publicacion  int,
			   usuario int
			)`,
			`CREATE TABLE IF NOT EXISTS puntos_exhibicionesadicionales(
			   id_pe INTEGER PRIMARY KEY AUTOINCREMENT,
			   punto int,
			   exhibicionadicional int
			)`,
			`CREATE TABLE IF NOT EXISTS usuarios_visitadores(
			   id_uv INTEGER PRIMARY KEY AUTOINCREMENT,
			   usuario int,
			   visitador int
			)`,
			`CREATE TABLE IF NOT EXISTS ciudades(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(50),
			   grupo int
			)`,
			`CREATE TABLE IF NOT EXISTS ciudades_grupos(
			   id_g INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(50)
			)`,
			`CREATE TABLE IF NOT EXISTS cadenas(
			   id_c INTEGER PRIMARY KEY AUTOINCREMENT,
			   nombre varchar(50)
			)`,
			`CREATE TABLE IF NOT EXISTS preguntas(
			   id_p INTEGER PRIMARY KEY AUTOINCREMENT,
			   pregunta varchar(255),
			   tipo varchar(2)
			)`];
		this.ejecutarBatch(estructura)
			.then((result) => {
				console.log("Base de datos");
			})
			.catch(error => { });

	}

	crearCarpeta(lugar, carpeta) {
		return new Promise((resolve, reject) => {
			this.file.createDir(lugar, carpeta, false)
				.then(() => {
					resolve(true);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	borrarCarpeta(lugar, carpeta) {
		return new Promise((resolve, reject) => {
			this.file.removeRecursively(lugar, carpeta)
				.then(() => {
					this.crearCarpeta(lugar, carpeta).then(() => {
						resolve(true);
					}).catch((error) => {
						reject(error);
					});
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	validarCarpeta(lugar, carpeta) {
		return new Promise((resolve, reject) => {
			this.file.checkDir(lugar, carpeta)
				.then(() => {
					this.borrarCarpeta(lugar, carpeta).then(() => {
						resolve(true);
					}).catch((error) => {
						reject(error);
					});
				})
				.catch(() => {
					this.crearCarpeta(lugar, carpeta).then(() => {
						resolve(true);
					}).catch((error) => {
						reject(error);
					});
				});
		});
	}

	validandoCarpetas() {
		return new Promise((resolve, reject) => {
			let dataDirectory = cordova.file.dataDirectory;
			this.validarCarpeta(dataDirectory, "tipospunto").then(() => {
				this.validarCarpeta(dataDirectory, "productos").then(() => {
					this.validarCarpeta(dataDirectory, "exhibiciones-adicionales")
						.then(() => {
							resolve(true);
						})
						.catch((error) => {
							reject(error);
						}); // exhibiciones
				}); // productos
			}); // tipospunto
		}); // return Promise
	}

	cancelarDescarga() {
		this.abortarDescarga = 1;
	}

	descargarArchivos(archivos, urlDescarga, destinoApp, carpeta) {
		return new Promise((resolve, reject) => {
			const fileTransfer: FileTransferObject = this.transfer.create();
			let archivos_descargados = 0;
			let archivos_por_descargar = archivos.length;
			let _error = 0;
			let progreso = 0;
			// console.log("archivos " + carpeta + ": " + archivos);
			for (let archivo of archivos) {
				// console.log("archivo: " + archivo);
				fileTransfer.onProgress((progressEvent: any) => {
					if (progressEvent.lengthComputable) {
						let progress = Math.round((progressEvent.loaded / progressEvent.total) * 100);
						if (progress > progreso) {
							progreso = progress;
							(<HTMLInputElement>document.getElementById("progreso-actualizacion")).value = String(progreso);
						}
					}
				});
				fileTransfer.download(`${urlDescarga}${archivo}`, `${destinoApp}${carpeta}${archivo}`)
					.then((entry) => {
						// console.log("Bn archivo: " + archivo);
						progreso = 0;
						archivos_descargados++;
						if (archivos_descargados == 20) {
							fileTransfer.abort();
							resolve(true);
						} else
							if (archivos_descargados == archivos_por_descargar) {
								resolve(true);
							}
					})
					.catch((error) => {
						// console.log("Mal archivo: " + archivo);
						if (error.http_status >= 500) {
							alert("¡Error en el servidor! COD-253 [" + carpeta + archivo + "]");
						} else
							if (error.http_status >= 400) {
								alert("¡Error en el servidor! COD-252 [" + carpeta + archivo + "]");
							} else {
								console.log("error fileTransfer.download: " + JSON.stringify(error));
								alert("¡No tienes conexión de internet! COD-251");
							}
						_error = 1;
						reject(error);
					});

				if ((this.abortarDescarga == 1) || (_error == 1)) {
					fileTransfer.abort();
					alert("¡Actualización Cancelada!");
					break;
				}
			}
		});
	}

	administradorDescarga(archivos, urlDescarga, destinoApp, carpeta, loop_total, loop) {
		return new Promise((resolve, reject) => {
			if (loop_total == loop) {
				resolve(true);
			} else {
				this.descargarArchivos(archivos[loop], urlDescarga, destinoApp, carpeta)
					.then(() => {
						loop++;
						this.administradorDescarga(archivos, urlDescarga, destinoApp, carpeta, loop_total, loop)
							.then(() => {
								resolve(true);
							})
							.catch((error) => {
								reject(error);
							});
					})
					.catch((error) => {
						reject(error);
					});
			}
		});
	}

	dividirArray(array, posiciones) {
		let i,
			tamaño = Math.ceil((array.length) / 10) * 10,
			aux = [];
		for (i = 0; i < tamaño; i += posiciones) {
			aux.push(array.slice(i, i + posiciones));
		}
		return aux;
	}

	//Organiza la data que llega en un sql y ejecuta en la bd interna del celular
	procesoActualizacion(data) {
		return new Promise((resolve, reject) => {
			data.data = JSON.parse(data.data);
			let datos = [];
			let tupla = [];
			let imagenes_tipopunto = [];
			let imagenes_productos = [];
			let imagenes_exhibiciones = [];

			datos.push(['DELETE FROM ciudades', []]);
			datos.push(['DELETE FROM ciudades_grupos', []]);
			datos.push(['DELETE FROM especialidades', []]);
			datos.push(['DELETE FROM especialidades_productos', []]);
			datos.push(['DELETE FROM eventos', []]);
			datos.push(['DELETE FROM exhibicionesadicionales', []]);
			datos.push(['DELETE FROM exhibicionesadicionales_especificacion', []]);
			datos.push(['DELETE FROM exhibicionesadicionales_exhibicionesadicionales_especificacion', []]);
			datos.push(['DELETE FROM productos', []]);
			datos.push(['DELETE FROM productos_cartilla', []]);
			datos.push(['DELETE FROM puntos', []]);
			datos.push(['DELETE FROM puntos_exhibicionesadicionales', []]);
			datos.push(['DELETE FROM fuerzas', []]);
			datos.push(['DELETE FROM tipospunto', []]);
			datos.push(['DELETE FROM tipospunto_productos', []]);
			datos.push(['DELETE FROM usuarios', []]);
			datos.push(['DELETE FROM usuarios_puntos', []]);
			datos.push(['DELETE FROM visitadores', []]);
			datos.push(['DELETE FROM visitadores_especialidades', []]);
			datos.push(['DELETE FROM usuarios_visitadores', []]);
			datos.push(['DELETE FROM cadenas', []]);
			datos.push(['DELETE FROM preguntas', []]);

			let sql = {
				ciudades: 'INSERT INTO ciudades VALUES (?,?,?)',
				ciudades_grupos: 'INSERT INTO ciudades_grupos VALUES (?,?)',
				especialidades: 'INSERT INTO especialidades VALUES (?,?,?)',
				especialidades_productos: 'INSERT INTO especialidades_productos VALUES (?,?,?)',
				eventos: 'INSERT INTO eventos VALUES (?,?,?,?,?,?,?)',
				exhibicionesadicionales: 'INSERT INTO exhibicionesadicionales VALUES (?,?,?)',
				exhibicionesadicionales_especificacion: 'INSERT INTO exhibicionesadicionales_especificacion VALUES (?,?)',
				exhibicionesadicionales_exhibicionesadicionales_especificacion: 'INSERT INTO exhibicionesadicionales_exhibicionesadicionales_especificacion VALUES (?,?,?)',
				fuerzas: 'INSERT INTO fuerzas VALUES (?,?)',
				productos: 'INSERT INTO productos VALUES (?,?,?,?,?,?)',
				productos_cartilla: 'INSERT INTO productos_cartilla VALUES (?,?,?,?,?)',
				puntos: 'INSERT INTO puntos VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
				puntos_exhibicionesadicionales: 'INSERT INTO puntos_exhibicionesadicionales VALUES (?,?,?)',
				tipospunto: 'INSERT INTO tipospunto VALUES (?,?,?)',
				tipospunto_productos: 'INSERT INTO tipospunto_productos VALUES (?,?,?,?)',
				usuarios: 'INSERT INTO usuarios VALUES (?,?,?,?,?,?,?,?,?)',
				usuarios_puntos: 'INSERT INTO usuarios_puntos VALUES (?,?,?,?,?,?)',
				/*Espacio de storage insuficiente... Se quitan tablas para ocupar menos espacio
				visitadores       				: 'INSERT INTO visitadores VALUES (?,?,?,?)',
                visitadores_especialidades  	: 'INSERT INTO visitadores_especialidades VALUES (?,?,?)',
                usuarios_visitadores  			: 'INSERT INTO usuarios_visitadores VALUES (?,?,?)',*/
				cadenas: 'INSERT INTO cadenas VALUES (?,?)',
				preguntas: 'INSERT INTO preguntas VALUES (?,?,?)'
			};

			/*   ------------- Creando sql Actualizacion  */
			for (let indice in data.data[0]) { //tablas
				if (indice == "tipospunto") {
					for (let fila of data.data[0][indice]) {
						tupla = [];
						tupla.push(sql[indice], fila);
						datos.push(tupla);
						imagenes_tipopunto.push(fila[0] + "." + fila[2]);
					}
				} else
					if (indice == "productos") {
						for (let fila of data.data[0][indice]) {
							tupla = [];
							tupla.push(sql[indice], fila);
							datos.push(tupla);
							imagenes_productos.push(fila[0] + "." + fila[4]);
						}
					} else
						if (indice == "exhibicionesadicionales") {
							for (let fila of data.data[0][indice]) {
								tupla = [];
								tupla.push(sql[indice], fila);
								datos.push(tupla);
								imagenes_exhibiciones.push(fila[0] + "." + fila[2]);
							}
						} else
							for (let fila of data.data[0][indice]) {
								tupla = [];
								tupla.push(sql[indice], fila);
								datos.push(tupla);
							}
			}

			// console.log("datos: " + JSON.stringify(datos));

			/*   ------------- Acualizando la base de datos  */
			this.ejecutarBatch(datos)
				.then((result) => {
					// console.log("ejecutarBatch actualizarApp bn");
					(<HTMLInputElement>document.getElementById("progreso-actualizacion")).value = String(100);
					/*   ------------- Transfiriendo archivos al dispositivo  */
					this.descargaArchivos(imagenes_tipopunto, imagenes_productos, imagenes_exhibiciones)
						.then(() => {
							resolve(true);
						})
						.catch(error => reject(error));
				})
				.catch(error => reject(error));

		});    // end return promise
	}

	descargaArchivos(imagenes_tipopunto, imagenes_productos, imagenes_exhibiciones) {
		return new Promise((resolve, reject) => {
			let url_imagenes_tipopunto = 'http://palacalle.abbottdata.com/imagenes/tipospunto/';
			let url_imagenes_productos = 'http://palacalle.abbottdata.com/imagenes/productos/';
			let url_imagenes_exhibiciones = 'http://palacalle.abbottdata.com/imagenes/exhibiciones-adicionales/';
			let dataDirectory = cordova.file.dataDirectory;
			let divisiones_array = 0;

			/* Descargando archivos*/
			this.abortarDescarga = 0;
			document.getElementById("text-overlay").innerHTML = "Descargando Imágenes";
			divisiones_array = Math.ceil((imagenes_tipopunto.length) / 10);
			this.administradorDescarga(this.dividirArray(imagenes_tipopunto, 10), url_imagenes_tipopunto, dataDirectory, "tipospunto/", divisiones_array, 0)
				.then(() => {
					document.getElementById("text-overlay").innerHTML = "Descargando Productos";
					divisiones_array = Math.ceil((imagenes_productos.length) / 10);
					this.administradorDescarga(this.dividirArray(imagenes_productos, 10), url_imagenes_productos, dataDirectory, "productos/", divisiones_array, 0)
						.then(() => {
							document.getElementById("text-overlay").innerHTML = "Descargando Exhibiciones Adicionales";
							divisiones_array = Math.ceil((imagenes_exhibiciones.length) / 10);
							this.administradorDescarga(this.dividirArray(imagenes_exhibiciones, 10), url_imagenes_exhibiciones, dataDirectory, "exhibiciones-adicionales/", divisiones_array, 0)
								.then(() => {
									alert("¡Actualización completa!, Debe reiniciar la aplicación");
									resolve(true);
								})
								.catch((error) => {
									reject(error);
								});/* exhibiciones adicionales*/
						})
						.catch((error) => {
							reject(error);
						});/* productos*/
				})
				.catch((error) => {
					reject(error);
				}); /* tipospunto*/
		});
	}

	actualizarApp() {
		return new Promise((resolve, reject) => {
			this.storageLocal.get('usuario')
				.then((val) => {
					var idUsuario = val;
					console.log("idUsuario: " + idUsuario.id_u);
					document.getElementById("text-overlay").innerHTML = "Descargando información";
					(<HTMLInputElement>document.getElementById("progreso-actualizacion")).value = String(0);
					this.http.get('http://palacalle.abbottdata.com/backend/v1/actualizar-app/index2.php', { usuario: idUsuario.id_u }, {})
						.then(data => {
							/* console.log("actualizarApp: " + JSON.stringify(data)); */
							this.validandoCarpetas()
								.then(() => {
									this.procesoActualizacion(data)
										.then(() => {
											resolve(true);
										})
										.catch((error) => {
											alert("¡Error al actualizar la información!");
											console.log("¡Error al actualizar la información! " + JSON.stringify(error));
											reject(error);
										});
								})
								.catch((error) => {
									alert("¡Error al actualizar la información! COD-24");
									console.log("¡Error al actualizar la información! COD-24" + JSON.stringify(error));
									reject(error);
								});
						})
						.catch(error => {
							if (error.status >= 500) {
								alert("¡Error, el servidor no responde!");
							} else
								if (error.status >= 400) {
									alert("¡Error, no se encuentra el servidor!");
								} else {
									alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
								}
							console.log("Error enviar datos a actualizar-app" + JSON.stringify(error));
							reject(error);
						});

				})
				.catch((error) => {
					console.log("Error" + error);

				});
		});
	}

	ejecutarBatch(datos) {
		console.log(datos);

		return new Promise((resolve, reject) => {
			this.storage.sqlBatch(datos)
				.then(() => {
					resolve(true);
				})
				.catch(error => {
					console.log(error);

					alert("¡Error en el sistema! COD-211 ");
					console.log("ERROR ejecutarBatch: " + JSON.stringify(error.message));
					reject(error);
				});
		});
	}

	probarConexion(probar = "") {
		return new Promise((resolve, reject) => {
			if (probar == "si") {
				if (this.network.type == 'none') {
					reject(1);
				} else
					resolve(1);
			} else {
				this.storageLocal.get('internet')
					.then((val) => {
						if (val === false) {
							reject(1);
						} else {
							if (this.network.type == 'none') {
								reject(1);
							} else
								resolve(1);
						}
					})
					.catch((error) => {
						console.log("error storage probarConexion: " + JSON.stringify(error));
					});
			}

		});
	}

	validarUsuario(datos) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql("SELECT id_u, nombre, foto, tipo FROM usuarios WHERE usuario = ? AND password = ? LIMIT 1", datos)
				.then((data) => {
					let res = {};
					let nombre = "";
					let tipo = "";
					if (data.rows.length > 0) {
						for (let i = 0; i < data.rows.length; i++) {
							res = {
								id_u: data.rows.item(i).id_u,
								nombre: data.rows.item(i).nombre,
								foto: data.rows.item(i).foto,
								tipo: data.rows.item(i).tipo
							};
							nombre = data.rows.item(i).nombre;
							tipo = data.rows.item(i).tipo;
						}
						this.storageLocal.set('usuario', res);
						if (tipo == "s")
							this.storageLocal.set('internet', true);
					}
					resolve([nombre, tipo]);
				})
				.catch(error => {
					console.log("Error consulta validarUsuario: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	preguntasExhibicion_no_Internet() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql("SELECT pregunta FROM preguntas WHERE tipo = 'e' LIMIT 5;", [])
				.then((data) => {
					let aux = [];
					let res = [{
						uno: "",
						dos: "",
						tres: "",
						cuatro: "",
						cinco: ""
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							aux.push(data.rows.item(i).pregunta);
						}
						res[0] = {
							uno: aux[0],
							dos: aux[1],
							tres: aux[2],
							cuatro: aux[3],
							cinco: aux[4]
						};
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta preguntasExhibicion_no_Internet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	sprintf(numero) {
		if (String(numero).length == 1) {
			numero = "0" + String(numero);
		}
		return numero;
	}

	fecha() {
		let dt = new Date();
		let time_offset = Math.floor(dt.getTimezoneOffset() / 60);
		if (time_offset == 0) {
			// time_offset = 5;
		}
		let hora = dt.getHours() - time_offset;
		let dia = dt.getDate();
		if (hora <= 0) {
			hora = 24 - hora;
			dia = dia - 1;
		}
		let mes = this.sprintf(dt.getMonth() + 1);
		dia = this.sprintf(dia);
		hora = this.sprintf(hora);
		let minutos = this.sprintf(dt.getMinutes());
		let segundos = this.sprintf(dt.getSeconds());
		return dt.getFullYear() + "-" + mes + "-" + dia + " " + hora + ":" + minutos + ":" + segundos;
	}

	loadPuntosMisPuntosVisitados(usuario, res) {
		return new Promise((resolve, reject) => {
			let fecha = this.fecha();
			this.storage.executeSql(`SELECT punto
				FROM publicaciones
				WHERE fecha > DATETIME(?,'-3 day') AND usuario = ? AND (tipo = 'p' OR tipo = 'n');`, [fecha, usuario])
				.then((data: any) => {
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							for (var punto of res) {
								if (data.rows.item(i).punto == punto.id_p) {
									punto.estado = 2;
									break;
								}
							}
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta loadPuntosMisPuntosVisitados: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadPuntosMios(ciudad, cadena, usuario) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.id_p, p.nombre, tp.foto, tp.nombre AS tipo_nombre, up.usuario, p.direccion, tp.id_t 	
					FROM puntos p 
					LEFT JOIN tipospunto tp ON p.tipo = tp.id_t 
					LEFT JOIN usuarios_puntos up ON up.punto = p.id_p 
					WHERE up.usuario = ? AND p.ciudad = ? AND p.cadena = ?
					GROUP BY p.id_p
					ORDER BY p.nombre ASC;`, [usuario, ciudad, cadena])
				.then((data) => {
					let res = [{
						id_p: "",
						nombre: "SELECCIONE",
						foto: "",
						tipo_nombre: "",
						tipo_id: "",
						usuario: "",
						direccion: "",
						estado: 0
					}];
					let tamaño = data.rows.length;
					let estado = 1;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								nombre: data.rows.item(i).nombre,
								foto: data.rows.item(i).id_t + "." + data.rows.item(i).foto,
								tipo_nombre: data.rows.item(i).tipo_nombre,
								tipo_id: data.rows.item(i).id_t,
								usuario: data.rows.item(i).usuario,
								direccion: data.rows.item(i).direccion,
								estado: estado
							});
						}
					}
					this.loadPuntosMisPuntosVisitados(usuario, res)
						.then((data) => {
							resolve(data);
						})
						.catch((error) => {
							reject(error);
						});
				})
				.catch(error => {
					console.log("Error consulta loadPuntosMios: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadPuntosOtros(ciudad, cadena, usuario) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.id_p, p.nombre, tp.foto, tp.nombre AS tipo_nombre, p.direccion, tp.id_t 	
					FROM puntos p 
					LEFT JOIN tipospunto tp ON p.tipo = tp.id_t 
					WHERE p.ciudad = ? AND p.cadena = ?
					GROUP BY p.id_p
					ORDER BY p.nombre ASC;`, [ciudad, cadena])
				.then((data) => {
					let res = [{
						id_p: "",
						nombre: "SELECCIONE",
						foto: "",
						tipo_nombre: "",
						tipo_id: "",
						usuario: "",
						direccion: "",
						estado: 0
					}];
					let tamaño = data.rows.length;
					let estado = 0;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								nombre: data.rows.item(i).nombre,
								foto: data.rows.item(i).id_t + "." + data.rows.item(i).foto,
								tipo_nombre: data.rows.item(i).tipo_nombre,
								tipo_id: data.rows.item(i).id_t,
								usuario: "",
								direccion: data.rows.item(i).direccion,
								estado: estado
							});
						}
					}
					this.loadPuntosMisPuntosVisitados(usuario, res)
						.then((data) => {
							resolve(data);
						})
						.catch((error) => {
							reject(error);
						});
				})
				.catch(error => {
					console.log("Error consulta loadPuntosOtros: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadPuntos(usuario, ciudad, cadena, puntos_traer) {
		return new Promise((resolve, reject) => {
			if (puntos_traer == 1) {
				this.loadPuntosMios(ciudad, cadena, usuario)
					.then((data) => {
						resolve(data);
					})
					.catch((error) => {
						reject(error);
					});
			} else
				this.loadPuntosOtros(ciudad, cadena, usuario)
					.then((data) => {
						resolve(data);
					})
					.catch((error) => {
						reject(error);
					});
		});
	}

	loadPuntosMios_noVisitados(usuario) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.id_p, p.nombre, tp.foto, tp.nombre AS tipo_nombre, up.usuario, p.direccion, tp.id_t, pu.id_p AS pu_id_ps
					FROM puntos p 
					LEFT JOIN tipospunto tp ON p.tipo = tp.id_t 
					LEFT JOIN usuarios_puntos up ON up.punto = p.id_p 
                    LEFT JOIN publicaciones pu ON pu.punto = p.id_p
					WHERE up.usuario = ? AND pu.id_p IS NULL AND up.estado = 3
					GROUP BY p.id_p
					ORDER BY p.nombre ASC;`, [usuario])
				.then((data) => {
					let res = [{
						id_p: "",
						nombre: "SELECCIONE",
						foto: "",
						tipo_nombre: "",
						tipo_id: "",
						usuario: "",
						direccion: "",
						estado: 0
					}];
					let tamaño = data.rows.length;
					let estado = 1;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								nombre: data.rows.item(i).nombre,
								foto: data.rows.item(i).id_t + "." + data.rows.item(i).foto,
								tipo_nombre: data.rows.item(i).tipo_nombre,
								tipo_id: data.rows.item(i).id_t,
								usuario: data.rows.item(i).usuario,
								direccion: data.rows.item(i).direccion,
								estado: estado
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta loadPuntosMios: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadCiudadesMios(usuario) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.ciudad, c.nombre
				FROM usuarios_puntos up
				INNER JOIN puntos p ON p.id_p = up.punto 
				INNER JOIN ciudades c ON c.id_c = p.ciudad 
				WHERE up.usuario = ?
				GROUP BY c.nombre
				ORDER BY c.nombre ASC;`, [usuario])
				.then((data) => {
					let res = [{
						id_c: "",
						nombre: "SELECCIONE"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_c: data.rows.item(i).ciudad,
								nombre: data.rows.item(i).nombre
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta loadCiudadesMios: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadCiudadesOtros() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.ciudad, c.nombre
				FROM  puntos p
				INNER JOIN ciudades c ON c.id_c = p.ciudad
                GROUP BY c.nombre
                ORDER BY c.nombre ASC;`, [])
				.then((data) => {
					let res = [{
						id_c: "",
						nombre: "SELECCIONE"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_c: data.rows.item(i).ciudad,
								nombre: data.rows.item(i).nombre
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta loadCiudadesOtros: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadCiudades(usuario, puntos_traer) {
		return new Promise((resolve, reject) => {
			if (puntos_traer == 1) {
				this.loadCiudadesMios(usuario)
					.then((data) => {
						resolve(data);
					})
					.catch((error) => {
						reject(error);
					});
			} else
				this.loadCiudadesOtros()
					.then((data) => {
						resolve(data);
					})
					.catch((error) => {
						reject(error);
					});
		});
	}

	loadCadenasMios(usuario, ciudad) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.cadena, c.nombre
				FROM usuarios_puntos up
				INNER JOIN puntos p ON p.id_p = up.punto 
				INNER JOIN cadenas c ON c.id_c = p.cadena 
				WHERE up.usuario = ? AND p.ciudad = ?
				GROUP BY c.nombre
				ORDER BY c.nombre ASC;`, [usuario, ciudad])
				.then((data) => {
					let res = [{
						id_c: "",
						nombre: "SELECCIONE"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_c: data.rows.item(i).cadena,
								nombre: data.rows.item(i).nombre
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta loadCadenasMios: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadCadenasOtros(ciudad) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.cadena, c.nombre
				FROM puntos p
				INNER JOIN cadenas c ON c.id_c = p.cadena 
				WHERE p.ciudad = ?
				GROUP BY c.nombre
				ORDER BY c.nombre ASC;`, [ciudad])
				.then((data) => {
					let res = [{
						id_c: "",
						nombre: "SELECCIONE"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_c: data.rows.item(i).cadena,
								nombre: data.rows.item(i).nombre
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta loadCadenasOtros: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	loadCadenas(usuario, ciudad, puntos_traer) {
		return new Promise((resolve, reject) => {
			if (puntos_traer == 1) {
				this.loadCadenasMios(usuario, ciudad)
					.then((data) => {
						resolve(data);
					})
					.catch((error) => {
						reject(error);
					});
			} else
				this.loadCadenasOtros(ciudad)
					.then((data) => {
						resolve(data);
					})
					.catch((error) => {
						reject(error);
					});
		});
	}

	loadTodosMisPuntos(usuario) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.id_p, p.nombre, tp.foto, tp.nombre AS tipo_nombre, up.usuario, p.direccion, tp.id_t 	
					FROM puntos p 
					LEFT JOIN tipospunto tp ON p.tipo = tp.id_t 
					LEFT JOIN usuarios_puntos up ON up.punto = p.id_p 
					WHERE up.usuario = ?
					GROUP BY p.id_p
					ORDER BY p.nombre ASC;`, [usuario])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					let estado = 1;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								nombre: data.rows.item(i).nombre,
								foto: data.rows.item(i).id_t + "." + data.rows.item(i).foto,
								tipo_nombre: data.rows.item(i).tipo_nombre,
								tipo_id: data.rows.item(i).id_t,
								usuario: data.rows.item(i).usuario,
								direccion: data.rows.item(i).direccion,
								estado: estado
							});
						}
					}
					this.loadPuntosMisPuntosVisitados(usuario, res)
						.then((data) => {
							resolve(data);
						})
						.catch((error) => {
							reject(error);
						});
				})
				.catch(error => {
					console.log("Error consulta loadTodosMisPuntos: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	buscarPunto(punto, usuario) {
		return new Promise((resolve, reject) => {

			this.storage.executeSql(`SELECT p.id_p, p.nombre, tp.foto, tp.nombre AS tipo_nombre, tp.id_t AS id_t, p.direccion
					FROM puntos p 
					LEFT JOIN tipospunto tp ON p.tipo = tp.id_t 
					WHERE p.nombre LIKE ?
					ORDER BY p.nombre ASC;`, ["%" + punto + "%"])
				.then((data) => {
					let res = [{
						id_p: "",
						nombre: "SELECCIONE",
						foto: "",
						tipo_nombre: "",
						tipo_id: "",
						usuario: "",
						direccion: "",
						estado: "0"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								nombre: data.rows.item(i).nombre,
								foto: data.rows.item(i).id_t + "." + data.rows.item(i).foto,
								tipo_nombre: data.rows.item(i).tipo_nombre,
								tipo_id: data.rows.item(i).id_t,
								usuario: "",
								direccion: data.rows.item(i).direccion,
								estado: "0"
							});
						}
						this.loadTodosMisPuntos(usuario)
							.then((data) => {
								let datos = [];
								datos.push(data);
								let mispuntos = datos;
								for (let indice in res) {
									for (let dato of mispuntos[0]) {
										if (dato['id_p'] == res[indice]['id_p']) {
											res[indice]['estado'] = dato['estado'];
											break;
										}
									}
								}
								resolve(res);
							})
							.catch((error) => {
								console.log("Error consulta loadTodosMisPuntos: " + JSON.stringify(error));
								reject(error);
							});
					} else {
						res = [{
							id_p: "",
							nombre: "NO SE ENCONTRARON PUNTOS",
							foto: "",
							tipo_nombre: "",
							tipo_id: "",
							usuario: "",
							direccion: "",
							estado: "0"
						}];
						resolve(res);
					}

				})
				.catch(error => {
					console.log("Error consulta buscarPunto: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	puntoEstado(punto, usuario) {
		return new Promise((resolve, reject) => {
			let fecha = this.fecha();
			this.storage.executeSql(`SELECT tipo
				FROM publicaciones
				WHERE fecha > DATETIME(?,'-3 day') AND usuario = ? AND punto = ?;`, [fecha, usuario, punto])
				.then((data) => {
					let res = [];
					res[0] = 0;
					res[1] = 0;
					if (data.rows.length > 0) {
						for (let i = 0; i < data.rows.length; i++) {
							if (data.rows.item(i).tipo == "p") {
								res[0] = 1;
							} else
								if (data.rows.item(i).tipo == "n") {
									res[1] = 1;
								}
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta puntoEstado: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	agotados(tipo, categoria) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.id_p, p.referencia, p.foto, tpp.infaltable
				FROM  productos p 
				INNER JOIN tipospunto_productos tpp ON p.id_p = tpp.producto 
				INNER JOIN tipospunto tp ON tpp.tipopunto = tp.id_t 
				WHERE tp.id_t = ? AND p.categoria = ?
				ORDER BY tpp.infaltable DESC, tpp.id_tp ASC;`, [tipo, categoria])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								nombre: data.rows.item(i).referencia,
								foto: data.rows.item(i).id_p + "." + data.rows.item(i).foto,
								infaltable: data.rows.item(i).infaltable
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta agotados: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	precio(tipo, categoria) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.id_p, p.referencia,p.foto, p.precio_min, p.precio_max 
				FROM  productos p 
				INNER JOIN tipospunto_productos tpp ON p.id_p = tpp.producto 
				INNER JOIN tipospunto tp ON tpp.tipopunto = tp.id_t 
				WHERE tp.id_t = ? AND p.categoria = ?
				ORDER BY tpp.infaltable DESC, tpp.id_tp ASC`, [tipo, categoria])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								nombre: data.rows.item(i).referencia,
								foto: data.rows.item(i).id_p + "." + data.rows.item(i).foto,
								precio_min: this.formatoNumero(data.rows.item(i).precio_min),
								precio_max: this.formatoNumero(data.rows.item(i).precio_max),
								precio_new: ""
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta precio: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	exhibicionesAdicionales(punto) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT exhibicionadicional
									FROM puntos_exhibicionesadicionales
									WHERE punto = ?;`, [punto])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push(data.rows.item(i).exhibicionadicional);
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta exhibicionesAdicionales: " + JSON.stringify(error));
					reject(error);
				});
		});
	}



	guardarPublicacion_noInternet_ExhibicionesAdicionalesDatos(calificacionexhibicion, exhibicionAdicionalDatos) {
		return new Promise((resolve, reject) => {
			let continuar = 1;
			let error;
			if (exhibicionAdicionalDatos.uno > 0) {
				this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '1', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.uno])
					.catch((error) => {
						continuar = 0;
						error = error;
					});
			}
			if (exhibicionAdicionalDatos.dos > 0) {
				this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '2', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.dos])
					.catch((error) => {
						continuar = 0;
						error = error;
					})
			}
			if (exhibicionAdicionalDatos.tres > 0) {
				this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '3', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.tres])
					.catch((error) => {
						continuar = 0;
						error = error;
					})
			}
			if (exhibicionAdicionalDatos.cuatro > 0) {
				this.storage.executeSql("INSERT INTO respuestasexhibicionadicional (calificacionexhibicion, pregunta, respuesta) VALUES (?, '4', ?)", [calificacionexhibicion, exhibicionAdicionalDatos.cuatro])
					.catch((error) => {
						continuar = 0;
						error = error;
					})
			}
			if (continuar == 1) {
				resolve(true);
			} else
				reject(error);
		});
	}

	guardarPublicacion_noInternet_ExhibicionesAdicionales(publicacion, exhibicionAdicional, exhibicionAdicionalDatos, secuencia = 0) {
		return new Promise((resolve, reject) => {
			let id_e;
			let cantidad_exhibiciones = exhibicionAdicional.length;

			id_e = exhibicionAdicional[secuencia];
			this.storage.executeSql("INSERT INTO calificacionexhibicionadicional (publicacion, exhibicionadicional, foto, foto_upload) VALUES (?, ?, 'png', ?)", [publicacion, id_e, exhibicionAdicionalDatos[secuencia].foto])
				.then(() => {
					let calificacionexhibicion = 0;
					this.storage.executeSql("SELECT MAX(id_c) AS id_c FROM calificacionexhibicionadicional", [])
						.then((data) => {
							calificacionexhibicion = data.rows.item(0).id_c;
							this.guardarPublicacion_noInternet_ExhibicionesAdicionalesDatos(calificacionexhibicion, exhibicionAdicionalDatos[secuencia])
								.then(() => {
									if ((secuencia + 1) == cantidad_exhibiciones) {
										resolve(1);
									}

									secuencia++;

									if (secuencia < cantidad_exhibiciones) {
										this.guardarPublicacion_noInternet_ExhibicionesAdicionales(publicacion, exhibicionAdicional, exhibicionAdicionalDatos, secuencia)
											.then(() => {
												resolve(1);
											})
											.catch((error) => {
												reject(error);
											});
									}

								})
								.catch((error) => {
									console.log("Error consulta guardarPublicacion_noInternet_ExhibicionesAdicionalesDatos: " + JSON.stringify(error));
									reject(error);
								});
						})
						.catch(error => {
							console.log("Error consulta MAX(id_c) guardarPublicacion_noInternet_ExhibicionesAdicionales: " + JSON.stringify(error));
							reject(error);
						});
				})
				.catch((error) => {
					console.log("Error consulta guardarPublicacion_noInternet_ExhibicionesAdicionales INSERT: " + JSON.stringify(error));
					reject(error);
				});
		});
	}


	guardarPublicacion_noInternet_Precio(precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos) {
		return new Promise((resolve, reject) => {
			let continuar = 1;
			let error;
			for (let producto of precio) {
				if (producto.precio_new != "") {
					this.storage.executeSql("INSERT INTO calificacionprecios (publicacion, producto, precio) VALUES (?, ?, ?)", [publicacion, producto.id_p, producto.precio_new])
						.catch((error) => {
							continuar = 0;
							error = error;
						});
				}
			}

			if (continuar == 1) {
				if (exhibicionAdicional != "") {
					this.guardarPublicacion_noInternet_ExhibicionesAdicionales(publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
						.then((data) => {
							resolve(1);
						})
						.catch((error) => {
							console.log("Error consulta guardarPublicacion_noInternet_ExhibicionesAdicionales: " + JSON.stringify(error));
							reject(error);
						});
				} else {
					resolve(publicacion);
				}
			} else {
				console.log("Error consulta guardarPublicacion_noInternet_Precio: " + JSON.stringify(error));
				reject(error);
			}

		});
	}

	guardarPublicacion_noInternet_Agotados(agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos) {
		return new Promise((resolve, reject) => {
			let productos_agotados = [];
			let producto;
			let continuar = 1;
			let error;

			for (producto of agotados) {
				if (producto != "") {
					productos_agotados.push(producto);
				}
			}

			for (producto of productos_agotados) {
				this.storage.executeSql("INSERT INTO calificacionagotados (publicacion, producto) VALUES (?, ?)", [publicacion, producto])
					.catch((error) => {
						continuar = 0;
						error = error;
					});
			}

			if (continuar == 1) {
				this.guardarPublicacion_noInternet_Precio(precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
					.then((data) => {
						resolve(data);
					})
					.catch((error) => {
						console.log("Error consulta guardarPublicacion_noInternet_Precio: " + JSON.stringify(error));
						reject(error);
					});
			} else {
				console.log("Error consulta guardarPublicacion_noInternet_Agotados: " + JSON.stringify(error));
				reject(error);
			}

		});
	}

	guardarPublicacion_noInternet_Exhibicion(exhibicion, agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql("INSERT INTO calificacionexhibicion (publicacion, r1, r2, r3, r4, r5) VALUES (?, ?, ?, ?, ?, ?);", [publicacion, exhibicion.uno, exhibicion.dos, exhibicion.tres, exhibicion.cuatro, exhibicion.cinco])
				.then(() => {
					this.guardarPublicacion_noInternet_Agotados(agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
						.then((data) => {
							resolve(data);
						})
						.catch((error) => {
							console.log("Error consulta guardarPublicacion_noInternet_Agotados: " + JSON.stringify(error));
							reject(error);
						});
				})
				.catch(error => {
					console.log("Error consulta guardarPublicacion_noInternet_Exhibicion: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	guardarPublicacion_noInternet(datos) {
		return new Promise((resolve, reject) => {

			let exhibicion = datos.exhibicion;
			let agotados = datos.agotados;
			let precio = datos.precio;
			// let fecha = this.fecha();
			let publicacion;
			let exhibicionAdicional;
			let exhibicionAdicionalDatos;
			if (datos.hasOwnProperty("exhibicionesAdicionales")) {
				exhibicionAdicional = datos.exhibicionesAdicionales;
				exhibicionAdicionalDatos = datos.exhibicionAdicionalDatos;
			} else {
				exhibicionAdicional = "";
				exhibicionAdicionalDatos = "";
			}

			/*  Publicacion  */
			this.storage.executeSql(`INSERT INTO publicaciones(usuario, punto, tipo, comentario, foto, fecha, foto_upload) VALUES (?, ?, ?, ?, ?, datetime('now','localtime'), ?)`, [datos.usuario, datos.id_punto, 'p', exhibicion.comentario, 'png', exhibicion.foto])
				.then((data) => {
					this.storage.executeSql("SELECT MAX(id_p) AS id_p FROM publicaciones", [])
						.then((data) => {
							publicacion = data.rows.item(0).id_p;
							this.guardarPublicacion_noInternet_Exhibicion(exhibicion, agotados, precio, publicacion, exhibicionAdicional, exhibicionAdicionalDatos)
								.then((data) => {
									resolve(data);
								})
								.catch((error) => {
									console.log("Error consulta guardarPublicacion_noInternet_Exhibicion: " + JSON.stringify(error));
									reject(error);
								});
						})
						.catch(error => {
							console.log("Error consulta MAX(id_p) guardarPublicacion_noInternet: " + JSON.stringify(error));
							reject(error);
						});
				})
				.catch(error => {
					console.log("Error consulta 1er insert guardarPublicacion_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	fotoTipoPunto(tipo) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT foto FROM tipospunto WHERE id_t = ?`, [tipo])
				.then((data) => {
					resolve(data.rows.item(0).foto);
				})
				.catch(error => {
					console.log("Error consulta fotoTipoPunto: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	exhibicionAdicionalInfo(punto, exhibicionAdicional) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT e.id_e, e.nombre AS nombre, ee.nombre AS especificacion, e.foto
			FROM puntos_exhibicionesadicionales pe
       		INNER JOIN exhibicionesadicionales_exhibicionesadicionales_especificacion eee ON pe.exhibicionadicional = eee.id_ee
       		INNER JOIN exhibicionesadicionales_especificacion ee ON ee.id_e = eee.especificacion
			INNER JOIN exhibicionesadicionales e ON e.id_e = eee.exhibicionadicional 
			WHERE pe.punto = ? AND pe.exhibicionadicional = ? LIMIT 1;`, [punto, exhibicionAdicional])
				.then((data) => {
					let res = {
						id_e: "",
						nombre: "",
						foto: ""
					};
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							let nombre_final = data.rows.item(i).nombre + ": " + data.rows.item(i).especificacion;
							if (data.rows.item(i).nombre == data.rows.item(i).especificacion) {
								nombre_final = data.rows.item(i).nombre;
							}
							res = {
								id_e: data.rows.item(i).id_e,
								nombre: nombre_final,
								foto: data.rows.item(i).id_e + "." + data.rows.item(i).foto
							};
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta exhibicionAdicionalInfo: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	mostrarVisitadorMes_no_Internet(usuario) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT v.id_v AS v_id, v.nombre AS v_nombre, f.id_f AS f_id, f.nombre AS f_nombre, v.telefono AS v_telefono, e.id_e, e.nombre
				FROM visitadores v
				INNER JOIN usuarios_visitadores uv ON uv.visitador = v.id_v
				INNER JOIN visitadores_especialidades ve ON v.id_v = ve.visitador 
				INNER JOIN especialidades e ON e.id_e = ve.especialidad
				INNER JOIN fuerzas f ON f.id_f = e.fuerza
				WHERE uv.usuario = ?;`, [usuario])
				.then((data) => {
					let res = {
						v_id: "",
						v_nombre: "Sin información",
						v_telefono: "Sin información",
						f_id: "",
						f_nombre: "Sin información"
					};
					let especialidades = [{
						id_e: "",
						nombre: "-- Seleccione --"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						res = {
							v_id: data.rows.item(0).v_id,
							v_nombre: data.rows.item(0).v_nombre,
							v_telefono: data.rows.item(0).v_telefono,
							f_id: data.rows.item(0).f_id,
							f_nombre: data.rows.item(0).f_nombre
						};
						for (let i = 0; i < tamaño; i++) {
							especialidades.push({
								id_e: data.rows.item(i).id_e,
								nombre: data.rows.item(i).nombre
							});
						}
					}
					resolve([res, especialidades]);
				})
				.catch(error => {
					console.log("Error consulta mostrarVisitadorMes_no_Internet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}


	mostrarFuerzas_noInternet() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT id_f, nombre
				FROM fuerzas
				ORDER BY nombre ASC;`, [])
				.then((data) => {
					let res = [{
						id_f: "",
						nombre: "Fuerza"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_f: data.rows.item(i).id_f,
								nombre: data.rows.item(i).nombre
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta mostrarFuerzas_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	mostrarEspecialidades_noInternet(fuerza) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT id_e, nombre
				FROM especialidades 
				WHERE fuerza = ?
				ORDER BY nombre ASC;`, [fuerza])
				.then((data) => {
					let res = [{
						id_e: "",
						nombre: "Especialidad"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_e: data.rows.item(i).id_e,
								nombre: data.rows.item(i).nombre
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta mostrarEspecialidades_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	mostrarVisitadores_noInternet(especialidad) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT v.id_v, v.nombre, v.telefono
				FROM visitadores v
				INNER JOIN visitadores_especialidades ve ON v.id_v = ve.visitador 
				WHERE ve.especialidad = ?
				ORDER BY v.nombre ASC`, [especialidad])
				.then((data) => {
					let res = [{
						id_v: "",
						nombre: "Visitador",
						telefono: "Teléfono"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_v: data.rows.item(i).id_v,
								nombre: data.rows.item(i).nombre,
								telefono: data.rows.item(i).telefono
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta mostrarVisitadores_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	mostrarProductos_noInternet(especialidad) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT p.nombre, p.descripcion
				FROM especialidades_productos ep
				INNER JOIN productos_cartilla p ON ep.producto = p.id_p
				WHERE ep.especialidad = ?
				ORDER BY ep.id_ep ASC;`, [especialidad])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								nombre: data.rows.item(i).nombre,
								descripcion: data.rows.item(i).descripcion
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta mostrarProductos_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	guardarDatosVisitaMedica_noInternet(respuestasvisitamedica, datos) {
		return new Promise((resolve, reject) => {
			let continuar = 1;
			let error_;
			if (datos.uno > 0) {
				this.storage.executeSql(`INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)`, [respuestasvisitamedica, 1, datos.uno])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.dos > 0) {
				this.storage.executeSql(`INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)`, [respuestasvisitamedica, 2, datos.dos])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.tres > 0) {
				this.storage.executeSql(`INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)`, [respuestasvisitamedica, 3, datos.tres])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.cuatro > 0) {
				this.storage.executeSql(`INSERT INTO respuestaspreguntasvisita (respuestavisitamedica, pregunta, respuesta) VALUES (?, ?, ?)`, [respuestasvisitamedica, 4, datos.cuatro])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}

			if (continuar == 1) {
				resolve(true);
			} else
				reject(error_);
		});
	}

	guardarVisitaMedica_noInternet(datos) {
		return new Promise((resolve, reject) => {
			let fecha = this.fecha();
			this.storage.executeSql(`INSERT INTO respuestasvisitamedica (usuario, fuerza, especialidad, visitador, comentario, fecha) VALUES (?, ?, ?, ?, ?, ?)`, [datos.usuario, datos.fuerza, datos.especialidad, datos.visitador, datos.comentario, fecha])
				.then((data) => {
					let respuestasvisitamedica = 0;
					this.storage.executeSql("SELECT MAX(id_r) AS id_r FROM respuestasvisitamedica", [])
						.then((data) => {
							respuestasvisitamedica = data.rows.item(0).id_r;
							this.guardarDatosVisitaMedica_noInternet(respuestasvisitamedica, datos)
								.then(() => {
									resolve(1);
								})
								.catch((error) => {
									console.log("Error consulta guardarDatosVisitaMedica_noInternet: " + JSON.stringify(error));
									reject(error);
								});
						})
						.catch(error => {
							console.log("Error consulta MAX respuestasvisitamedica: " + JSON.stringify(error));
						});
				})
				.catch(error => {
					console.log("Error consulta guardarVisitaMedica_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	guardarNovedad_noInternet(datos) {
		return new Promise((resolve, reject) => {
			let fecha = this.fecha();
			this.storage.executeSql(`INSERT INTO publicaciones (usuario, punto, tipo, comentario, fecha, foto, foto_upload) VALUES (?, ?, ?, ?, ?, ?, ?)`, [datos.usuario, datos.punto, "n", datos.comentario, fecha, "png", datos.foto])
				.then(() => {
					resolve(1);
				})
				.catch(error => {
					console.log("Error consulta guardarNovedad_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	mostrarCiudadesEventos() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT ciudad FROM eventos 
				GROUP BY ciudad
				ORDER BY ciudad ASC;`, [])
				.then((data) => {
					let res = [{
						nombre: "Ciudad"
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								nombre: data.rows.item(i).ciudad
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta mostrarCiudadesEventos: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	mostrarEventos(ciudad) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT id_e, nombre, encargado, fecha, hora FROM eventos 
				WHERE ciudad = ?
				ORDER BY nombre ASC;`, [ciudad])
				.then((data) => {
					let res = [{
						id_e: "",
						nombre: "Nombre del evento",
						encargado: "",
						fecha: "",
						hora: ""
					}];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_e: data.rows.item(i).id_e,
								nombre: data.rows.item(i).nombre,
								encargado: data.rows.item(i).encargado,
								fecha: data.rows.item(i).fecha,
								hora: data.rows.item(i).hora
							});
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta mostrarEventos: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	guardarDatosEvento_noInternet(calificacioneseventos, datos) {
		return new Promise((resolve, reject) => {
			let continuar = 1;
			let error_;
			if (datos.uno > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 1, datos.uno])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.dos > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 2, datos.dos])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.tres > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 3, datos.tres])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.cuatro > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 4, datos.cuatro])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.cinco > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 5, datos.cinco])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.seis > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 6, datos.seis])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.siete > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 7, datos.siete])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}
			if (datos.ocho > 0) {
				this.storage.executeSql(`INSERT INTO respuestaseventos (calificacioneseventos, pregunta, respuesta) VALUES (?, ?, ?)`, [calificacioneseventos, 8, datos.ocho])
					.catch(error => {
						continuar = 0;
						error_ = error;
					});
			}

			if (continuar == 1) {
				resolve(true);
			} else
				reject(error_);
		});
	}

	guardarCalificacionEvento_noInternet(publicacion, datos) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`INSERT INTO calificacioneseventos (usuario, evento, publicacion) VALUES (?, ?, ?)`, [datos.usuario, datos.evento, publicacion])
				.then(() => {
					let calificacioneseventos = 0;
					this.storage.executeSql("SELECT MAX(id_c) AS id_c FROM calificacioneseventos", [])
						.then((data) => {
							calificacioneseventos = data.rows.item(0).id_c;
							this.guardarDatosEvento_noInternet(calificacioneseventos, datos)
								.then(() => {
									resolve(1);
								})
								.catch((error) => {
									console.log("Error consulta guardarDatosEvento_noInternet: " + JSON.stringify(error));
									reject(error);
								});
						})
						.catch(error => {
							console.log("Error consulta MAX calificacioneseventos: " + JSON.stringify(error));
						});
				})
				.catch(error => {
					console.log("Error consulta guardarCalificacionEvento_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	guardarEvento_noInternet(datos) {
		return new Promise((resolve, reject) => {
			let fecha = this.fecha();
			this.storage.executeSql(`INSERT INTO publicaciones(usuario, tipo, comentario, evento_nombre, evento_ciudad, foto, fecha, foto_upload) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, [datos.usuario, 'e', datos.comentario, datos.nombre, datos.ciudad, 'png', fecha, datos.foto])
				.then(() => {
					let publicacion = 0;
					this.storage.executeSql("SELECT MAX(id_p) AS id_p FROM publicaciones", [])
						.then((data) => {
							publicacion = data.rows.item(0).id_p;
							this.guardarCalificacionEvento_noInternet(publicacion, datos)
								.then(() => {
									resolve(1);
								})
								.catch((error) => {
									console.log("Error consulta guardarCalificacionEvento_noInternet: " + JSON.stringify(error));
									reject(error);
								});
						})
						.catch(error => {
							console.log("Error consulta MAX publicaciones: " + JSON.stringify(error));
						});
				})
				.catch(error => {
					console.log("Error consulta guardarEvento_noInternet: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	uploadInfo() {
		return new Promise((resolve, reject) => {
			let datos = [];
			this.probarConexion("si")
				.then(() => {
					this.publicaciones()
						.then((data) => {
							datos.push(data);
							this.respuestasvisitamedica()
								.then((data) => {
									datos.push(data);
									if ((datos[0].length > 0) || (datos[1].length > 0)) {
										this.enviarInfo(datos)
											.then((data) => {
												resolve(data);
											})
											.catch((error) => {
												console.log("¡Error, this.enviarInfo!: " + JSON.stringify(error));
												reject(error);
											});
									} else
										resolve(1);
								}).catch((error) => {
									console.log("¡Error, this.respuestasvisitamedica!: " + JSON.stringify(error));
									reject(error);
								});
						}).catch((error) => {
							console.log("¡Error, this.publicaciones!: " + JSON.stringify(error));
							reject(error);
						});
				})
				.catch((error) => {
					console.log("¡Error, uploadInfo!: " + JSON.stringify(error));
					alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
					reject(error);
				});
		});
	}

	enviarInfo(data) {
		return new Promise((resolve, reject) => {
			this.http.post('http://palacalle.abbottdata.com/backend/v1/upload/', { uploadInfo: 1, datos: JSON.stringify(data) }, {})
				.then(data => {
					let res;
					res = JSON.parse(data.data);
					resolve(res);
				})
				.catch(error => {
					if (error.status >= 500) {
						alert("¡Error, el servidor no responde!");
					} else
						if (error.status >= 400) {
							alert("¡Error, no se encuentra el servidor!");
						} else {
							alert("¡Error, no tienes conexión a internet o tu red es muy lenta!");
						}
					console.log("¡Error, enviarInfo!: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	eliminarInfoLocal() {
		return new Promise((resolve, reject) => {
			let datos = [];
			datos.push(['DELETE FROM publicaciones', []]);
			datos.push(['DELETE FROM calificacionexhibicion', []]);
			datos.push(['DELETE FROM calificacionagotados', []]);
			datos.push(['DELETE FROM calificacionprecios', []]);
			datos.push(['DELETE FROM calificacionexhibicionadicional', []]);
			datos.push(['DELETE FROM respuestasexhibicionadicional', []]);
			datos.push(['DELETE FROM calificacioneseventos', []]);
			datos.push(['DELETE FROM respuestaseventos', []]);
			datos.push(['DELETE FROM respuestasvisitamedica', []]);
			datos.push(['DELETE FROM respuestaspreguntasvisita', []]);
			this.ejecutarBatch(datos)
				.then((result) => {
					resolve(1);
				})
				.catch(error => {
					console.log("¡Error, eliminarInfoLocal!: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	publicaciones() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT id_p, usuario, punto, tipo, comentario, foto, fecha, comentario_agotados, respuesta, evento_nombre, evento_ciudad FROM publicaciones;`, [])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					let h = 1;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							let calificacionexhibicion;
							let calificacionagotados;
							let calificacionprecios;
							let calificacionexhibicionadicional;
							let calificacioneseventos;
							let publicacion = data.rows.item(i).id_p;
							let usuario = data.rows.item(i).usuario;
							let punto = data.rows.item(i).punto;
							let tipo = data.rows.item(i).tipo;
							let comentario = data.rows.item(i).comentario;
							let foto = data.rows.item(i).foto;
							let fecha = data.rows.item(i).fecha;
							let comentario_agotados = data.rows.item(i).comentario_agotados;
							let respuesta = data.rows.item(i).respuesta;
							let evento_nombre = data.rows.item(i).evento_nombre;
							let evento_ciudad = data.rows.item(i).evento_ciudad;

							this.calificacionexhibicion(publicacion)
								.then(data => {
									calificacionexhibicion = data;
									this.calificacionagotados(publicacion)
										.then(data => {
											calificacionagotados = data;
											this.calificacionprecios(publicacion)
												.then(data => {
													calificacionprecios = data;
													this.calificacionexhibicionadicional(publicacion)
														.then(data => {
															calificacionexhibicionadicional = data;
															let tamaño_2 = calificacionexhibicionadicional.length;
															let j = 1;
															if (tamaño_2 == 0) {
																this.calificacioneseventos(publicacion)
																	.then(data => {
																		calificacioneseventos = data;
																		res.push([
																			usuario,
																			punto,
																			tipo,
																			comentario,
																			foto,
																			fecha,
																			comentario_agotados,
																			respuesta,
																			evento_nombre,
																			evento_ciudad,
																			calificacionexhibicion,
																			calificacionagotados,
																			calificacionprecios,
																			calificacionexhibicionadicional,
																			calificacioneseventos,
																			publicacion
																		]);
																		if (h == tamaño) {
																			resolve(res);
																		}
																		h++;
																	})
																	.catch((error) => reject(error));
															} else
																for (let exhibicionadicional in calificacionexhibicionadicional) {
																	this.respuestasexhibicionadicional(calificacionexhibicionadicional[exhibicionadicional][0])
																		.then(data => {
																			if (j == tamaño_2) {
																				calificacionexhibicionadicional[exhibicionadicional][4] = data;
																				this.calificacioneseventos(publicacion)
																					.then(data => {
																						calificacioneseventos = data;

																						res.push([
																							usuario,
																							punto,
																							tipo,
																							comentario,
																							foto,
																							fecha,
																							comentario_agotados,
																							respuesta,
																							evento_nombre,
																							evento_ciudad,
																							calificacionexhibicion,
																							calificacionagotados,
																							calificacionprecios,
																							calificacionexhibicionadicional,
																							calificacioneseventos,
																							publicacion
																						]);
																						if (h == tamaño) {
																							resolve(res);
																						}
																						h++;
																					})
																					.catch((error) => reject(error));
																			} else
																				calificacionexhibicionadicional[exhibicionadicional][4] = data;
																			j++;
																		})
																		.catch((error) => reject(error));
																}
														})
														.catch((error) => reject(error));
												})
												.catch((error) => reject(error));
										})
										.catch((error) => reject(error));
								})
								.catch((error) => reject(error));
						}
					} else
						resolve(res);
				})
				.catch(error => {
					console.log("Error consulta publicaciones: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	calificacionexhibicion(publicacion) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT publicacion, r1, r2, r3, r4, r5 FROM calificacionexhibicion WHERE publicacion = ?;`, [publicacion])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push([
								data.rows.item(i).publicacion,
								data.rows.item(i).r1,
								data.rows.item(i).r2,
								data.rows.item(i).r3,
								data.rows.item(i).r4,
								data.rows.item(i).r5
							]);
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta calificacionexhibicion: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	calificacionagotados(publicacion) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT publicacion, producto FROM calificacionagotados WHERE publicacion = ?;`, [publicacion])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push([
								data.rows.item(i).publicacion,
								data.rows.item(i).producto
							]);
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta calificacionagotados: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	calificacionprecios(publicacion) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT publicacion, producto, precio FROM calificacionprecios WHERE publicacion = ?;`, [publicacion])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push([
								data.rows.item(i).publicacion,
								data.rows.item(i).producto,
								data.rows.item(i).precio
							]);
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta calificacionprecios: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	calificacionexhibicionadicional(publicacion) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT id_c, publicacion, exhibicionadicional, foto FROM calificacionexhibicionadicional WHERE publicacion = ?;`, [publicacion])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push([
								data.rows.item(i).id_c,
								data.rows.item(i).publicacion,
								data.rows.item(i).exhibicionadicional,
								data.rows.item(i).foto
							]);
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta calificacionexhibicionadicional: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	respuestasexhibicionadicional(calificacionexhibicionadicional) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT calificacionexhibicion, pregunta, respuesta FROM respuestasexhibicionadicional WHERE calificacionexhibicion = ?;`, [calificacionexhibicionadicional])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push([
								data.rows.item(i).calificacionexhibicion,
								data.rows.item(i).pregunta,
								data.rows.item(i).respuesta
							]);
						}
					}
					resolve(res);
				})
				.catch(error => {
					console.log("Error consulta respuestasexhibicionadicional: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	calificacioneseventos(publicacion) {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT id_c, publicacion, usuario, evento FROM calificacioneseventos WHERE publicacion = ?;`, [publicacion])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push([
								data.rows.item(i).id_c,
								data.rows.item(i).publicacion,
								data.rows.item(i).usuario,
								data.rows.item(i).evento
							]);
						}
					}
					if (tamaño == 0) {
						resolve(res);
					} else
						this.respuestaseventos(res)
							.then(data => {
								resolve(data);
							})
							.catch((error) => reject(error));
				})
				.catch(error => {
					console.log("Error consulta calificacioneseventos: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	respuestaseventos(res, secuencia = 0) {
		return new Promise((resolve, reject) => {
			let tamaño_res = res.length;
			this.storage.executeSql(`SELECT calificacioneseventos, pregunta, respuesta FROM respuestaseventos WHERE calificacioneseventos = ?;`, [res[secuencia][0]])
				.then((data) => {
					let respuestas = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							respuestas.push([
								data.rows.item(i).calificacioneseventos,
								data.rows.item(i).pregunta,
								data.rows.item(i).respuesta
							]);
						}
					}

					res[secuencia][4] = respuestas;
					if ((secuencia + 1) == tamaño_res) {
						resolve(res);
					}

					secuencia++;

					if (secuencia < tamaño_res) {
						this.respuestaseventos(res, secuencia)
							.then(() => {
								resolve(1);
							})
							.catch((error) => reject(error));
					}

				})
				.catch(error => {
					console.log("Error consulta respuestaseventos: " + JSON.stringify(error));
					reject(error);
				});

		});
	}

	respuestasvisitamedica() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql(`SELECT id_r, usuario, fuerza, especialidad, visitador, comentario, fecha FROM respuestasvisitamedica;`, [])
				.then((data) => {
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push([
								data.rows.item(i).id_r,
								data.rows.item(i).usuario,
								data.rows.item(i).fuerza,
								data.rows.item(i).especialidad,
								data.rows.item(i).visitador,
								data.rows.item(i).comentario,
								data.rows.item(i).fecha
							]);
						}
					}
					if (tamaño == 0) {
						resolve(res);
					} else
						this.respuestaspreguntasvisita(res)
							.then(data => {
								resolve(data);
							})
							.catch((error) => reject(error));
				})
				.catch(error => {
					console.log("Error consulta respuestasvisitamedica: " + JSON.stringify(error));
					reject(error);
				});
		});
	}

	respuestaspreguntasvisita(res, secuencia = 0) {
		return new Promise((resolve, reject) => {
			let tamaño_res = res.length;
			this.storage.executeSql(`SELECT respuestavisitamedica, pregunta, respuesta FROM respuestaspreguntasvisita WHERE respuestavisitamedica = ?;`, [res[secuencia][0]])
				.then((data) => {
					let respuestas = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							respuestas.push([
								data.rows.item(i).respuestavisitamedica,
								data.rows.item(i).pregunta,
								data.rows.item(i).respuesta
							]);
						}
					}
					res[secuencia][7] = respuestas;
					if ((secuencia + 1) == tamaño_res) {
						resolve(res);
					}

					secuencia++;

					if (secuencia < tamaño_res) {
						this.respuestaspreguntasvisita(res, secuencia)
							.then((data) => {
								resolve(data);
							})
							.catch((error) => reject(error));
					}
				})
				.catch(error => {
					console.log("Error consulta respuestaspreguntasvisita: " + JSON.stringify(error));
					reject(error);
				});
		});
	}
	//Trae las fotos de la base de datos interna del storage del celular
	traerFotos(fotos) {
		//procesa la foto y la organiza para buscarla en la base de datos y retornarlo
		return new Promise((resolve, reject) => {
			let sql;

			if (fotos[2] == "publicaciones") {
				sql = 'SELECT foto_upload FROM publicaciones WHERE id_p = ?';
			} else
				if (fotos[2] == "exhibiciones-adicionales-usuarios") {
					sql = 'SELECT foto_upload FROM calificacionexhibicionadicional WHERE id_c = ?';
				}
			this.storage.executeSql(sql, [fotos[0]])
				.then(data => {
					resolve([data.rows.item(0).foto_upload, fotos[1], fotos[2]]);
				})
				.catch(error => {
					console.log("Error consulta traerFotos, " + JSON.stringify(error));
					reject(error);
				});

		});
	}

	//Se envia el email al supervisor despues de terminar de calificar un punto
	enviarAvisoSupervisor(publicacion) {
		this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { enviarAvisoSupervisor: 1, publicacion: publicacion.id_p, punto: publicacion.punto }, {});
	}

	//Se envia un email al usuario despues de calificar por novedad
	enviarAvisoUsuario(publicacion) {
		this.http.get('http://palacalle.abbottdata.com/backend/v1/publicaciones/', { enviarAvisoUsuario: 1, publicacion: publicacion.id_p }, {});
	}






	/*  -----------------------------------------------------------  PRUEBAS */
	traerImagen() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql("SELECT id_p, foto_upload FROM publicaciones", [])
				.then((data) => {

					console.log("publicaciones: " + data.rows.length);

					let id_p;
					let foto;

					for (let i = 0; i < data.rows.length; i++) {

						id_p = data.rows.item(i).id_p;
						foto = data.rows.item(i).foto_upload;

						this.cameraProvider.cargarFotoServidor(foto, id_p, "prueba")
							.then((data) => {
								console.log("res traerImagen: " + data);
							})
							.catch(error => {
								console.log("¡Ha ocurrido un error al cargar la imágen cargarFotoServidor!");
							});

					}

				})
				.catch(error => {
					console.log("Error consulta traerImagen: " + JSON.stringify(error));
					reject(error);
				});

		});
	}

	consultaPrueba() {
		return new Promise((resolve, reject) => {
			this.storage.executeSql("SELECT id_p, usuario, punto FROM publicaciones", [])
				.then((data) => {
					console.log("publicaciones: " + data.rows.length);
					let res = [];
					let tamaño = data.rows.length;
					if (tamaño > 0) {
						for (let i = 0; i < tamaño; i++) {
							res.push({
								id_p: data.rows.item(i).id_p,
								usuario: data.rows.item(i).usuario,
								punto: data.rows.item(i).punto
							});
						}
					}
					console.log("datos: " + JSON.stringify(res));

					/* this.storage.executeSql("SELECT c.nombre, up.usuario, up.punto FROM puntos p INNER JOIN usuarios_puntos up ON up.punto = p.id_p INNER JOIN ciudades c ON p.ciudad = c.id_c  WHERE up.usuario = 38;", [])
					 .then((data) => {
						 console.log("ciudades: " + data.rows.length);
						 let res = [];
						 let tamaño = data.rows.length;
						 if( tamaño > 0) {
							 for(let i = 0; i < tamaño; i++) {
								 res.push({
									 nombre: data.rows.item(i).nombre
								 });
							 }
						 }
						 console.log("datos: " + JSON.stringify(res));   
						 resolve(true);   
					 })
					 .catch(error => {
						 console.log("Error consulta ciudades: " + JSON.stringify(error));
						 reject(error);
					 });*/
				})
				.catch(error => {
					console.log("Error consulta usuarios_puntos: " + JSON.stringify(error));
					reject(error);
				});

		});
	}

}
